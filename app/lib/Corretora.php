<?php

class Corretora extends Generic 
{
	public $entidades;

	protected function config () {
		$this->table = get_config('db.prefix') . 'corretora';
		$this->fields = [
			'corretoraid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'nome' => 'varchar(64) DEFAULT NULL',
			'cnpj' => 'varchar(64) DEFAULT NULL',
			'imagem' => 'varchar(255) DEFAULT NULL',
			'email' => 'varchar(255) DEFAULT NULL',
			'codigo' => 'int(9) DEFAULT NULL'
		];
		$this->pk = 'corretoraid';
	}

	protected function afterSave () {
		
	}

	public function saveEntidades () {
		// Apaga as entidades já registradas
		$corretora_entidade = new CorretoraEntidade;
		$corretora_entidade->deleteList(array(
			array(
				'field' => 'corretoraid',
				'value' => $this->getID()
			)
		));
		// Registra as entidades carregadas na classe
		if ( count($this->entidades) > 0 ) foreach ($this->entidades as $item) {
			$corretora_entidade = new CorretoraEntidade(array(
				'relation' => 'AND',
				array(
					'field' => 'corretoraid',
					'value' => $this->getID()
				),
				array(
					'field' => 'entidadeid',
					'value' => $item->getID()
				)
			));
			if ( !$corretora_entidade->getID() ) {
				$corretora_entidade->corretoraid = $this->getID();
				$corretora_entidade->entidadeid = $item->getID();
				$corretora_entidade->save();
			}
		}
	}

	protected function format () {
		// Carrega todas as entidades
		$corretora_entidade = new CorretoraEntidade;
		$corretora_entidade->loadList(array(
			array(
				'field' => 'corretoraid',
				'value' => $this->getID()
			)
		));
		if ( $corretora_entidade->getList() ) foreach ( $corretora_entidade->getList() as $item )
			$this->entidades[] = $item->entidade;
	}

	public function temEntidade ($id) {
		if ( !$this->getID() ) return false;
		$entidades_id = array();
		foreach ($this->entidades as $entidade)
			$entidades_id[] = $entidade->getID();

		return in_array($id, $entidades_id);
	}

	public function setEntidades ($idlist) {
		$this->entidades = array();
		foreach($idlist as $entidadeid) {
			$entidade = new Entidade;
			$entidade->loadByID($entidadeid);
			if ($entidade->getID()) {
				$this->entidades[] = $entidade;
			}
		}
	}

	public function get_the_entidades ($delimiter = ', ') {
		$entidades_nome = array();
		foreach ($this->entidades as $entidade)
			$entidades_nome[] = $entidade->nome;
		return implode($delimiter, $entidades_nome);
	}
}

?>