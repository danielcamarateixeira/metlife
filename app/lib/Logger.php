<?php

class Logger extends Generic
{

	private static 	$instance, $filename, $filepath,
					$buffer;

	protected function config () {
		$this->table = get_config('db.prefix') . 'log';
		$this->fields = [
			'logid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'level' => 'varchar(32) NOT NULL',
			'action' => 'varchar(64) NOT NULL',
			'message' => 'text NOT NULL',
			'performer' => 'varchar(64) NOT NULL',
			'performerip' => 'varchar(45) NOT NULL',
			'target' => 'varchar(64) NOT NULL',
			'datetime' => 'datetime NOT NULL',
		];
		$this->pk = 'logid';
	}

	// Singleton
	public static function getInstance () {
		// Cria a instância
		if (!isset(self::$instance)) {
			self::$instance = new Logger;
			self::$filename = date('Y-m-d').'.log';
			self::$filepath = APP . get_config('logfolder');
			self::$buffer = array();

			if ( !is_dir(self::$filepath) )
				mkdir(self::$filepath);
		}

		return self::$instance;
	}

	public static function add ($level, $action, $message = false, $performer = false, $target = false) {
		if (!$target)
			$target = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if (is_array($message))
			$message = json_encode($message);
		$datetime = new DateTime('NOW');

		self::$buffer[] = array(
			'level' => $level,
			'action' => $action,
			'message' => $message,
			'performer' => $performer,
			'performerip' => $_SERVER['REMOTE_ADDR'],
			'target' => $target,
			'datetime' => $datetime->format('Y-m-d G:i:s.u')
		);
	}

	public function setLevel ($level) { $this->set('level', $level); }
	public function setAction ($action) { $this->set('action', $action); }
	public function setMessage ($message) { $this->set('message', $message); }
	public function setPerformer ($performer) { $this->set('performer', $performer); }
	public function setPerformerIp ($performerip) { $this->set('performerip', $performerip); }
	public function setTarget ($target) { $this->set('target', $target); }
	public function setDatetime ($datetime) { $this->set('datetime', $datetime); }

	public static function error ($action, $message = false, $performer = false, $target = false) {
		Logger::add('ERROR', $action, $message, $performer, $target);
	}
	public static function debug ($action, $message = false, $performer = false, $target = false) {
		Logger::add('DEBUG', $action, $message, $performer, $target);
	}
	public static function info ($action, $message = false, $performer = false, $target = false) {
		Logger::add('INFO', $action, $message, $performer, $target);
	}

	public static function register () {
		$log_file_str = '';
		if (count(self::$buffer)>0) foreach (self::$buffer as $item) {
			// Salva no BD
			$log = new Logger;
			$log->setLevel( $item['level'] );
			$log->setAction( $item['action'] );
			$log->setMessage( $item['message'] );
			$log->setPerformer( $item['performer'] );
			$log->setPerformerIp( $item['performerip'] );
			$log->setTarget( $item['target'] );
			$log->setDatetime( $item['datetime'] );
			$log->save();

			$log_file_str .= '['.$item['datetime'].'] ['.$item['level'].'] '.$item['action'].' '.$item['message'].'
';
		}

		file_put_contents(self::$filepath.'/'.self::$filename, $log_file_str, FILE_APPEND);
	}

}