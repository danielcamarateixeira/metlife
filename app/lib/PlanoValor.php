<?php

class PlanoValor extends Generic
{

	protected function config () {
		$this->table = get_config('db.prefix') . 'planos_valor';
		$this->fields = [
			'planovalorid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'planoid' => 'int(9) NOT NULL',
			'entidadeid' => 'int(9) NOT NULL',
			'valor' => 'decimal(10,2) NOT NULL'
		];
		$this->pk = 'planovalorid';
	}

	protected function format () {
		$this->entidade = new Entidade;
		$this->entidade->loadByID( $this->entidadeid );

		$this->plano = new Plano;
		$this->plano->loadByID( $this->planoid );
	}

}