<?php


abstract class Generic
{

	protected 	$fields, $pk, $table, $filters_index,
				$properties, $list;
	public 		$per_page, $next_page, $prev_page, $max_pages, $curr_page;


	/**
	 * CLASS HEADERS
	 */

	// Construtor
	public function __construct ($filters = false) {
		// Paginação
		$this->per_page = 10000;
		$this->prev_page = false;
		$this->next_page = false;
		$this->max_pages = false;
		$this->curr_page = false;
		
		$this->config();
		$this->init();

		$filters_index = array();
		$this->loadDefaultData();

		if ($filters)
			$this->loadSingle($filters);
	}

	protected function init () {
		// Cria a tabela
		$row_fields = array();
		if ( count($this->fields) ) foreach ($this->fields as $column => $info) {
			$row_fields[] = "{$column} {$info}";
		}
		$string_fields = implode(', ', $row_fields);
		$primary_key = ( isset($this->pk) ) ? ", PRIMARY KEY ({$this->pk})" : '';
		try {
			$create_table = Database::getInstance()->exec('CREATE TABLE IF NOT EXISTS '.$this->table.' ('.$string_fields.' '.$primary_key.') ENGINE=InnoDB DEFAULT CHARSET=latin1;');
		} catch (PDOException $e) {
			die('Erro ao criar a tabela '.$this->table);
		}
		
		// Atualiza a tabela
		if ( count($this->fields) ) foreach ($this->fields as $column => $info) {
			try {
				$column_exists = Database::getInstance()->query("SHOW COLUMNS FROM ".$this->table." LIKE '{$column}'")->fetchAll();
				if ( !count( $column_exists ) ) {
					Database::getInstance()->exec("ALTER TABLE ".$this->table." ADD {$column} {$info}");
				}
			} catch (PDOException $e) {
				die('O campo '.$column.' não pode ser inserido na tabela '.$this->table);
			}
		}
	}

	protected function config () {
		$this->table = get_config('db.prefix') . 'generic';
		$this->fields = [
			'primary_key' => 'int(9) NOT NULL AUTO_INCREMENT',
			'field_test' => 'varchar(32) NOT NULL',
			'another_field' => 'datetime NOT NULL'
		];
		$this->pk = 'primary_key';
	}

	/* --- END OF CLASS HEADERS --- */


	/**
	 * GET
	 */

	public function getID () {
		return $this->get($this->pk);
	}
	public function get ($option) {
		if (!$this->properties) return false;
		if (!array_key_exists($option, $this->properties)) return false;

		return $this->properties[$option];
	}
	public function __get($name) {
        if (isset($this->properties[strtolower($name)])) {
            return $this->properties[strtolower($name)];
        } else {
            return false;
        }
    }


	/* --- END OF GET --- */

	/**
	 * SET
	 */

	public function set ($option, $value) {
		$this->properties[$option] = $value;
	}
	public function __set($name, $value) {
        $this->properties[strtolower($name)] = $value;
    }

	/* --- END OF SET --- */


	/**
	 * LOAD
	 */

	public function loadSingle ($filters) {
		// Reseta os filtros e cria a string do WHERE
		$this->clearFilterIndex();
		$WHERE = $this->parseFilters($filters);
		$ORDERBY = (isset($filters['orderby'])) ? $filters['orderby'] : false;
		$ORDER = (isset($filters['order'])) ? $filters['order'] : 'ASC';
		$ORDERRULE = ($ORDERBY) ? "ORDER BY {$ORDERBY} {$ORDER}" : false;

		$sql = "SELECT * FROM $this->table WHERE {$WHERE} {$ORDERRULE}";
		$search = Database::getInstance()->prepare($sql);
		$search = $this->bindList($search, $filters);

		try {
			$search->execute();
			$this->properties = $search->fetch(PDO::FETCH_ASSOC);
			$this->format();
		} catch (PDOException $e) {
			debug($e->getMessage());
			die('Erro ao processar um query');
		}
	}

	public function loadDefaultData () {
		foreach ($this->fields as $field => $value) {
			$this->$field = '';
		}
	}

	public function loadByID ($id) {
		$args = array(
			array(
				'field' => $this->pk,
				'value' => $id
			)
		);
		$this->loadSingle($args);
	}

	/* --- END OF LOAD --- */


	public function recordExists ($filters) {
		$this->clearFilterIndex();
		$WHERE = $this->parseFilters($filters);

		$sql = "SELECT COUNT(*) AS total FROM $this->table WHERE {$WHERE}";
		$search = Database::getInstance()->prepare($sql);
		$search = $this->bindList($search, $filters);
		try {
			$search->execute();
			$result = $search->fetchObject();
			$total = (bool) $result->total;
			return $total;
		} catch (Exception $e) {
			debug($e->getMessage());
			die('Erro ao processar um query');
		}
	}

	public function loadSingleWithData ($data) {
		$this->properties = $data;
		$this->format();
	}

	public function loadList ($filters = 1, $page = false) {
		$this->clearFilterIndex();
		$this->clearList();
		$GROUP = (isset($filters['groupby'])) ? "GROUP BY " . $filters['groupby'] : "GROUP BY {$this->pk}";
		$SEARCH_FIELDS = (isset($filters['search_fields'])) ? implode(',', $filters['search_fields']) : '*';
		unset($filters['groupby'], $filters['search_fields']);
		$WHERE = $this->parseFilters($filters);
		$ORDERBY = (isset($filters['orderby'])) ? $filters['orderby'] : false;
		$ORDER = (isset($filters['order'])) ? $filters['order'] : 'ASC';
		$ORDERRULE = ($ORDERBY) ? "ORDER BY {$ORDERBY} {$ORDER}" : false;

		// Paginação
		if ( $page ) {
			if ( isset($_GET[$page]) ) {
				$page = (int) $_GET[$page];
				if ($page <= 0) $page = 1;
			}
			else
				$page = 1;

			$this->curr_page = $page;
			$offset = $this->per_page * ($page-1);

			$count = new static;
			$count = $count->countList($filters);
			$next_offset = $this->per_page * $page;
			$this->next_page = ( $next_offset >= $count ) ? false : $page+1;
			$this->prev_page = ( $page-1 <= 0 ) ? false : $page-1;
			$this->max_pages = ceil( $count / $this->per_page );

			$LIMIT = "LIMIT {$offset}, {$this->per_page}";
		}
		else
			$LIMIT = (isset($filters['limit'])) ? "LIMIT ".$filters['limit'] : false;

		$sql = "SELECT {$SEARCH_FIELDS} FROM $this->table WHERE {$WHERE} {$GROUP} {$ORDERRULE} {$LIMIT}";
		$search = Database::getInstance()->prepare($sql);
		$search = $this->bindList($search, $filters);
		
		try {
			$search->execute();
			
			while ($single = $search->fetch(PDO::FETCH_ASSOC)) {
				$obj = new static;
				$obj->loadSingleWithData( $single );
				$this->list[] = $obj;
				// $this->list[] = $single;
			}
			return $this->list;
		} catch (PDOException $e) {
			debug($e->getMessage());
			die('Erro ao processar um query');
		}
	}

	public function countList ($filters) {
		$this->clearFilterIndex();
		$this->clearList();
		$WHERE = $this->parseFilters($filters);
		$sql = "SELECT COUNT(*) FROM $this->table WHERE {$WHERE}";
		$search = Database::getInstance()->prepare($sql);
		$search = $this->bindList($search, $filters);

		try {
			$search->execute();
			return $search->fetchColumn();
		} catch (PDOException $e) {
			debug($e->getMessage());
			die('Erro ao processar um query');
		}
	}

	function bindList ($obj, $filters) {
		foreach ( $this->filters_index as $column => $value ) {
			if ( strtoupper($value[1]) == 'LIKE' )
				$obj->bindValue( ":{$column}", '%'.$value[0].'%' );
			elseif ( strtolower($value[0]) == 'null' )
				$obj->bindValue( ":{$column}", null, PDO::PARAM_INT );
			else
				$obj->bindValue( ":{$column}", $value[0] );
		}

		return $obj;
	}

	public function getList () {
		return $this->list;
	}

	protected function clearList () {
		$this->list = array();
	}

	public function save () {
		$this->beforeSave();

		// Pega o ID, caso for 0 cria um novo registro
		$id = ($this->get($this->pk)) ? $this->get($this->pk) : 0;
		// Define se é INSERT ou UPDATE
		$STATEMENT = ($id) ? 'UPDATE': 'INSERT';
		// Define os campos a serem atualizados (todos)
		$FIELDS = array();
		$BIND = array();
		foreach( $this->fields as $column => $value ) {
			if (!$id && $column == $this->pk) {}
			else {
				$FIELDS[] = "{$column} = :{$column}";
				$BIND[$column] = $this->$column;
			}
		}

		$CLAUSURES = implode(', ', $FIELDS);
		// Define a cláusula WHERE
		$WHERE = ($id) ? "WHERE {$this->pk} = {$id}" : '';
		// Cria a query
		$SQL = "{$STATEMENT} {$this->table} SET {$CLAUSURES} {$WHERE}";

		$save = Database::getInstance()->prepare($SQL);
		// Faz o bindValue somente dos campos que existem no banco de dados
		foreach ($BIND as $column => $value) {
			// É um campo no banco de dados
			if ( array_key_exists($column, $this->fields) ) {
				if (mb_strtolower($value) == 'null' || $value === false  || $value == '')
					$save->bindValue( ":{$column}", null, PDO::PARAM_INT );
				else
					$save->bindValue( ":{$column}", $value );
			}
		}
		// Executa a query
		try {
			$save->execute();
		} catch (Exception $e) {
			debug($e->getMessage());
		}

		// Carrega o registro atualizado na prórpia instância
		$id = ($id) ? $id : Database::getInstance()->lastInsertId();
		$args = array(
			array(
				'field' => $this->pk,
				'value' => $id
			)
		);
		$this->loadSingle($args);

		$this->afterSave();
	}

	protected function beforeSave () {
		// Callback ao salvar (antes de montar a query)
	}
	protected function afterSave () {
		// Callback ao salvar (ao final do método)
	}

	protected function format () {
		if (!$this->properties) return false;

		$result = $this->properties;

		// ... modifica a result
		// $result['field'] = 'value';
		// ---

		$this->properties = $result;
	}

	/**
	 * FILTERS
	 */

	// Retorna a string formatada para a query
	protected function parseFilters ($filters) {
		// Caso seja uma string ou esteja vazio retorna o valor sem processar nada
		if (!$filters) return $this->customizeFilters('1');
		if (!is_array($filters)) return $this->customizeFilters($filters);

		// Verifica se é um array expandido
		if (isset($filters[0]) || isset($filters['relation'])) {
			if ( is_array($filters[0]) )
			{
				// Trabalha com o argumento 'relation'
				$filter_parts = array();
				$relation = isset($filters['relation']) ? $filters['relation'] : 'AND';
				unset($filters['relation'], $filters['order'], $filters['orderby'], $filters['limit']);

				// Percorre os campos definidos no filtro
				if (count($filters)>0) foreach ( $filters as $key => $filter ) {
					// Se for um conjunto de filtros dentro do filtro chama a função recursivamente
					if ( array_key_exists('relation', $filter) )
						$filter_parts[] = '('. $this->parseFilters($filter) . ')';
					// Se for apenas filtros trabalha eles
					else {
						// Organiza as propriedades padrão
						$column = $filter['field'];
						$value = $filter['value'];
						$compare = isset($filter['compare']) ? $filter['compare'] : '=';
						$column_index = md5(uniqid(rand(), true));
						// Verifica se usa algum tipo de função
						if ( isset($filter['function']) ) {
							$function = $filter['function'];
							
							if ( $function == 'FIND_IN_SET' )
								$filter_parts[] = "{$function}(:{$column_index}, {$column})";
							else
								$filter_parts[] = "{$function}(:{$column_index}, {$column})";

							$this->filters_index[$column_index] = array( $filter['value'], $compare );
						}
						elseif ( 'null' == mb_strtolower($value) ) {
							$filter_parts[] = "{$column} IS NULL";
						}
						else {
							// Cria a string com os dados do filtro
							$filter_parts[] = "{$column} {$compare} :{$column_index}";
							$this->filters_index[$column_index] = array( $filter['value'], $compare );
						}
						// Adiciona a index para uso do método bindValue();
						// $this->filters_index[$column_index] = array( $filter['value'], $compare );
					}
				}

			}

			// Retorna a string de regras
			return $this->customizeFilters( implode(' '.$relation.' ', $filter_parts) );
		}
		return 1;
	}

	protected function customizeFilters ($filter_str) {
		return $filter_str;
	}

	// Esvazia o Array com os filtros
	protected function clearFilterIndex () {
		$this->filters_index = array();
	}

	protected function getFields () {
		if (!$this->properties) return false;
		$fields_list = array();
		foreach ($this->properties as $column => $value) {

		}
	}

	public function getProperties () {
		return $this->properties;
	}

	public function getPropertiesNames () {
		if (!$this->properties) return false;
		$fields_list = array();

		foreach ($this->properties as $column => $value)
			$fields_list[] = $column;

		return $fields_list;
	}

	public function getPaginationHtml () {
		parse_str($_SERVER['QUERY_STRING'], $parseqs);
		$html = '<ul class="pagination">';
		if ( $this->prev_page )
			$parseqs['p'] = $this->prev_page;
		$prev_class = (!$this->prev_page) ? 'class="disabled page-item"' : 'class="page-item"';
		$prev_url = ($this->prev_page) ? '?' . http_build_query($parseqs, '', '&amp;') : '#';
		$html .= sprintf('<li %s><a class="page-link" href="%s">&laquo;</a></li>', $prev_class, $prev_url);
		for($i=1; $i<=$this->max_pages; $i++) {
			$parseqs['p'] = $i;
			$class = ( $this->curr_page == $i ) ? 'class="active page-item"' : 'class="page-item"';
			$html .= sprintf('<li %s><a class="page-link" href="%s">%s</a></li>', $class, '?' . http_build_query($parseqs, '', '&amp;'), $i);
		}
		if ($this->next_page)
			$parseqs['p'] = $this->next_page;
		$next_class = (!$this->next_page) ? 'class="disabled page-item"' : 'class="page-item"';
		$next_url = ($this->next_page) ? '?' . http_build_query($parseqs, '', '&amp;') : '#';
		$html .= sprintf('<li %s><a class="page-link" href="%s">&raquo;</a></li>', $next_class, $next_url);

		$html .= '</ul>';
		return $html;
	}

	public function haveProp ($prop, $array) {
		if ( !is_array($array) )
			$array = array($array);

		$thisProp = explode(',', $this->$prop);

		$search = array_intersect($array, $thisProp);

		if ( count($search) > 0 )
			return true;
		else
			return false;
	}

	public function deleteList ($filters = false) {
		if ($filters) {
			$this->clearFilterIndex();
			$WHERE = $this->parseFilters($filters);

			$sql = "DELETE FROM $this->table WHERE {$WHERE}";
			$delete = Database::getInstance()->prepare($sql);
			$delete = $this->bindList($delete, $filters);
			
			try {
				return $delete->execute();
			} catch (PDOException $e) {
				debug($e->getMessage());
				die('Erro ao processar um query');
			}
		}
	}

	public function delete () {
		$id = $this->getID();
		if (!$id) return false;

		$delete = Database::getInstance()->exec("DELETE FROM {$this->table} WHERE {$this->pk} = '{$id}'");
		return $delete;
	}
	public function drop () {
		$delete = Database::getInstance()->prepare("DROP TABLE {$this->table}");
		$delete->execute();
		return $delete;
	}
	public function truncate () {
		return Database::getInstance()->exec("TRUNCATE TABLE {$this->table}");
	}

}