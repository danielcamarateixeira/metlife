<?php

class Asset
{

	private static $css_header, $css_footer, $js_header, $js_footer,
			$instance;


	// O método singleton 
    public static function getInstance ()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    private function __construct () {}

    public static function addCss ($file, $section = 'header', $external = false) {
    	if (!$external) $file = CSS . "/{$file}";

    	if ($section == 'header')
    		self::$css_header[] = $file;
    	else
    		self::$css_footer[] = $file;
    }

    public static function addJs ($file, $section = 'header', $external = false) {
    	if (!$external) $file = JS . "/{$file}";

    	if ($section == 'header')
    		self::$js_header[] = $file;
    	else
    		self::$js_footer[] = $file;
    }

    public static function loadCss ($section = 'header') {
    	$css = ($section == 'header') ? self::$css_header : self::$css_footer;

    	if (!count($css)) return false;
    	foreach ($css as $file) {
    		echo '<link rel="stylesheet" href="'.$file.'">', PHP_EOL;
    	}
    }

    public static function loadJs ($section = 'header') {
    	$js = ($section == 'header') ? self::$js_header : self::$js_footer;

    	if (!count($js)) return false;
    	foreach ($js as $file) {
    		echo '<script src="'.$file.'"></script>', PHP_EOL;
    	}
    }

    public static function getImage ($image, $default = false) {
    	if (!$default)
			return IMAGES . '/' . $image;
		else {
			if ( file_exists( SRC . '/assets/images/' . $image ) )
				return IMAGES . '/' . $image;
			else
				return IMAGES . '/' . $default;
		}
    }

}

function the_image ($file, $default = false) {
	echo Asset::getImage($file, $default);
}

function get_image ($file, $default = false) {
    return Asset::getImage($file, $default);
}