<?php

class Entidade extends Generic 
{
	protected function config () {
		$this->table = get_config('db.prefix') . 'entidade';
		$this->fields = [
			'entidadeid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'nome' => 'varchar(64) NOT NULL',
			'codigo' => 'int(9) NOT NULL',
		];
		$this->pk = 'entidadeid';
	}
}

?>