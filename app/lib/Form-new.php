<?php

class Form
{
	protected 	$json_raw, $json,
				$steps, $groups, $fields;

	public function __construct ($filepath) {
		$this->loadFile( $filepath );
		$this->parseJson();
	}

	public function loadFile ($filepath) {
		if ( file_exists($filepath) ) {
			$this->json_raw = file_get_contents( $filepath );
			$this->json = json_decode( $this->json_raw, true );
		}
	}

	public function parseJson () {

	}

	protected function parseField ($name, $input) {

	}

	protected function getInput ($name, $input) {

	}

	protected function getTextarea ($name, $input) {

	}

	protected function getRadio ($name, $input) {

	}

	protected function getCheckbox ($name, $input) {

	}

	protected function getSelect ($name, $input) {

	}

	protected function getTitle () {

	}
}

?>