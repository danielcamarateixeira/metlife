<?php

class Database
{

	private static $instance;

	// O método singleton 
    public static function getInstance ()
    {
        if (!isset(self::$instance)) {
        	try {
        		self::$instance = new PDO("mysql:host=".get_config('db.host').";dbname=".get_config('db.name'), get_config('db.user'), get_config('db.pass'), array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

	            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
        	} catch (PDOException $e) {
        		die('<h1>Impossível conectar com o banco de dados</h1>');
        	}
            
        }

        return self::$instance;
    }

    private function __construct () {}

}