<?php

class Form
{
	public $filepath, $editmode;
	protected $html, $html_output, $field_html, $js, $js_output, $json, $tagtemplate;
	protected $fields, $values, $fields_name;

	public function __construct ($string, $values = false) {
		if ( is_json($string) ) {
			$this->json = json_decode( $string, true );
		}
		else {
			$this->filepath = $string;
			$this->json = json_decode( file_get_contents($this->filepath), true );
		}
		
		$this->html = array();
		$this->values = $values;
		$this->editmode = false;
		$this->parseJson();
		$this->buildHtml();
	}

	public function parseJson () {
		$this->html = array();
		foreach ($this->json[0] as $key => $item) {
			if ($item['type'] == 'group')
				$this->html[] = $this->parseGroup($item, $key);
			else
				$this->html[] = $this->parseFields();
		}
	}

	public function parseGroup ($group, $name) {
		$tags = array();

		$nameprefix = isset($group['nameprefix']) ? $group['nameprefix'] : false;

		foreach ($group as $key => $item) {
			$wrap = true;
			if (isset($item['wrap']))
				if ($item['wrap'] == false)
					$wrap = false;

			

			if ($key == 'showtitle')
				if ($item == true)
					$tags[] = sprintf('<h2 class="form-label-title">%s</h2>', $group['title']);

			if ( $key == 'fields' ) {
				if ($wrap)
					$tags[] = sprintf('<div class="form-group-wrap %s">', $name);
				
				$tags[] = $this->parseFields($item, $nameprefix);

				if ($wrap)
					$tags[] = '</div>';
			}
		}

		return $tags;
	}

	public function parseFields ($fields, $nameprefix = false) {
		$tags = array();

		foreach ($fields as $name => $item) {
			$tags[] = $this->parseField($name, $item, $nameprefix);
		}

		return $tags;
	}

	public function parseField ($name, $item, $nameprefix = false) {
		$tags = array();
		$tagid = 'input'.ucfirst($name);
		$xclass = (isset($item['xclass'])) ? $item['xclass'] : '';
		$xclass .= " input-{$name}";
		$xclasscontainer = (isset($item['xclasscontainer'])) ? $item['xclasscontainer'] : false;
		$xdata = (isset($item['xdata'])) ? $this->parseData($item['xdata']) : false;
		$placeholder = (isset($item['placeholder'])) ? 'placeholder="'.$item['placeholder'].'"' : false;
		$required = (isset($item['required'])) ? (bool) $item['required'] : true;
		$required = ($required) ? 'required' : false;
		$label = (isset($item['label'])) ? $item['label'] : false;
		$rows = (isset($item['rows'])) ? 'rows="'.$item['rows'].'"' : false;
		$length = (isset($item['length'])) ? $item['length'] : false;
		$inline = (isset($item['inline'])) ? $item['inline'] : false;
		$readonly = (isset($item['readonly'])) ? 'readonly' : false;
		$disabled = (isset($item['disabled'])) ? 'disabled' : false;
		$value = (isset($item['value'])) ? $item['value'] : false;
		$strtotime = (isset($item['strtotime'])) ? $item['strtotime'] : false;
		$dateformat = (isset($item['dateformat'])) ? $item['dateformat'] : false;
		$min = (isset($item['min'])) ? $item['min'] : false;
		$max = (isset($item['max'])) ? $item['max'] : false;
		if ( $min && $item['type'] == 'date' ) $min = 'min="'.date('Y-m-d', strtotime($min)).'"';
		elseif ( $min ) $min = 'min="'.$min.'"';
		if ( $max && $item['type'] == 'date' ) $max = 'max="'.date('Y-m-d', strtotime($max)).'"';
		elseif ( $max ) $max = 'max="'.$max.'"';
		$mask = (isset($item['mask'])) ? 'data-mask="'.$item['mask'].'"' : false;
		$prepend = (isset($item['prepend'])) ? $item['prepend'] : false;
		$append = (isset($item['append'])) ? $item['append'] : false;
		$addonl = (isset($item['addonl'])) ? $item['addonl'] : false;
		$addonr = (isset($item['addonr'])) ? $item['addonr'] : false;
		$addon = ($addonl || $addonr);


		if ( $nameprefix ) {
			$name = $nameprefix . $name;
			$tagid = 'input_' . $name;
		}

		if ( isset($this->values[$name]) )
			$value = $this->values[$name];

		if ($inline == 'first' || !$inline)
			$tags[] = '<div class="form-field-wrap row clearfix">';

		if (isset($item['before']))
			$tags[] = $item['before'];

		$tags[] = sprintf('<div class="form-group %s">', $xclasscontainer);

		switch ($item['type']) {
			case "checkbox":
				$fieldhtml = $this->parseCheckbox($name, $item);
				$this->fields[] = array(
					'storage_type' => 'text',
					'column_name' => $name,
					'column_label' => $label
				);
			break;
			case "radio":
				$fieldhtml = $this->parseRadio($name, $item);
				$this->fields[] = array(
					'storage_type' => 'text',
					"column_name" => $name,
					'column_label' => $label
				);
			break;
			case "select":
				$fieldhtml = $this->parseSelect($name, $item);
				$this->fields[] = array(
					'storage_type' => 'text',
					"column_name" => $name,
					'column_label' => $label
				);
			break;
			case "textarea":
				$fieldhtml = '';
				if ($label)
					$fieldhtml .= sprintf('<label for="%s" class="form-label-title">%s</label>', $tagid, $label);

				$fieldhtml .= sprintf('<textarea name="%s" id="%s" class="form-control %s" %s %s %s %s>%s</textarea>', $name, $tagid, $xclass, $placeholder, $required, $rows, $readonly, $value);
				$this->fields[] = array(
					'storage_type' => 'text',
					'column_name' => $name,
					'column_label' => $label
				);
			break;
			default:
				$fieldhtml = '';
				if ($label)
					$fieldhtml .= sprintf('<label for="%s">%s</label>', $tagid, $label);

				if ($value && $strtotime && !isset($this->values[$name])) {
					$timestr = strtotime($value);
					$value = strftime($dateformat, strtotime($value));
				}

				if ($value)
					$value = 'value="'.$value.'"';

				if ($prepend)
					$fieldhtml .= '<span class="inline">'.$prepend.'</span>';

				if ($addon)
					$fieldhtml .= '<div class="input-group">';

				if ($addonl)
					$fieldhtml .= '<span class="input-group-addon">'.$addonl.'</span>';

				$fieldhtml .= sprintf('<input type="%s" name="%s" id="%s" class="form-control %s" %s %s %s %s %s %s %s %s %s>', $item['type'], $name, $tagid, $xclass, $placeholder, $required, $readonly, $disabled, $value, $xdata, $min, $max, $mask);

				if ($addonr)
					$fieldhtml .= '<span class="input-group-addon">'.$addonr.'</span>';

				if ($addon)
					$fieldhtml .= '</div>';

				$length = (!$length) ? '255' : $length;
				$this->fields[] = array(
					'storage_type' => 'varchar',
					'storage_length' => $length,
					'column_name' => $name,
					'column_label' => $label
				);

				if ($append)
					$fieldhtml .= '<span class="inline">'.$prepend.'</span>';
			break;
		}
		$this->fields_name[] = $name;
		$tags[] = $fieldhtml;

		$tags[] = '</div>';

		if (isset($item['after']))
			$tags[] = $item['after'];

		if ($inline == 'last' || !$inline)
			$tags[] = '</div>'; // wrapper

		// Grava o HTML do campo
		$field_html_string = (is_array($fieldhtml)) ? implode('', $fieldhtml) : $fieldhtml;
		$this->field_html[$name] = $field_html_string;
		return $tags;
	}

	protected function parseData ($xdata) {
		$data = array();
		foreach ( $xdata as $field => $value )
			$data[] = "data-{$field}=\"{$value}\"";
		return implode(' ', $data);
	}


	public function parseCheckbox ($name, $checkbox) {
		$tags = array();

		if ( isset($checkbox['title']) )
			$tags[] = sprintf('<label class="form-label-title">%s</label>', $checkbox['title']);

		$optionsinline = false;
		if ( isset($checkbox['optionsinline']) )
			if ( $checkbox['optionsinline'] == true )
				$optionsinline = true;

		$values_checked = (isset($this->values[$name])) ? explode(';', $this->values[$name]) : false;
			
		$tags[] = '<div class="form-checkbox-group">';
		if (isset($checkbox['options'])) foreach ($checkbox['options'] as $i => $option) {
			$tagid = 'chk'.ucfirst($name).$i;
			$checked = ( isset($this->values[$name]) && in_array($option, $values_checked) ) ? 'checked' : false;
			$tag = ($optionsinline) ? '<label class="checkbox-inline">' : '<div class="checkbox"><label>';
			$tag .= sprintf('<input name="%s[]" type="checkbox" id="%s" value="%s" %s> %s', $name, $tagid, $option, $checked, $option);
			$tag .= ($optionsinline) ? '</label>' : '</label></div>';

			$tags[] = $tag;
		}
		$tags[] = '</div>';

		return $tags;
	}

	public function parseRadio ($name, $radio) {
		$tags = array();
		$required = (isset($radio['required'])) ? (bool) $radio['required'] : true;
		$required = ($required) ? 'required' : false;

		if ( isset($radio['title']) )
			$tags[] = sprintf('<label class="form-label-title">%s</label>', $radio['title']);

		$optionsinline = false;
		if ( isset($radio['optionsinline']) )
			if ( $radio['optionsinline'] == true )
				$optionsinline = true;

		$values_checked = (isset($this->values[$name])) ? explode(';', $this->values[$name]) : false;
			
		$tags[] = '<div class="form-radio-group">';
		if (isset($radio['options'])) foreach ($radio['options'] as $i => $option) {
			$tagid = 'rad'.ucfirst($name).$i;
			$checked = ( isset($this->values[$name]) && in_array($option, $values_checked) ) ? 'checked' : false;
			$tag = ($optionsinline) ? '<label class="radio-inline">' : '<div class="radio"><label>';
			$tag .= sprintf('<input name="%s" type="radio" id="%s" value="%s" %s %s> %s', $name, $tagid, $option, $required, $checked, $option);
			$tag .= ($optionsinline) ? '</label>' : '</label></div>';

			$tags[] = $tag;
		}
		$tags[] = '</div>';

		return $tags;
	}

	public function parseSelect ($name, $select) {
		$tags = array();
		$tagid = (isset($select['id'])) ? $select['id'] : 'sl'.ucfirst($name);
		$required = (isset($select['required'])) ? 'required' : false;
		$multiple = (isset($select['multiple'])) ? 'multiple' : false;
		$strtotime = (isset($select['strtotime'])) ? $select['strtotime'] : false;
		$dateformat = (isset($select['dateformat'])) ? $select['dateformat'] : 'Y-m-d';
		$values_selected = (isset($this->values[$name])) ? explode(';', $this->values[$name]) : false;
		$xclass = (isset($select['xclass'])) ? $select['xclass'] : '';
		$xclass .= " input-{$name}";

		if (isset($select['label'])) {
			$tags[] = sprintf('<label for="%s">%s</label>', $tagid, $select['label']);
		}

		$tags[] = sprintf('<select name="%s" id="%s" class="form-control %s" %s %s>', $name, $tagid, $xclass, $required, $multiple);

		if (isset($select['placeholder'])) {
			$tags[] = sprintf('<option disabled selected>%s</option>', $select['placeholder']);
		}

		if (isset($select['options'])) foreach ($select['options'] as $i => $option) {
			$option = ($strtotime) ? strftime($dateformat, strtotime($option)) : $option;
			$selected = ( isset($this->values[$name]) && in_array($option, $values_selected) ) ? 'selected' : false;
			$tags[] = sprintf('<option %s>%s</option>', $selected, $option);
		}

		$tags[] = '</select>';

		return $tags;
	}

	public function buildHtml () {
		$this->html_output = multi_implode($this->html, '');
	}

	public function getHtml () {
		return $this->html_output;
	}

	public function getFields () {
		return $this->fields;
	}
	public function getFieldsName () {
		return $this->fields_name;
	}

	public function getFieldHtml ($fieldname) {
		$html = $this->field_html[$fieldname];
		return $html;
	}

	public function setValues ($array) {
		$this->values = $array;
	}
}

?>