<?php

class Cancelamento extends Generic
{

	private $mail, $status_index;

	protected function config () {
		$this->table = get_config('db.prefix') . 'cancelamento';
		$this->fields = [
			'cancelamentoid' => 'int(32) NOT NULL AUTO_INCREMENT',
			'protocolo' => 'varchar(32) NOT NULL',
			'status' => 'varchar(64) NOT NULL',
			'datecreate' => 'datetime NOT NULL',
			'dateupdate' => 'datetime NOT NULL',
			'remoteip' => 'varchar(26) NOT NULL',
			'remoteinfo' => 'text NOT NULL',
			'ciente' => 'tinyint(1) NOT NULL',
			'concordo' => 'tinyint(1) NOT NULL',
			'mail_protocolo' => 'tinyint(1) NOT NULL',
			'tipoatendimento' => 'varchar(32) NOT NULL',
			'observacao_operador' => 'text NOT NULL',
			'usuarioid' => 'int(9) NOT NULL',
		];
		// Preenche o resto dos campos do BD com o JSON do form
		$form = new Form(VIEW.'/forms/cancelamento.json');
		foreach( $form->getFields() as $field ) {
			$field_length = (isset($field['storage_length'])) ? sprintf('(%u)', $field['storage_length']) : false;
			$column_name = str_replace('-', '_', $field['column_name']);
			$this->fields[$column_name] = sprintf('%s%s NOT NULL', $field['storage_type'], $field_length);
		}
		$this->pk = 'cancelamentoid';

		$this->connectSmtpMail();

		$this->status_index = array(
			'solicitacao' => array(
				'title' => 'Em análise (sem protocolo)',
				'icon' => 'fa fa-list-alt',
				'desc' => 'Aqui você encontra as solicitações recebidas ainda sem protocolo.',
				'desc_single' => 'Solicitação recebida sem protocolo.'
			),
			'analise' => array(
				'title' => 'Em análise',
				'icon' => 'fa fa-list-alt',
				'desc' => 'Aqui você encontra as solicitações recebidas.',
				'desc_single' => 'Solicitação recebida.'
			),
			'atendimento' => array(
				'title' => 'Em atendimento',
				'icon' => 'fa fa-user-o',
				'desc' => 'Aqui você encontra as solicitações que têm um responsável pelo atendimento.',
				'desc_single' => 'Solicitação em atendimento.'
			),
			'revisao' => array(
				'title' => 'Em revisão',
				'icon' => 'fa fa-refresh',
				'desc' => 'Aqui você encontra os cadastros reabertos pela supervisão.',
				'desc_single' => 'Pedido reaberto pelos supervisores.'
			),
			'finalizado' => array(
				'title' => 'Finalizado',
				'icon' => 'fa fa-check',
				'desc' => 'Aqui você encontra as solicitações já finalizadas pelos operadores e com clientes já avisados.',
				'desc_single' => 'Pedido já finalizado por um operador com cliente já avisado.'
			),
			'desistencia' => array(
				'title' => 'Desistência',
				'icon' => 'fa fa-ban',
				'desc' => 'Aqui você encontra as desistências de solicitações de cancelamentos.',
				'desc_single' => 'Desistência da solicitação de cancelamento.'
			),
			'recuperado' => array(
				'title' => 'Recuperado',
				'icon' => 'fa fa-user-plus',
				'desc' => 'Aqui você encontra clientes que solicitaram o cancelamento e desistiram, continuando com o benefício.',
				'desc_single' => 'Cliente continua com o benefício após desistir do cancelamento.'
			)
		);

		$this->per_page = 60;
	}

	public function getStatus ($option = 'title', $status = false) {
		if (!$status) $status = $this->status;
		if ( isset($this->status_index[$status][$option]) )
			return $this->status_index[$status][$option];
		else
			return $status;
	}

	public function getTipoPedido () {
		$tipopedido = mb_strtoupper( $this->tipopedido, 'UTF-8' );
		switch ($tipopedido) {
			case "EXCLUSÃO DE DEPENDENTE":
				return 'exclusao';
			break;
			case "CANCELAMENTO DO CONTRATO":
			default:
				return 'cancelamento';
			break;
		}
	}

	public function temProtocolo () {
		$cpf = $this->cpf;
		$exists = new Cancelamento("cpf = '{$cpf}'");
		if (!$exists->protocolo)
			return false;
		return true;
	}
	public function geraProtocolo () {
		return str_pad($this->getID(), 6, "0", STR_PAD_LEFT) . '/' . date('Y');
	}

	public function getCurrentUrl () {
		$md5 = md5($this->getID());
		$baseurl = get_config('url');
		switch ( $this->status ) {
			case "solicitacao":
				return "{$baseurl}revisao?id={$md5}";
			break;
			case "analise":
				return "{$baseurl}analise?id={$md5}";
			break;
			default:
				return "{$baseurl}pedido?id={$md5}";
			break;
		}
	}

	public function getDocumentoHtml ($template = false, $custom_fields = false) {
		if ( !$template ) {
			switch ($this->tipopedido) {
				case "Exclusão de dependente":
					$filename = 'carta-exclusao.html';
				break;
				case "Cancelamento do contrato":
				default:
					$filename = 'carta-cancelamento.html';
				break;
			}
		}
		else {
			$filename = $template;
		}

		// Processa os dados bancários caso tenha
		// if ( $this->banco != '' ) {
		// 	$dados_bancarios = $this->getProcessedTemplate('dados-bancarios.html');
		// 	$texto_template = str_replace('{dados-bancarios-tr}', $this->getProcessedTemplate('dados-bancarios-tr.html'), $texto_template);
		// 	$texto_template = str_replace('{dados-bancarios}', $this->getProcessedTemplate('dados-bancarios.html'), $texto_template);
		// }

		$texto_template = file_get_contents(VIEW . '/forms/' . $filename);
		preg_match_all('/{(.*?)}/', $texto_template, $matches);
		// debug($matches);
		if (count($matches[0])>0) foreach($matches[1] as $field) {
			// $texto_template = str_replace('{'.$field.'}', $this->$field, $texto_template);
			if ($this->$field) {
				$texto_template = str_replace('{'.$field.'}', $this->$field, $texto_template);
			}
			elseif (isset($custom_fields[$field])) {
				$texto_template = str_replace('{'.$field.'}', $custom_fields[$field], $texto_template);
			}
			else {
				$texto_template = str_replace('{'.$field.'}', '', $texto_template);
			}
		}
		// $texto_template = strftime($texto_template);
		preg_match_all('/%(.*?)%/', $texto_template, $matches);
		if (count($matches[0])>0) foreach($matches[1] as $datestr) {
			$texto_template = str_replace('%'.$datestr.'%', strftime('%'.$datestr), $texto_template);
		}

		return $texto_template;
	}

	public function getProcessedTemplate ($filename, $custom_fields = false, $replace_fields = true, $replace_dates = true, $replace_custom_fields = false, $hide_empty_fields = true, $minify = true) {
		$template = file_get_contents(VIEW . '/forms/' . $filename);

		if ( $replace_fields ) {
			preg_match_all('/{(.*?)}/', $template, $matches);
			if (count($matches[0])>0) foreach($matches[1] as $field) {
				if ($this->$field) {
					$template = str_replace('{'.$field.'}', $this->$field, $template);
				}
				elseif (isset($custom_fields[$field])) {
					$template = str_replace('{'.$field.'}', $custom_fields[$field], $template);
				}
				elseif ( $hide_empty_fields ) {
					$template = str_replace('{'.$field.'}', '', $template);
				}
			}
		}

		if ( $replace_dates ) {
			preg_match_all('/%(.*?)%/', $template, $matches);
			if (count($matches[0])>0) foreach($matches[1] as $datestr) {
				$template = str_replace('%'.$datestr.'%', strftime('%'.$datestr), $template);
			}
		}

		// if ( $minify ) {
		// 	$template = preg_replace(
		// 	    array(
		// 	        '/ {2,}/',
		// 	        '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'
		// 	    ),
		// 	    array(
		// 	        ' ',
		// 	        ''
		// 	    ),
		// 	    $template
		// 	);
		// }

		return $template;
	}

	protected function wrapHtmlMail ($body) {
		$html  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
 		$html .= '<head>';
 		$html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
 		$html .= '<title>Nunes & Grossi</title>';
 		$html .= '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
 		$html .= '</head>';
 		$html .= '<body>';
 		$html .= $body;
 		$html .= '</body>';
 		$html .= '</html>';

 		$html = utf8_decode($html);

 		return $html;
	}

	public function connectSmtpMail () {
		require_once LIB . '/PHPMailer/PHPMailerAutoload.php';

		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = 'mail.nunesegrossi.com.br';
		// $mail->Host = 'localhost';
		$mail->Username = 'rn412@nunesegrossi.com.br';
		$mail->Password = 'gosocialcc2017';

		// $mail->SMTPSecure = false;
		// $mail->SMTPAutoTLS = false;
		// $mail->Port = 2525;
		// $mail->SMTPAuth = true;

		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;
		$mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		    )
		);
		// DEBUG!!!
		// $mail->SMTPDebug = 3;
		// $mail->Debugoutput = 'html';

		$this->mail = $mail;

		return $this->mail;
	}

	public function sendMailTest () {
		// $this->mail->setFrom('rn412@nunesegrossi.com.br', "Cancelamento - Nunes & Grossi");
		$this->mail->setFrom('rn412@nunesegrossi.com.br', 'Cancelamento - Nunes & Grossi');
		$this->mail->addAddress('johnngoes@gmail.com', 'JOAO VITOR V GOES');
		$this->mail->Subject = "Protocolo de cancelamento {$this->protocolo} - Nunes & Grossi";
		$this->mail->isHTML(true);
		$custom_fields['urlconsulta'] = get_config('url')."pedido?id=".md5($this->getID());
		$custom_fields['carta_pedido'] = $this->getProcessedTemplate('carta-mail-debug.html');
		$body = $this->getDocumentoHtml('email-protocolo.html', $custom_fields);
		$html = $this->wrapHtmlMail( $body );
		$this->mail->Body = $html;

		return $this->mail->send();
	}

	public function sendMailProtocoloCliente () {
		$this->connectSmtpMail();
		// Carrega o template do email
		$custom_fields['urlconsulta'] = get_config('url')."pedido?id=".md5($this->getID());

		$custom_fields_carta = false;
		if ( $this->banco != '' )
			$custom_fields_carta['dados-bancarios-tr'] = $this->getProcessedTemplate('dados-bancarios-tr.html');
		if ( $pedido->solicitante_nome != '' )
			$custom_fields_carta['dados-solicitante-tr'] = $this->getProcessedTemplate('dados-solicitante-tr.html');

		$custom_fields['carta_pedido'] = $this->getDocumentoHtml('carta-'.$this->getTipoPedido().'.html', $custom_fields_carta);
		$mail_template = $this->getDocumentoHtml('email-protocolo.html', $custom_fields);
		$html = $this->wrapHtmlMail( $mail_template );

		$this->mail->setFrom('rn412@nunesegrossi.com.br', 'Cancelamento - Nunes & Grossi');
		$this->mail->addAddress($this->email);
		$this->mail->isHTML(true);

		$this->mail->Subject = "Protocolo de cancelamento {$this->protocolo} - Nunes & Grossi";
		$this->mail->Body = $html;

		if($this->mail->send()) {
			return true;
			// $this->mail_protocolo = 1;
			// $this->dateupdate = date('Y-m-d G:i:s');

			// $this->save();
		}
		else {
			return false;
		}
	}

	public function sendMailProtocoloAdm () {
		$this->connectSmtpMail();
		// Carrega o template do email
		$custom_fields['datasolicitacao'] = strftime('%d/%m/%Y %Hh%M', strtotime($this->datecreate));
		$mail_template = $this->getDocumentoHtml('email-adm.html', $custom_fields);
		$html = $this->wrapHtmlMail( $mail_template );

		$this->mail->setFrom('rn412@nunesegrossi.com.br', 'Cancelamento - Nunes & Grossi');
		$this->mail->addAddress( get_config('mail_adm.to') );
		if ( get_config('mail_adm.bcc') )
			$this->mail->addBCC( get_config('mail_adm.bcc') );
		// $this->mail->addAddress('joao@gosocial.cc');
		$this->mail->isHTML(true);

		$this->mail->Subject = "Pedido de cancelamento {$this->protocolo} - Nunes & Grossi";
		$this->mail->Body = $html;

		if($this->mail->send()) {
			// $this->dateupdate = date('Y-m-d G:i:s');

			// $this->save();
			return true;
		}
		else {
			return false;
		}
	}

	public function loadByMd5 ($md5) {
		return $this->loadSingle("MD5($this->pk) = '$md5'");
	}

	public function setHistory ($action, $descricao = '') {
		$auth = new Auth;
		$user = $auth->getUsuario();
		$who = ($user) ? $user->nome : 'Usuário';
		$whoID = ($user) ? $user->getID() : 0;

		$history = new CancelamentoHistorico;
		$history->acao = $action;
		$history->descricao = $descricao;
		$history->usuarioid = $whoID;
		$history->usuarionome = $who;
		$history->datehist = date('Y-m-d H:i:s');
		$history->remoteip = $_SERVER['REMOTE_ADDR'];
		$history->remoteinfo = $_SERVER ['HTTP_USER_AGENT'];
		$history->cancelamentoid = $this->getID();

		return $history->save();
	}

	public function getHistory ($actions = false) {
		$args = array(
			'orderby' => 'datehist',
			'order' => 'DESC',
			'relation' => 'AND',
			array(
				'field' => 'cancelamentoid',
				'value' => $this->getID()
			)
		);

		if ($actions) {
			$args_action = array(
				'relation' => 'OR'
			);
			if (!is_array($actions)) $actions = array($actions);
			foreach ($actions as $action) {
				$args_action[] = array(
					'field' => 'acao',
					'value' => $action
				);
			}
			$args[] = $args_action;
		}

		$history = new CancelamentoHistorico;
		$history->loadList($args);

		return $history;
	}

	public function getHistoryHtml ($actions = false, $title = '<i class="fa fa-history" aria-hidden="true"></i> Atividade') {
		$html = array();

		$historico = $this->getHistory($actions);
		if ($historico->getList()) {
			$html[] = '<table class="table table-striped table-historico">';
			$html[] = '<thead><tr><th colspan="3">'.$title.'</th></tr></thead>';
			$html[] = '<tbody>';
			foreach ($historico->getList() as $item) {
				$html[] = '<tr>';
				$html[] = '<td class="hist-date">' . date('d/m/Y H:i', strtotime($item->datehist)) . '</td>';
				$html[] = '<td class="hist-action">'. $item->acao .' ('. $item->usuarionome .')</td>';
				if ( $item->descricao_html != '' )
					$html[] = '<td class="hist-desc"><i class="fa fa-info-circle" aria-hidden="true" title="' . $item->descricao_html . '" data-toggle="tooltip" data-placement="bottom"></i></td>';
				else
					$html[] = '<td class="hist-desc"></td>';
				$html[] = '</tr>';
			}
			$html[] = '</tbody>';
			$html[] = '</table>';
		}
		else {
			$html[] = '<div class="alert alert-info">Não há registro de interações para este pedido.</div>';
		}

		return implode('', $html);
	}

	public function getDataEntrada () {
		// Tenta achar um registro no histórico
		$args = array(
			'relation' => 'AND',
			array(
				'field' => 'cancelamentoid',
				'value' => $this->getID()
			),
			array(
				'field' => 'acao',
				'value' => 'Pedido solicitado'
			)
		);
		$history = new CancelamentoHistorico($args);

		// Se encontrar
		if ( $history->getID() ) {
			return $history->datehist;
		}

		// Senão retorna a data que o registro foi feito no bd
		return $this->datecreate;
	}

	public function isFinalizado () {
		if ( $this->status == 'finalizado' || $this->status == 'recuperado' || $this->status == 'desistencia' )
			return true;
		else
			return false;
	}

	public function getDiasEntrada ($text = false, $textSingular = 'dia', $textPlural = 'dias', $textZero = 'Hoje') {
		$now = time();
		$then = strtotime( $this->getDataEntrada() );
		if ( $this->isFinalizado() ) {
			$finalizadoList = $this->getHistory(array('Pedido finalizado', 'Pedido recuperado', 'Desistência do pedido'));
			$finalizadoEm = $finalizadoList->getList();
			$finalizadoEm = $finalizadoEm[0];
			$now = strtotime( $finalizadoEm->datehist );
			$textZero = ($textZero == 'Hoje') ? '1 dia' : $textZero;
		}

		$days = $now - $then;

		$dias = floor($days / (60 * 60 * 24));
		if ($text && $dias <= 0)
			$str = $textZero;
		elseif ($text && $dias == 1)
			$str = $dias.' '.$textSingular;
		elseif ($text && $dias > 1)
			$str = $dias.' '.$textPlural;
		else
			$str = $dias;

		return $str;
	}

	public function getDiasEntradaLabelClass () {
		if ( $this->isFinalizado() )
			return 'label-default';

		$dias = $this->getDiasEntrada();

		if ($dias <= 5)
			return 'label-success';
		elseif ($dias >= 6 && $dias <= 7)
			return 'label-warning';
		elseif ($dias >= 8)
			return 'label-danger label-blinking';
	}

	public function getDiasEntradaLabelHtml () {
		$date = date('d/m/Y H:i', strtotime($this->getDataEntrada()));
		return sprintf('<span class="label %s" title="%s" data-toggle="tooltip" data-placement="top">%s</span>', $this->getDiasEntradaLabelClass(), $date, $this->getDiasEntrada(true));
	}


	// Ações de mudança de estado do pedido
	public function statusFinalizar ($info = array()) {
		if ( $this->status == 'atendimento' || $this->status == 'revisao' ) {
			$finalizadoInfo = json_encode($info);
			if (isset( $finalizadoInfo['finalizado_cliente_avisado'] ))
				$finalizadoInfo['finalizado_cliente_avisado'] = ($finalizadoInfo['finalizado_cliente_avisado']) ? 'Sim' : 'Não';
			$this->status = 'finalizado';
			$this->dateupdate = date('Y-m-d G:i:s');
			$this->setHistory('Pedido finalizado', $finalizadoInfo);
			$this->save();

			return true;
		}
		return false;
	}

	public function statusDesistencia () {
		if ( $this->status == 'atendimento' || $this->status == 'revisao' ) {
			$this->status = 'desistencia';
			$this->dateupdate = date('Y-m-d G:i:s');
			$this->setHistory('Desistência do pedido');
			$this->save();

			return true;
		}
		return false;
	}

	public function statusRevisao () {
		if ( $this->status == 'finalizado' || $this->status == 'desistencia' || $this->status == 'recuperado' ) {
			$this->status = 'revisao';
			$this->dateupdate = date('Y-m-d G:i:s');
			$this->setHistory('Reaberto para revisão');
			$this->save();

			return true;
		}
		return false;
	}

	public function statusRecuperado () {
		if ( $this->status == 'desistencia' ) {
			$this->status = 'recuperado';
			$this->dateupdate = date('Y-m-d G:i:s');
			$this->setHistory('Pedido recuperado');
			$this->save();

			return true;
		}
		return false;
	}

	public function statusExcluido () {
		$this->status = 'excluido';
		$this->dateupdate = date('Y-m-d G:i:s');
		$this->setHistory('Pedido excluído');
		$this->save();

		return true;
	}

	protected function format () {
		if ( $this->usuarioid ) {
			$this->usuario = new Usuario;
			$this->usuario->loadByID($this->usuarioid);
			$this->usuarionome = $this->usuario->nome;
			$this->usuariologin = $this->usuario->usuario;
		}
	}

	public function temStatus ($array) {
		return $this->haveProp('status', $array);
	}

	public function getFormValues () {
		return $this->properties;
	}

}

?>