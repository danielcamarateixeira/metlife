<?php

class GSSms extends Generic {

	protected $zenvia_alias, $zenvia_pass, $zenvia_apiurl, $zenvia_facade;

	protected function config () {
		$this->table = get_config('db.prefix') . 'sms';
		$this->fields = [
			'smsid' => 'int(9) NOT NULL AUTO_INCREMENT',

			'propostaid' => 'int(9) DEFAULT NULL',
			'smsuniqid' => 'varchar(255) DEFAULT NULL',

			'celular' => 'varchar(64) DEFAULT NULL',
			'body' => 'text DEFAULT NULL',
			'schedule' => 'datetime DEFAULT NULL',
			'status' => 'int(2) DEFAULT NULL',
			'detail' => 'int(3) DEFAULT NULL',
			'exception' => 'text DEFAULT NULL',
			'exceptionDetails' => 'text DEFAULT NULL',

			'created' => 'datetime DEFAULT NULL',
			'updated' => 'datetime DEFAULT NULL'
		];
		$this->pk = 'smsid';

		$this->configureZenvia();
	}

	public function configureZenvia ($alias = 'nunesegrossi.web', $pass = 'zZAGDWQquo', $apiurl = 'https://api-rest.zenvia.com') {
		require_once LIB . '/zenvia/autoload.php';

		$this->zenvia_alias = $alias;
		$this->zenvia_pass = $pass;
		$this->zenvia_apiurl = $apiurl;
	}

	public function startZenvia () {
		$this->zenvia_facade = new SmsFacade( $this->zenvia_alias, $this->zenvia_pass, $zenvia_apiurl);
	}

	public function sendSMSGeneric ($to, $body, $propostaid = false) {
		$this->startZenvia();

		$sms = new Sms();
		$sms->setTo($to);
		$sms->setMsg($body);
		$sms->setId(uniqid());
		$sms->setCallbackOption(Sms::CALLBACK_FINAL);

		$date = new DateTime('now');
		$date->setTimeZone(new DateTimeZone('America/Sao_Paulo'));
		$schedule = $date->format("Y-m-d\TH:i:s");
		$sms->setSchedule($schedule);

		/* Cria o registro no bd */
		$this->smsuniqid = $sms->getId();
		$this->celular = $to;
		$this->body = $body;
		$this->schedule = $schedule;
		$this->created = NOW();
		$this->updated = NOW();
		if ($propostaid)
			$this->propostaid = $propostaid;

		$this->save();

		// debug($this->getProperties());
		// exit;

		try{
		    //Envia a mensagem para o webservice e retorna um objeto do tipo SmsResponse com o status da mensagem enviada
		    $response = $this->zenvia_facade->send($sms);

		    $this->status = $response->getStatusCode();
		    $this->detail = $response->getDetailCode();
		    $this->updated = NOW();
		    $this->save();

		    return $response;
		}
		catch(Exception $ex){
			$this->exception = $ex->getMessage();
			$this->exceptionDetails = $ex->getTraceAsString();
			$this->save();

			return array(
				'exception' => $ex->getMessage(),
				'details' => $ex->getTraceAsString()
			);
		    // echo "Falha ao fazer o envio da mensagem. Exceção: ".$ex->getMessage()."<br />".$ex->getTraceAsString();
		}
	}

	public function getStatus () {
		switch ($this->status) {
			case '00': return 'Ok'; break;
			case '01': return 'Agendado'; break;
			case '02': return 'Enviado'; break;
			case '03': return 'Entregue'; break;
			case '04': return 'Não recebido'; break;
			case '05': return 'Bloqueado - Sem Cobertura'; break;
			case '06': return 'Bloqueado - Lista Negra'; break;
			case '07': return 'Bloqueado - Número Inválido'; break;
			case '08': return 'Bloqueado - Conteúdo não permitido'; break;
			case '08': return 'Bloqueado - Mensagem expirou'; break;
			case '09': return 'Bloqueado'; break;
			case '10': return 'Erro'; break;
			default: return 'Indefinido'; break;
		}
	}

	public function getDetail () {
		switch ($this->detail) {
			case '000': return 'Message Sent'; break;
			case '002': return 'Message successfully canceled'; break;
			case '010': return 'Empty message content'; break;
			case '011': return 'Message body invalid'; break;
			case '012': return 'Message content overflow'; break;
			case '013': return 'Incorrect or incomplete ‘to’ mobile number'; break;
			case '014': return 'Empty ‘to’ mobile number'; break;
			case '015': return 'Scheduling date invalid or incorrect'; break;
			case '016': return 'ID overflow'; break;
			case '017': return 'Parameter ‘url’ is invalid or incorrect'; break;
			case '018': return 'Field ‘from’ invalid'; break;
			case '021': return '‘id’ fieldismandatory'; break;
			case '080': return 'Message with same ID already sent'; break;
			case '100': return 'Message Queued'; break;
			case '110': return 'Message sent to operator'; break;
			case '111': return 'Message confirmation unavailable'; break;
			case '120': return 'Message received by mobile'; break;
			case '130': return 'Message blocked'; break;
			case '131': return 'Message blocked by predictive cleansing'; break;
			case '132': return 'Message already canceled'; break;
			case '133': return 'Message content in analysis'; break;
			case '134': return 'Message blocked by forbidden content'; break;
			case '135': return 'Aggregate is Invalid or Inactive'; break;
			case '136': return 'Message expired'; break;
			case '140': return 'Mobile number not covered'; break;
			case '141': return 'International sending not allowed'; break;
			case '145': return 'Inactive mobile number'; break;
			case '150': return 'Message expired in operator'; break;
			case '160': return 'Operator network error'; break;
			case '161': return 'Message rejected by operator'; break;
			case '162': return 'Message cancelled or blocked by operator'; break;
			case '170': return 'Bad message'; break;
			case '171': return 'Bad number'; break;
			case '172': return 'Missing parameter'; break;
			case '180': return 'Message ID notfound'; break;
			case '190': return 'Unknown error'; break;
			case '200': return 'Messages Sent'; break;
			case '210': return 'Messages scheduled but Account Limit Reached'; break;
			case '240': return 'File empty or not sent'; break;
			case '241': return 'File too large'; break;
			case '242': return 'File readerror'; break;
			case '300': return 'Received messages found'; break;
			case '301': return 'No received messages found'; break;
			case '400': return 'Entity saved'; break;
			case '900': return 'Authentication error'; break;
			case '901': return 'Account type not support this operation.'; break;
			case '990': return 'Account Limit Reached – Please contact support'; break;
			case '998': return 'Wrong operation requested'; break;
			case '999': return 'Unknown Error'; break;
			default: return 'Indefinido'; break;
		}
	}

}