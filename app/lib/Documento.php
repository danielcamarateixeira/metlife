<?php

// require_once LIB . '/dompdf/autoload.inc.php';
// require_once LIB . '/dompdf/lib/html5lib/Parser.php';
// require_once LIB . '/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
// require_once LIB . '/dompdf/lib/php-svg-lib/src/autoload.php';
// require_once LIB . '/dompdf/src/Autoloader.php';
// Dompdf\Autoloader::register();
// use Dompdf\Dompdf;

class Documento
{

	private $template, $template_raw;
	public 	$custom_fields, $fields, $prefix;

	public function __construct ($file) {
		if ( file_exists($file) )
			$this->template = file_get_contents($file);
	}

	public function setRule ($field, $value) {
		$this->template_raw = $this->template = str_replace( $field, $value, $this->template );
	}

	public function parseFields () {
		preg_match_all('/{{(.*?)}}/', $this->template, $matches);
		if (count($matches[0])>0) foreach($matches[1] as $field) {
			// debug($field);
			// debug($this->fields->$field,1);
			// debug('---');
			// debug('Tentando preencher o campo '.$this->prefix.$field.' com o valor '.$this->fields->{$this->prefix.$field});
			if ($this->fields->$field || $this->fields->{$this->prefix.$field})
				$this->template = str_replace('{{'.$field.'}}', $this->fields->{$this->prefix.$field}, $this->template);
			elseif (isset($this->custom_fields[$field]))
				$this->template = str_replace('{{'.$field.'}}', $this->custom_fields[$field], $this->template);
			else
				$this->template = str_replace('{{'.$field.'}}', 'n/a', $this->template);
		}
	}

	public function parseDates () {
		preg_match_all('/%%(.*?)%%/', $this->template, $matches);
		if (count($matches[0])>0) foreach($matches[1] as $datestr)
			$this->template = str_replace('%%'.$datestr.'%%', strftime('%'.$datestr), $this->template);
	}

	public function parseConstants () {
		preg_match_all('/\[\[(.*?)\]\]/', $this->template, $matches);
		if (count($matches[0])>0) foreach($matches[1] as $const)
			$this->template = str_replace('[['.$const.']]', constant($const), $this->template);
	}

	public function parseMethods () {
		preg_match_all('/{\((.*?)\)}/', $this->template, $matches);
		if (count($matches[0])>0) foreach($matches[1] as $method)
			$this->template = str_replace('{('.$method.')}', $this->fields->{$method}(), $this->template);
	}

	public function minify () {
		$this->template = preg_replace( "/\r|\n/", "", $this->template );
	}

	public function process () {
		$this->parseFields();
		$this->parseDates();
		$this->parseConstants();
		$this->parseMethods();
		$this->minify();

		return $this->template;
	}

	public function getHtml () {
		return $this->process();
	}

	public function getPDF () {
		$dompdf = new Dompdf();
	}

}

/*

public function getDocumento ($custom_fields = false) {
		if ( !$custom_fields ) $custom_fields = array();

		// Adiciona os forms
		$json_forms = array('metlife-titular', 'metlife-dependentes', 'metlife-plano');
		foreach ($json_forms as $form) {
			$json_forms_html = '';
			$parse_json_fields = new Form(TEMPLATES . "/{$form}.json");
			$fields_array = array();
			foreach ( $parse_json_fields->getFields() as $field ) {
				// $fields_array[$field['column_name']] = '<span>'.$field['column_label'].'</span><br><strong>'.$this->$field['column_name'].'</strong>';
				$fields_array[$field['column_name']] = '<span>'.$field['column_label'].'</span><br><strong>{{'.$field['column_name'].'}}</strong>';
			}
			$fields_html = '<table width="100%"><tr><td>' . implode('</td></tr><tr><td>', $fields_array) . '</td></tr></table>';
			$json_forms_html .= $fields_html;
			$custom_fields[$form] = $json_forms_html;
		}

		// Adiciona os dependentes
		if ( $this->{'quantidade-dependentes'} > 0 ) {
			$deps = array();
			for ( $i=1; $i<=$this->{'quantidade-dependentes'}; $i++ ) {
				$deps_tpl = $this->getTemplate( 'proposta-dependente.phtml', array('depnum'=>$i), "dep{$i}-" );
				$deps[] = $deps_tpl;
			}
			$custom_fields['dependentes'] = implode($deps);
		}

		return $this->getTemplate( 'proposta.phtml', $custom_fields );
	}

	public function getTemplate ($file, $custom_fields = false, $prefix = false) {
		$filepath = TEMPLATES . "/{$file}";
		if ( file_exists($filepath) ) {
			$html = file_get_contents( $filepath );

			// Preenche os campos
			preg_match_all('/{{(.*?)}}/', $html, $matches);
			if (count($matches[0])>0) foreach($matches[1] as $field) {
				if ($this->$field)
					$html = str_replace('{{'.$field.'}}', $this->{$prefix.$field}, $html);
				elseif (isset($custom_fields[$field]))
					$html = str_replace('{{'.$field.'}}', $custom_fields[$field], $html);
				else
					$html = str_replace('{{'.$field.'}}', 'n/a', $html);
			}

			// Preenche as datas
			preg_match_all('/%%(.*?)%%/', $html, $matches);
			if (count($matches[0])>0) foreach($matches[1] as $datestr)
				$html = str_replace('%%'.$datestr.'%%', strftime('%'.$datestr), $html);

			// Preenche com constantes do sistema
			preg_match_all('/\[\[(.*?)\]\]/', $html, $matches);
			if (count($matches[0])>0) foreach($matches[1] as $const)
				$html = str_replace('[['.$const.']]', constant($const), $html);

			$html = preg_replace( "/\r|\n/", "", $html );
			return $html;
		}
		else
			return false;
	}

	*/

?>