<?php

class Auth
{

	public $cookies;

	public function __construct () {
		$this->cookies = array(
			'logado' => md5('MetlifeLogado'),
			'usuario' => md5('MetlifeUsuario'),
			'senha' => md5('MetlifeSenha'),
			'expira' => time() + 60*60*24*6
		);
	}

	public function isLogged () {
		if (isset($_SESSION['logged']))
			return $_SESSION['logged'];
		elseif ($this->haveCookies())
			return $this->login( $this->getCookie('usuario'), $this->getCookie('senha') );

		return false;
	}

	public function login ($usuario, $senha, $lembrar = false) {
		$user = new Usuario(array(
			'relation' => 'AND',
			array(
				'field' => 'usuario',
				'value' => $usuario
			),
			array(
				'field' => 'senha',
				'value' => md5($senha)
			)
		));

		if ($user->getID()) {
			$_SESSION['logged'] = true;
			$_SESSION['userid'] = $user->getID();
			$_SESSION['permissao'] = $user->permissao;

			if ($lembrar) {
				$this->setCookie( 'logado', 1, false );
				$this->setCookie( 'usuario', $usuario );
				$this->setCookie( 'senha', $senha );
			}
			else {
				$this->deleteCookie('logado');
				$this->deleteCookie('usuario');
				$this->deleteCookie('senha');
			}

			$user->lastlogin = date('Y-m-d H:i:s');
			$user->save();

			return true;
		}

		return false;
	}

	public function logout () {
		$_SESSION['logged'] = false;
		unset($_SESSION['userid']);
	}

	public function getUsuario () {
		if (!$this->isLogged())
			return false;

		$user = new Usuario;
		$user->loadByID($_SESSION['userid']);
		return $user;
	}

	public function haveCookies () {
		return isset( $_COOKIE[ $this->cookies['logado'] ] );
	}

	public function getCookie ($rawvalue) {
		return base64_decode( $_COOKIE[ $this->cookies[$rawvalue] ] );
	}

	public function setCookie ($name, $value = null, $encode = true) {
		if ($encode) $value = base64_encode($value);
		setCookie( $this->cookies[$name], $value, $this->cookies['expira'] );
	}

	public function deleteCookie ($name) {
		unset($_COOKIE[ $this->cookies[$name] ]);
	}

}

?>