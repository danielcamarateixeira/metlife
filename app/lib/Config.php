<?php

/* Config.php
 * ----------------------------------
 * Classe singleton para configuração do site
 * 
 * Essa classe será responsável por fazer com que
 * sempre seja retornada alguma informação de configuração
 * caso ela for solicitada.
 *
 * Exemplo de utilização:
 * 
 * 1. Solicita alguma informação
 *	Config::get('option.name');
 * Onde option.name é a configuração.
 * 
 * Caso ocorra alguma grande mudança no decorrer do script
 * e as configurações precisem ser atualizadas, basta chamar
 * o method reload(). Exemplo:
 *
 *  Config::reload();
 */


class Config {

	private static	$instance,
					$properties,
                    $json_default, $json_app;

	// O método singleton 
    public static function getInstance ()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }
    private function __construct () {
    	self::init();
    }

    private static function init () {
        self::$json_default = 'default';
        self::$json_app = 'app';

    	self::loadConfig( self::$json_default );
    	self::loadConfig(MODE);
        self::loadConfig( self::$json_app );
        self::format();
    }

    public static function get ($option) {
    	self::getInstance();

        $option_pieces = explode('.', $option);
        $count_pieces = count($option_pieces);
        $return = self::$properties;

        foreach ($option_pieces as $piece) {
            if (!array_key_exists($piece, $return)) return false;
            $return = $return[$piece];
        }

        return $return;
    }

    public static function set ($option, $value) {
        self::getInstance();

        // Pega o arquivo json
        $JSON = self::parseFile( self::$json_app );
        if (!$JSON) $JSON = array();
        // Determina qual a propriedade a ser definida
        $option_pieces = explode('.', $option);
        $option_final = $value;
        foreach ( array_reverse($option_pieces) as $item) {
            $option_final = array($item => $option_final);
        }

        $JSON = array_merge_recursive_distinct( $JSON, $option_final );
        self::writeFile($JSON, self::$json_app);
    }

    private static function loadConfig ($mode, $overwrite = true, $extension = 'json') {
    	$JSON = self::parseFile($mode, $extension);
        if (!$JSON) return false;

    	if ($overwrite && null !== self::$properties)
    		self::$properties = array_merge_recursive_distinct( self::$properties, $JSON );
    	else
    		self::$properties = $JSON;
    }

    private static function format () {
        if (null === self::$properties) return false;

        if ( array_key_exists('url', self::$properties) ) {
            $url = self::$properties['url'];
            $url_pieces = explode(':', $url);
            $url_rel = substr($url_pieces[1], 2);
            $url_rel_pieces = explode('/', $url_rel);
            array_shift($url_rel_pieces);
            $url_rel_final = '/' . implode('/', $url_rel_pieces);

            self::$properties['relative_url'] = $url_rel_final;
            self::$properties['url_start'] = count($url_rel_pieces);
        }
    }

    private static function parseFile ($filename, $extension = 'json') {
    	$PATH_FILE = CONFIG . "/{$filename}.{$extension}";
    	if ( file_exists($PATH_FILE) ) {
    		$content = file_get_contents($PATH_FILE);
    		$json = json_decode($content, true);

    		return $json;
    	}
    	return false;
    }

    private static function writeFile ($data, $filename, $extension = 'json') {
        $PATH_FILE = CONFIG . "/{$filename}.{$extension}";
        $JSON = json_encode($data, JSON_PRETTY_PRINT);
        if ( file_put_contents($PATH_FILE, $JSON) )
            return true;
        else
            return false;
    }

}


function config ($option) {
    echo get_config($option);
}
function get_config ($option) {
    return Config::get($option);
}
function set_config ($option, $value) {
    return Config::set($option, $value);
}