<?php

class CorretoraEntidade extends Generic
{

	protected function config () {
		$this->table = get_config('db.prefix') . 'corretora_entidade';
		$this->fields = [
			'corretoraentidadeid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'corretoraid' => 'int(9) NOT NULL',
			'entidadeid' => 'int(9) NOT NULL'
		];
		$this->pk = 'corretoraentidadeid';
	}

	protected function format () {
		$this->entidade = new Entidade;
		$this->entidade->loadByID( $this->entidadeid );
	}

}