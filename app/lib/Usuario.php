<?php

class Usuario extends Generic
{

	protected function config () {
		$this->table = get_config('db.prefix') . 'usuarios';
		$this->fields = [
			'userid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'corretoraid' => 'int(9) NOT NULL',
			'cargo' => 'varchar(32) NOT NULL',
			'nome' => 'varchar(64) NOT NULL',
			'usuario' => 'varchar(64) NOT NULL',
			'senha' => 'varchar(32) NOT NULL',
			'telefone' => 'varchar(32) NOT NULL',
			'celular' => 'varchar(32) NOT NULL',
			'email' => 'varchar(64) NOT NULL',
			'cpf' => 'varchar(64) NOT NULL',
			'verificado_data' => 'datetime NOT NULL',
			'datecreated' => 'datetime NOT NULL',
			'lastlogin' => 'datetime NOT NULL',
			'firstlogin' => 'datetime NOT NULL',
			'avatar' => 'varchar(255) NOT NULL'
		];
		$this->pk = 'userid';
	}

	protected function format () {
	}

	/**
	 * Verifica se tem determinada permissão
	 * $list: valor procurado, pode ser string ou array
	 * 
	 * Permissões pré-determinadas:
	 * - vendedor (antigo corretor)
	 * - corretora
	 * - administradora
	 * - desenvolvedor
	 * - backoffice
	 * - cadastro
	 */
	public function temPermissao ($list) {
		return $this->haveProp('cargo', $list);
	}

	public function isVerified () {
		if ( $this->verificado_data != '0000-00-00 00:00:00' )
			return true;

		return false;
	}

	public function verifiedStatus () {
		return ( $this->isVerified() ) ? 'verificado' : 'pendente';
	}

	public function getAvatar () {
		if ( $this->avatar != '' )
			return $this->avatar;
		else
			return get_image( 'user-default.jpg' );
	}

	public function getCargo () {
		return mb_convert_case($this->cargo, MB_CASE_TITLE);
	}


}