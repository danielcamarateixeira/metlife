<?php

/**
 * Form
 * Versão 2.0
 *
 * Patch Notes
 * ---
 * 2.1 - todo
 * - Permitir que a classe controle os itens e/ou o formulário em JSON ANTES de gerar o html.
 * 2.0
 * - Substituído método de geração de HTML para DomDocument
 * - Adicionado comentários ao longo do código
 * 1.0
 * - Permite ler arquivos JSON e retornar o HTML do formulário pronto
 */

class Form
{
	protected 	$json_raw, $json, $values,
				$fields_index, $fields_name_index, $DOM;

	/**
	 * Ao iniciar a classe já carrega o arquivo json de formulário que ela irá manipular
	 *
	 * Exemplo de uso:
	 * > $form = new Form( TEMPLATES . '/form.json' );
	 */
	public function __construct ($filepath, $values = false) {
		$this->DOM = new DOMDocument('1.0');
		$this->loadFile( $filepath );
		$this->setValues($values);
		$this->parseJson();
	}

	/**
	 * Carrega o arquivo e lê o JSON
	 */
	public function loadFile ($filepath) {
		if ( file_exists($filepath) ) {
			$this->json_raw = file_get_contents( $filepath );
			$this->json = json_decode( $this->json_raw, true );
		}
	}

	/**
	 * Percorre o JSON e atualiza os parametros da classe
	 */
	public function parseJson () {
		foreach ($this->json[0] as $name => $element) {
			$this->parseElement( $name, $element );
		}
	}

	/**
	 * Percorre o elemento encontrado
	 * Versão 2.0: 
	 */
	protected function parseElement ($name, $element) {
		switch( $this->getElementType($element) ) {
			case 'group':
				$this->parseGroup( $name, $element ); break;
			case 'field':
				$this->parseField( $name, $element );
		}
	}

	/**
	 * Percorre um grupo de campos
	 */
	protected function parseGroup ($name, $group) {
		// Container do grupo
		$container = !$this->isFalse( 'container', $group ) ? $this->DOM->createElement('div') : $this->DOM;
		if ( !$this->isFalse( 'container', $group ) )
			$container->setAttribute('class', 'form-group-container row ' . $name);

		// Adiciona o título caso exista
		if ( $this->isTrue('showtitle', $group) ) {
			$title = $this->DOM->createElement('h2', $this->getProp('title', $group));
			$title->setAttribute('class', 'form-group-title');

			$container->appendChild($title);
		}

		if ( $this->getProp('fields', $group) ) {
			$field_group_list = array();
			$prefix = $this->getProp('prefix', $group);
			if ( $repeat = $this->getProp('repeat', $group) )
				for ( $i=1; $i<=$repeat; $i++ )
					$field_group_list[] = $this->parseFieldGroup( $group['fields'], $name, $name.$i.'-' );
			else
				$field_group_list[] = $this->parseFieldGroup( $group['fields'], $name, $prefix );

			foreach ( $field_group_list as $field_group ) {
				$container->appendChild( $field_group );
			}
		}

		if ( !$this->isFalse( 'container', $group ) )
			$this->DOM->appendChild($container);
	}

	/**
	 * Percorre o array com o grupo de campos.
	 * Essa função é necessária para o funcionamento da repetição de campos (propriedade "repeat").
	 * Retorna o DOM do grupo de campos.
	 */
	protected function parseFieldGroup ($fields, $group_name = false, $prefix = false) {
		$container = $this->DOM->createElement('div');
		$container->setAttribute('class', "form-field-group clearfix {$group_name} {$prefix}");
		if ( $group_name )
			$container->setAttribute('data-group', $group_name);
		if ( $prefix )
			$container->setAttribute('data-prefix', $prefix);

		if ( count($fields) > 0 ) foreach ($fields as $name => $field) {
			if ($prefix)
				$field['prefix'] = $prefix;
			if ($group_name)
				$field['group'] = $group_name;

			$container->appendChild( $this->parseField($name, $field) );
		}

		return $container;
	}

	/**
	 * Percorre o campo e chama o método correspondente ao mesmo.
	 * Retorna o DOM do input.
	 */
	protected function parseField ($name, $field) {
		$field['name'] = $this->getProp('prefix', $field) . $name;
		$container = $this->DOM->createElement('div');
		$containerclass = array('form-group', 'form-group-'.$field['name']);
		if ( $this->getProp('containerclass', $field) )
			$containerclass[] = $this->getProp('containerclass', $field);
		else
			$containerclass[] = 'col-xs-12';
		$container->setAttribute('class', implode(' ', $containerclass));
		

		if ( $field['type'] == 'button' || $field['type'] == 'submit' )
			$field['showlabel'] = false;

		if ( !$this->isFalse('showlabel', $field) && $this->getProp('label', $field) ) {
		// if ( $this->getProp('label', $field) ) {
			$label = $this->DOM->createElement('label', $field['label']);
			$label_class = array('label-title');
			if ( !$this->isFalse('required', $field) )
				$label_class[] = 'label-required';
			$label->setAttribute('class', implode(' ', $label_class));
			$container->appendChild($label);
		}

		// Adiciona ao index
		$this->field_index[] = $field;
		$this->field_name_index[] = $name;

		if ( $this->getProp('alert', $field) ) {
			$alert_type = ($this->getProp('alert-type', $field)) ? $this->getProp('alert-type', $field) : 'warning';
			$alert = $this->DOM->createElement('div');
			$alert->setAttribute('class', 'alert alert-sm alert-'.$alert_type);
			$alert_text = $this->DOM->createDocumentFragment();
			$alert_text->appendXML( $this->getProp('alert', $field) );
			$alert->appendChild( $alert_text );
			$container->appendChild($alert);
		}

		if ( $field['type'] == 'radio' || $field['type'] == 'checkbox' )
			$input = $this->parseRadioCheckbox($field);
		elseif ( $field['type'] == 'select' )
			$input = $this->parseSelect($field);
		elseif ( $field['type'] == 'textarea' )
			$input = $this->parseTextarea($field);
		elseif ( $field['type'] == 'button' || $field['type'] == 'submit' )
			$input = $this->parseButton($field);
		else
			$input = $this->parseText($field);

		// Adiciona os addon caso tenha
		if ( $this->getProp('addonl', $field) || $this->getProp('addonr', $field) ) {
			$addon_container = $this->DOM->createElement('div');
			$addon_container->setAttribute('class', 'input-group');

			if ($addonl = $this->getProp('addonl', $field)) {
				$addon = $this->DOM->createElement('span', $addonl);
				$addon->setAttribute('class', 'input-group-addon');
				$addon_container->appendChild( $addon );
			}

			$addon_container->appendChild( $input );

			if ($addonr = $this->getProp('addonr', $field)) {
				$addon = $this->DOM->createElement('span', $addonr);
				$addon->setAttribute('class', 'input-group-addon');
				$addon_container->appendChild( $addon );
			}
			$input = $addon_container;
		}

		$container->appendChild($input);
		return $container;
	}

	/**
	 * Normaliza o campo "options", permitindo duas utilizações no JSON:
	 *
	 * Simples:
	 * 	["option 1", "option 2"]
	 * 
	 * Extendido:
	 * 	[
	 *		{
	 *			"value": "",
	 *			"label": ""
	 *		}
	 * 	]
	 */
	protected function parseOptions ($options) {
		$new_options = array();
		
		foreach ($options as $key => $option) {
			if ( !is_array($option) )
				$new_options[$option] = $option;
			else
				$new_options[$option['value']] = $option['label'];
		}

		return $new_options;
	}

	/**
	 * Percorre tanto o campo de Radio quanto o de Checkbox por serem parecidos.
	 * Retorna o DOM do container do input.
	 */
	protected function parseRadioCheckbox ($field) {
		$container = $this->DOM->createElement('div');
		$container->setAttribute('class', "form-{$field['type']}-group");

		if ( $this->getProp('title', $field) ) {
			$title = $this->DOM->createElement('h4', $field['title']);
			$title_class = array('label-title');

			if ( !$this->isFalse('required', $field) )
				$title_class[] = 'label-required';				

			$title->setAttribute('class', implode(' ', $title_class));
			$container->appendChild($title);
		}

		if ( $this->getProp('options', $field) ) {
			$i = 0;
			foreach ($this->parseOptions($this->getProp('options', $field)) as $value => $label) {
				$container_input = $this->DOM->createElement('label');
				if ( $this->getProp('inline', $field) )
					$container_input->setAttribute('class', "{$field['type']}-inline");

				$input = $this->DOM->createElement('input');
				$input->setAttribute('type', $field['type']);

				if ( $field['type'] == 'radio' )
					$input->setAttribute('name', $field['name']);
				elseif ( $field['type'] == 'checkbox' )
					$input->setAttribute('name', $field['name'].'[]');

				$input->setAttribute('value', $value);
				$input->setAttribute('id', "{$field['name']}_{$field['group']}_{$i}");

				// REQUIRED
				if ( !$this->isFalse('required', $field) && $field['type'] == 'radio' )
					$input->setAttribute('required', 'required');

				if ( isset($this->values[$field['name']]) )
					if ($value == $this->values[$field['name']])
						$input->setAttribute('checked', 'checked');

				$label = $this->DOM->createTextNode( $label );
				$container_input->appendChild( $input );
				$container_input->appendChild( $label );

				if ( !$this->getProp('inline', $field) ) {
					$container_input_div = $this->DOM->createElement('div');
					$container_input_div->setAttribute('class', $field['type']);
					$container_input_div->appendChild( $container_input );
					$container_input = $container_input_div;
				}

				$container->appendChild($container_input);
				$i++;
			}
		}

		return $container;
	}

	/**
	 * Percorre os campos do tipo SELECT.
	 * Retorna o DOM do container do select.
	 */
	protected function parseSelect ($field) {
		$select = $this->DOM->createElement('select');
		$select->setAttribute('id', "{$field['name']}_{$field['group']}");
		$select->setAttribute('name', $field['name']);
		$field = $this->setClass($field);
		if ( $this->getProp('options', $field) )
			foreach ($this->parseOptions($this->getProp('options', $field)) as $value => $label) {
				$option = $this->DOM->createElement('option', $label);
				$option->setAttribute('value', $value);

				if ( isset($this->values[$field['name']]) )
					if ($value == $this->values[$field['name']])
						$option->setAttribute('selected', 'selected');

				$select->appendChild($option);
			}

		$this->placeAttributes($select, $field);

		return $select;
	}

	/**
	 * Percorre os campos do tipo TEXTAREA.
	 * Retorna o DOM do container do textarea.
	 */
	protected function parseTextarea ($field) {
		$textarea = $this->DOM->createElement('textarea');
		$textarea->setAttribute('id', "{$field['name']}_{$field['group']}");
		$textarea->setAttribute('name', $field['name']);
		$field = $this->setClass($field);

		$text = false;
		if ( isset($field['value']) )
			$text = $this->DOM->createTextNode( $field['value'] );

		// Define o valor caso seja carregado
		if ( isset($this->values[$field['name']]) ) {
			if( isset($field['value']) ) unset($field['value']);
			$text = $this->DOM->createTextNode( $this->values[$field['name']] );
		}

		if ( $text )
			$textarea->appendChild($text);

		$this->placeAttributes($textarea, $field);
		return $textarea;
	}

	protected function parseButton ($field) {
		$button = $this->DOM->createElement('button', $field['label']);
		$button->setAttribute('type', $this->getProp('type', $field));
		$this->placeAttributes($button, $field);

		return $button;
	}

	/**
	 * Percorre os campos do tipo TEXTO ou qualquer outro que não seja especificado.
	 * Retorna o DOM do container do input.
	 */
	protected function parseText ($field) {
		$input = $this->DOM->createElement('input');
		$input->setAttribute('type', $this->getProp('type', $field));
		$input->setAttribute('id', "{$field['name']}_{$field['group']}");
		$input->setAttribute('name', $field['name']);
		$field = $this->setClass($field);

		// Define o valor caseo seja carregado
		if ( isset($this->values[$field['name']]) ) {
			if( isset($field['value']) ) unset($field['value']);
			// $value = ( $this->isTrue('raw', $field) ) ? $this->values[$field['name'].'_raw'] : $this->values[$field['name']];
			$value = $this->values[$field['name']];
			$input->setAttribute('value', $value);
		}

		$this->placeAttributes($input, $field);

		return $input;
	}

	/**
	 * Retorna o campo com as classes ajustadas.
	 */
	protected function setClass ($field, $custom = false) {
		if (!$custom) $custom = array();
		elseif (!is_array($custom)) $custom = explode(' ', $custom);

		$custom[] = 'form-control';
		$custom[] = "input-{$field['name']}";

		if( isset($field['attributes']['class']) )
			$field['attributes']['class'] .= ' ' . implode(' ', $custom);
		else
			$field['attributes']['class'] = implode(' ', $custom);

		return $field;
	}

	/**
	 * Retorna o tipo do elemento, se não for especificado retorna como field.
	 */
	protected function getElementType ($element) {
		return ( isset($element['type']) ) ? $element['type'] : 'field';
	}

	/**
	 * Adiciona no objeto do DOM informado os atributos passados pelo JSON.
	 * Os atributos podem ser passados direto no escopo do "field".
	 * Exemplo:
	 * "field": {
	 *	"required": true,
	 *  "mask": "00/00/0000"
	 * }
	 * Ou passados no escopo de "attributes".
	 * Exemplo:
	 * "field": {
	 *	"type": "date",
	 *	"attributes": {
	 *		"min": "1",
	 *		"max": {
	 *			"strtotimef": "next month"
	 *		}
	 * 	}
	 * }
	 * 
	 */
	protected function placeAttributes ($dom, $field) {
		// REQUIRED
		if ( !$this->isFalse('required', $field) )
			$field['attributes']['required'] = 'required';

		// MASK
		if ( $this->getProp('mask', $field) )
			$field['attributes']['data-mask'] = $field['mask'];

		// PLACEHOLDER
		if ( $this->getProp('placeholder', $field) )
			$field['attributes']['placeholder'] = $field['placeholder'];

		// VALUE
		if ( $this->getProp('value', $field) )
			$field['attributes']['value'] = $field['value'];

		// DATA-
		if ( $this->getProp('data', $field) ) {
			if ( count($field['data']) > 0 ) foreach ($field['data'] as $attr => $value)
				$field['attributes']['data-'.$attr] = $value;
		}

		// CSS CLASS
		if ( $this->getProp('class', $field) ) {
			if ( isset($field['attributes']['class']) )
				$field['attributes']['class'] .= ' ' . $field['class'];
			else
				$field['attributes']['class'] = $field['class'];
		}

		if ( $this->getProp('attributes', $field) )
			foreach ( $field['attributes'] as $attr => $value ) {
				if ( is_array($value) )
					$dom->setAttribute($attr, call_user_func($value[0], $value[1]));
				else
					$dom->setAttribute($attr, $value);
			}
	}

	/**
	 * Verifica se a propriedade informada é verdadeira somente se ela existir.
	 * Retorna true se existir e for verdadeira.
	 */
	protected function isTrue ($index, $element) {
		if ( isset($element[$index]) )
			return (bool) $element[$index];
		else
			return false;
	}
	/**
	 * Verifica se a propriedade informada é falsa.
	 * Retorna true (propriedade = false) se for explicitamente informada e o valor falso.
	 * Retorna falso (propriedade = true) se não for explicitamente informada.
	 */
	protected function isFalse ($index, $element) {
		if ( isset( $element[$index] ) && $element[$index] === false )
			return true;
		else 
			return false;
	}

	/**
	 * Retorna a propriedade caso ela existe, se não retorna falso.
	 * Evita erros no PHP caso a propriedade não exista no elemento.
	 */
	protected function getProp ($prop, $element) {
		if ( isset($element[$prop]) )
			return $element[$prop];
		else
			return false;
	}

	/**
	 * Retorna um possível label (ou título) do campo.
	 * Seguindo a seguinte ordem:
	 * 1. Procura a propriedade "label", se não encontrar:
	 * 2. Procura a propriedade "title", se não encontrar:
	 * 3. Usa o campo "name" capitalizado e espaços no lugar dos caracteres "-" e "_".
	 */
	protected function getLabel ($field) {
		if ( $label = $this->getProp('label', $field) )
			return $label;
		elseif ( $title = $this->getProp('title', $field) )
			return $title;
		else
			return str_replace(array('-','_'), ' ', mb_convert_case($this->getProp('name', $field), MB_CASE_TITLE, "UTF-8"));
	}

	public function setValues ($values) {
		$this->values = $values;
	}

	/**
	 * Retorna o HTML do formulário completo
	 */
	public function getHtml () {
		return $this->DOM->saveHTML();
	}

	/**
	 * Retorna todos os campos do formulário como um array.
	 * A diferença para o JSON é que os campos estão fora do escopo do grupo e com as propriedades atualizadas.
	 */
	public function getFields () {
		return $this->field_index;
	}

	/**
	 * Retorna todos os nomes dos campos do formulário num array.
	 */
	public function getFieldsName () {
		return $this->field_name_index;
	}
}

/**
 * Função auxiliar para retornar a data já formatada usando o strtotime.
 */
function strtotimef ($date) {
	return date('Y-m-d', strtotime($date));
}

?>