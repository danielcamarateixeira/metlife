<?php

class PropostaHistorico extends Generic
{

	protected $dictionary;

	protected function config () {
		$this->table = get_config('db.prefix') . 'proposta_historico';
		$this->fields = [
			'historicoid' => 'int(32) NOT NULL AUTO_INCREMENT',
			'propostaid' => 'int(32) NOT NULL',
			'usuarioid' => 'int(32) NOT NULL',
			'usuarionome' => 'varchar(64) NOT NULL',
			'datehist' => 'datetime NOT NULL',
			'remoteip' => 'varchar(26) NOT NULL',
			'remoteinfo' => 'text NOT NULL',
			'acao' => 'varchar(64) NOT NULL',
			'descricao' => 'text NOT NULL'
		];
		$this->pk = 'historicoid';

		$this->dictionary = array(
			'finalizado_dataemail' => 'E-mail enviado em',
			'finalizado_observacoes' => 'Observações',
			'alteracao_dataemail' => 'E-mail enviado em',
			'alteracao_observacoes' => 'Observações'
		);

		$this->per_page = 1000;
	}

	protected function format () {
		foreach ($this->dictionary as $label => $title) {
			$this->descricao = str_replace($label, $title, $this->descricao);
		}
		$this->descricao_html = $this->descricao;
		if ( ($this->descricao != '') && is_json ($this->descricao) ) {
			$desc_json = json_decode( $this->descricao );
			$desc_list = array();
			foreach ($desc_json as $label => $text) {
				if ($text != '') {
					$desc_list[] = "{$label}: {$text}";
				}
			}
			$desc_html = implode('
', $desc_list);
			$this->descricao_html = $desc_html;
		}
	}

}

?>