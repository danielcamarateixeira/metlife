<?php

/**
 * Status:
 * - vendedor
 * - backoffice
 * - cadastro
 * - analise
 * - implantado
 * - indeferida
 * - arquivada	
 *
 * Etapa:
 * - corretora
 * - entidade
 * - titular
 * - dependentes
 * - plano
 * - filiacao
 * - adesao
 * - documentos
 * - finalizacao
 *
 * Assinatura:
 * - Digital (campo assinatura_digital)
 * - Manuscrita
 * - Depois
 *
 * Observações
 * - Formato JSON
 * - Exemplo: 
 * 		[
 *			{
 *				"usuarioid": 1,
 *				"usuarionome": "Nome do usuario",
 *				"usuariocargo": "Cargo do usuário",
 *				"data": "yyyy-mm-dd H:i:s",
 *				"status": "Status antes da observação ser enviada",
 *				"novo_status": "Novo status após ser enviado",
 *				"observacoes": {
 *					"etapa": "Texto da observação",
 *					...
 *				}
 *			},
 			...
 *		]
 */

class Proposta extends Generic
{
	protected $etapas, $etapa, $mail, $formatted_fields;

	protected function config () {
		$this->table = get_config('db.prefix') . 'proposta';
		$this->fields = [
			'propostaid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'usuarioid' => 'int(9) DEFAULT NULL',
			'entidadeid' => 'int(9) DEFAULT NULL',
			'corretoraid' => 'int(9) DEFAULT NULL',
			'planoid' => 'int(9) DEFAULT NULL',
			'hash' => 'varchar(32) DEFAULT NULL',

			'protocolo' => 'varchar(32) DEFAULT NULL',
			'status' => 'varchar(64) DEFAULT NULL',
			'etapa' => 'varchar(64) DEFAULT NULL',
			'datecreate' => 'datetime DEFAULT NULL',
			'dateupdate' => 'datetime DEFAULT NULL',
			'remoteip' => 'varchar(26) DEFAULT NULL',
			'remoteinfo' => 'text DEFAULT NULL',
			'observacoes' => 'text DEFAULT NULL',
			'observacoes_adm' => 'text DEFAULT NULL',
			'campos' => 'text DEFAULT NULL',

			'mail_cliente_analise' => 'bool DEFAULT NULL',
			'sms_cliente_aceite' => 'int(1) DEFAULT NULL'
			/* 0 = não enviado; 1 = enviado; 2 = aceite confirmado */
		];
		$this->pk = 'propostaid';

		$this->connectSMTP();
	}

	public function geraProtocolo () {
		if ( !$this->protocolo )
			return $this->protocolo = str_pad( $this->getID(), 6, '0', STR_PAD_LEFT);
	}

	public function geraHash () {
		if ( !$this->hash )
			return $this->hash = md5( uniqid($this->getID()) );
	}

	protected function format () {
		$this->usuario = new Usuario;
		$this->usuario->loadByID( $this->usuarioid );
		$this->usuario_nome = $this->usuario->nome;
		$this->usuario_telefone = $this->usuario->telefone;
		$this->usuario_cpf = $this->usuario->cpf;
		

		$this->entidade = new Entidade;
		$this->entidade->loadByID( $this->entidadeid );
		$this->entidade_nome = $this->entidade->nome;

		$this->plano = new Plano;
		$this->plano->loadByID( $this->planoid );
		$this->plano_nome = $this->plano->nome;

		$this->corretora = new Corretora;
		$this->corretora->loadByID( $this->corretoraid );
		$this->corretora_nome = $this->corretora->nome;
		$this->corretora_cod = $this->corretora->codigo;

		$this->vidas = $this->{'quantidade-dependentes'} + 1;

		$this->deserializeFields();
		$this->formatFields();

		$this->datecreatef = date('d/m/Y', strtotime($this->datecreate));
		$this->datecreatedoc = strftime('%d de %B de %Y', strtotime($this->datecreate));
	}

	protected function formatFields () {

		if ($this->vigencia) {
			$this->vigenciaf = '01/' . $this->vigencia;
			$this->{'vigencia-inicio'} = $this->vigenciaf;
		}

		return true;

		// Cria um index com os campos formatados para resetá-los depois
		$this->formatted_fields = array('vencimento-primeira-mensalidade', 'vigencia', 'vigencia-inicio');

		// Evita que os campos sejam formatados mais de uma vez
		foreach ( $this->formatted_fields as $field )
			if ( $this->{$field . '_raw'} && $this->{$field . '_raw'} != '' ) return false;
			// if ( $this->{$field . '_raw'} && $this->{$field . '_raw'} != '' ) break;

		// Cria os campos originais (raw) para que seja possível reaproveitá-los
		foreach ( $this->formatted_fields as $field )
			if ($this->$field) $this->{$field . '_raw'} = $this->$field;

		if ($this->{'vencimento-primeira-mensalidade'}) $this->{'vencimento-primeira-mensalidade'} = date('d/m/Y', strtotime($this->{'vencimento-primeira-mensalidade'}));
		if ($this->{'vigencia-inicio'}) $this->{'vigencia-inicio'} = date('d/m/Y', strtotime($this->{'vigencia-inicio'}));

		if ($this->vigencia) $this->vigencia = '01/' . $this->vigencia;
	}

	protected function unformatFields () {
		return false;
		// Procura todos os campos com _raw no final
		$raw  = preg_grep ("/(.*)_raw/", $this->getPropertiesNames());
		if ($raw) foreach($raw as $fieldraw) {
			if ( $this->$fieldraw != '' && $this->$fieldraw == $this->{ str_replace('_raw', '', $fieldraw) } ) {
				// debug('Original field ('.$fieldraw.'): '.$this->{ str_replace('_raw', '', $fieldraw) }.PHP_EOL.'RAW Field: '.$this->$fieldraw);
				$this->{ str_replace('_raw', '', $fieldraw) } = $this->$fieldraw;
				// debug('unformatted setting: '.str_replace('_raw', '', $fieldraw).' = '.$this->{$fieldraw});
				unset($this->{$fieldraw});
			}
		}

		/* Método antigo, meio bugado */
		/*
		if ( $this->formatted_fields ) foreach ( $this->formatted_fields as $field ) {
			if ( $this->$field . '_raw' ) {
				// debug('unformat: '.$field.'; raw: '.$this->{$field.'_raw'}.'; formated: '.$this->$field);
				$this->$field = $this->{$field . '_raw'};
				unset($this->{$field.'_raw'});
			}
		}
		*/
	}

	public function serializeFields () {
		// Primeiro deve preservar os campos já salvos em JSON
		$campos = ($this->campos != '') ? json_decode( $this->campos, true ) : array();
		$to_serialize = array();
		$this->unformatFields();

		// Percorre todos os campos na classe e serializa todos os que não são campos no bd
		foreach ($this->properties as $name => $value) {
			if ( !array_key_exists($name, $this->fields) && is_string($value) && strpos($name, '_raw') === false )
				$to_serialize[$name] = $value;
		}
		$this->campos = json_encode( array_merge($campos, $to_serialize) );
	}

	public function deserializeFields () {
		if ( $this->campos != '' ) {
			$campos = json_decode( $this->campos, true );
			$this->properties = array_merge($this->properties, $campos);
		}
	}

	public function orderByField ($field) {
		usort($this->list, function($a, $b) use ($field) {
			return strcasecmp($a->get($field), $b->get($field));
		});
	}

	public function getHash () {
		return $this->hash;
	}

	public function loadByHash ($hash) {
		$args = array(
			'field' => 'hash',
			'value' => $hash
		);
		return $this->loadSingle( array($args) );
	}

	public function getIdade ($data) {
		$past = DateTime::createFromFormat('d/m/Y', $data);
		$present = new DateTime('now');
		$diff = $present->diff($past);
		return $diff->y;
	}

	public function getData ($format = 'd/m/Y') {
		$data = new DateTime( $this->datecreate );
		return $data->format($format);
	}

	public function getVigencia () {
		if ($this->{'vigencia-inicio'})
			return $this->{'vigencia-inicio'};
		return false;
	}

	public function getStatus () {
		if ( $this->status == 'backoffice' )
			return 'Digitação';
		
		return mb_convert_case($this->status, MB_CASE_TITLE);
	}

	public function setPostData ($post) {
		// debug($post);
		if ( count($post) > 0 ) foreach ( $post as $field => $value ) {
			$field_value = is_array($value) ? implode(';', $value) : trim($value);

			// if ($field == 'cpf' || $field == 'rg' || $field == 'cep' || $field == 'correspondencia-cep')
			// if ( in_array( array('cpf','rg','cep','correspondencia-cep'), $field ) )
			// 	$field_value = preg_replace("/[^0-9]/", "", $field_value);

			$this->$field = mb_strtoupper($field_value, 'UTF-8');
		}
		$this->formatFields();
	}

	public function getDocumentosPendentes () {
		$pendencias = array();
		foreach ( $this->getDocumentosObrigatorios() as $docs ) {
			if ( !$this->$docs || $this->$docs == '' )
				$pendencias[] = $docs;
		}
		return (count($pendencias)>0) ? $pendencias : false;
	}

	public function getDocumentosObrigatorios () {
		$docs = array(
			'doc-titular-identidade',
			'doc-titular-residencia'
		);
		if ( $this->{'tipo-assinatura'} && $this->{'tipo-assinatura'} == 'Digital' )
			$docs[] = 'doc-assinatura-digital';
		if ( $this->{'quantidade-dependentes'} && $this->{'quantidade-dependentes'} > 0 ) for ( $i=1; $i<=$this->{'quantidade-dependentes'}; $i++ ) {
			$docs[] = 'doc-dependente'.$i.'-identidade';
			// $docs[] = 'doc-dependente'.$i.'-residencia';
			$docs[] = 'doc-dependente'.$i.'-vinculo';
		}
		return $docs;
	}

	public function getTodosDocumentos () {
		$prefix = 'doc-';
		$docs = array();
		foreach ( $this->getProperties() as $prop => $value ) {
			if (strncmp($prop, $prefix, strlen($prefix)) === 0)
				$docs[$prop] = $value;
		}
		return count($docs)>0 ? $docs : false;
	}

	public function getAutoDocName ($slug) {
		return mb_convert_case( trim(str_replace(array('-','doc'), array(' ', ''), $slug)), MB_CASE_TITLE, "UTF-8" );
	}

	/* Callback chamado antes de salvar
	 * Aqui deve eliminar os campos que são do formulário de $properties
	 * e serializar os dados atualizados para a propriedade "campos"
	 */
	protected function beforeSave () {
		$this->unformatFields();
	}

	public function getDocumentoPDF () {
		$path = UPLOADS . '/proposta/' . $this->getID();
		$filepath = $path . '/proposta.pdf';
		if ( !file_exists($filepath) ) {
			require_once LIBS . 'dompdf/autoload.inc.php';
		}

		return UPLOADS_REL . '/proposta/' . $this->getID() . '/proposta.pdf';
	}

	public function getDocumento () {
		$doc = new Documento( TEMPLATES . '/proposta.phtml' );
		if ( $this->{'quantidade-dependentes'} > 0 ) {
			$dep_html = array();
			for ($i=1; $i<=$this->{'quantidade-dependentes'}; $i++) { 
				$dep_tpl = new Documento( TEMPLATES . '/proposta-dependente.phtml' );
				$dep_tpl->fields = $this;
				$dep_tpl->custom_fields = array(
					'depnum' => $i
				);
				$dep_tpl->prefix = "dependente{$i}-";
				$dep_html[] = $dep_tpl->getHtml();
			}
			$doc->custom_fields['dependentes'] = implode('', $dep_html);
		}
		$doc->fields = $this;
		$doc->process();
		return $doc;
	}

	public function temFichaFiliacao () {
		return ( $this->filiado == 'NÃO' ) ? true : false;
	}

	public function getDocumentoFiliacao () {
		if ( $this->filiado == 'NÃO' ) {
			$doc = new Documento( TEMPLATES . '/adesao-'.slugify($this->entidade->nome).'.phtml' );
			if ( $this->{'quantidade-dependentes'} > 0 ) {
				$dep_html = array();
				for ($i=1; $i<=$this->{'quantidade-dependentes'}; $i++) { 
					$dep_tpl = new Documento( TEMPLATES . '/adesao-'.slugify($this->entidade->nome).'-dependentes.phtml' );
					$dep_tpl->fields = $this;
					$dep_tpl->prefix = "dependente{$i}-";
					$dep_html[] = $dep_tpl->getHtml();
				}
				$doc->custom_fields['dependentes'] = implode('', $dep_html);
			}
			$doc->fields = $this;
			$doc->process();
			return $doc;
		}
		return false;
	}

	public function getPropertiesJs () {
		$buffer = '<script type="text/javascript">';
		$vars = array();
		foreach ( $this->getProperties() as $field => $prop ) {
			$field = str_replace('-', '_', $field);
			if ( is_string($prop) )
				$vars[] = sprintf('%s = "%s"', $field, $prop);
		}
		$buffer .= 'var ' . implode(', ', $vars) . ';';
		$buffer .= 'console.log($protocolo);';
		$buffer .= '</script>';

		return $buffer;
	}

	/**
	 * Métodos das Observações internas e para o vendedor
	 */
	public function setObservacoes ($observacoes, $novostatus) {
		global $currentuser;
		// Verifica se alguma observação foi preenchida ou está tudo vazio
		if ($this->isEmptyObservacaoField($observacoes)) return false;
		// Verifica se as observações atuais estão vazias
		$obs = ( $this->observacoes == '' ) ? array() : json_decode( $this->observacoes, 1 );
		// Adiciona a nova observação
		$obs[] = array(
			'usuarioid' => $currentuser->getID(),
			'usuarionome' => $currentuser->nome,
			'usuariocargo' => $currentuser->cargo,
			'data' => date('Y-m-d H:i:s'),
			'status' => $this->status,
			'novo_status' => $novostatus,
			'observacoes' => $observacoes
		);
		// Serializa o campo novamente com as observações novas
		$this->observacoes = json_encode( $obs );
	}

	public function setObservacoesAdm ($observacoes, $novostatus) {
		global $currentuser;
		// Verifica se alguma observação foi preenchida ou está tudo vazio
		if ($this->isEmptyObservacaoField($observacoes)) return false;
		// Verifica se as observações atuais estão vazias
		$obs = ( $this->observacoes_adm == '' ) ? array() : json_decode( $this->observacoes_adm, 1 );
		// Adiciona a nova observação
		$obs[] = array(
			'usuarioid' => $currentuser->getID(),
			'usuarionome' => $currentuser->nome,
			'usuariocargo' => $currentuser->cargo,
			'data' => date('Y-m-d H:i:s'),
			'status' => $this->status,
			'novo_status' => $novostatus,
			'observacoes' => $observacoes
		);
		// Serializa o campo novamente com as observações novas
		$this->observacoes_adm = json_encode( $obs );
	}

	public function isEmptyObservacaoField ($observacoes) {
		if(is_array($observacoes)) {
			foreach ($observacoes as $observacao)
				if ($observacao != '')
					return false;
		}
		elseif ($observacoes != '')
			return false;

		return true;
	}

	public function getObservacoes ($order = 'default', $orderby = 'ASC', $adm = false) {
		$field = ($adm) ? $this->observacoes_adm : $this->observacoes;
		if (!$field) return false;
		if ($order == 'data') {
			$obs_data = array();
			foreach ($obs as $item)
				$obs_data[$item['data']] = $item;
			ksort($obs_data);
			$obs = $obs_data;
		}
		else
			$obs = json_decode($field,1);

		if ($orderby != 'ASC')
			$obs = array_reverse($obs);

		return $obs;
	}

	public function getLastObservacao ($adm = false, $status_novo = false) {
		$field = ($adm) ? $this->observacoes_adm : $this->observacoes;
		if (!$field) return false;
		$obs_list = $this->getObservacoes('default', 'ASC', $adm);
		$last = end( $obs_list );
		if ($status_novo) {
			if ($status_novo != $last['novo_status'])
				return false;
		}
		return $last;
	}

	public function getObservacoesAdm ($order = 'default', $orderby = 'ASC') {
		return getObservacoes($order, $orderby, $adm);
	}

	public function getLastObservacaoHtml ($adm = false) {
		$field = ($adm) ? $this->observacoes_adm : $this->observacoes;
		if (!$field) return false;
		$obs = $this->getLastObservacao($adm);
		return $this->getObservacaoHtml($obs);
	}

	public function getLastObservacaoPlainText ($adm = false) {
		$field = ($adm) ? $this->observacoes_adm : $this->observacoes;
		if (!$field) return false;
		$obs = $this->getLastObservacao($adm);
		return $this->getObservacaoPlainText($obs);
	}

	public function getLastObservacaoEtapaHtml ($etapa, $novo_status = false, $status = false) {
		if (!$this->observacoes) return false;
		$obs = $this->getLastObservacao();
		if ($novo_status && $obs['novo_status'] != $novo_status) return false;
		if ($status && $obs['status'] != $status) return false;
		return $this->getObservacaoEtapaHtml($obs, $etapa);
	}

	public function getLastObservacaoEtapaListHtml ($delimiter = ', ') {
		if (!$this->observacoes) return false;
		$obs = $this->getLastObservacao();
		$etapas = array();
		foreach ($obs['observacoes'] as $etapa => $item)
			if ($item != '')
				$etapas[] = mb_convert_case($etapa, MB_CASE_TITLE);
		return implode($delimiter, $etapas);
	}
	public function getLastObservacaoEtapaLink () {
		if (!$this->observacoes) return false;
		$obs = $this->getLastObservacao();
		$etapas = array();
		foreach ($obs['observacoes'] as $etapa => $item)
			if ($item != '')
				$etapas[] = mb_convert_case($etapa, MB_CASE_TITLE);
		return get_url('proposta/'.mb_strtolower($etapas[0]).'?id='.$this->getID());
	}

	public function getObservacaoPlainText ($obs) {
		$text = '';
		if (is_array($obs['observacoes'])) {
			foreach ($obs['observacoes'] as $etapa => $texto) {
				if ($texto != '')
					$text .= sprintf('<strong>%s</strong><br>'.PHP_EOL.'%s'.PHP_EOL, mb_strtoupper($etapa), $texto);
			}
		}
		else
			if ($obs['observacoes'] != '')
				$text .= $obs['observacoes'].PHP_EOL;

		return $text;
	}
	public function getObservacaoHtml ($obs) {
		$html = '<div class="alert alert-danger alert-observacao '.$obs['status'].'">';
		$html .= '<span class="pull-right observacoes-title-section">Observações</span>';
		$html .= sprintf('<h4 class="observacao-title">%s <i class="fa fa-angle-right"></i> %s</h4>', mb_strtoupper($obs['status']), mb_strtoupper($obs['novo_status']));
		$html .= sprintf('<p class="observacao-meta"><span class="data">%s</span> &bull; <span class="username">%s</span></p><hr>',
							date('d/m/Y H:i', strtotime($obs['data'])),
							$obs['usuarionome']);
		$html .= '<p class="observacao"><i class="fa fa-quote-left"></i>';
		if (is_array($obs['observacoes'])) {
			foreach ($obs['observacoes'] as $etapa => $texto) {
				if ($texto != '')
					$html .= sprintf('<strong>%s</strong>: %s', mb_strtoupper($etapa), $texto);
			}
		}
		else
			if ($obs['observacoes'] != '')
				$html .= $obs['observacoes'];
		$html .= '</p>';
		$html .= '</div>';
		return $html;
	}
	public function getObservacaoEtapaHtml ($obs, $etapa) {
		if ( $obs['observacoes'][$etapa] == '' ) return false;
		$html  = '<div class="alert alert-danger alert-observacao '.$obs['status'].'">';
		$html .= '<span class="pull-right observacoes-title-section">Observações</span>';
		$html .= sprintf('<p class="observacao-etapa">%s</p>', $obs['observacoes'][$etapa]);
		$html .= sprintf('<hr><p class="observacao-meta"><span class="data">%s</span> &bull; <span class="username">%s</span> &bull; <span class="status">%s <i class="fa fa-angle-right"></i> %s</span></p>',
							date('d/m/Y H:i', strtotime($obs['data'])),
							$obs['usuarionome'],
							mb_convert_case($obs['status'], MB_CASE_TITLE),
							mb_convert_case($obs['novo_status'], MB_CASE_TITLE));
		$html.= '</div>';
		return $html;
	}
	public function getObservacoesHtml ($order = 'default', $orderby = 'ASC', $adm = false) {
		$observacoes = $this->getObservacoes($order, $orderby, $adm);
		$html = '';
		if (!$observacoes) return false;
		foreach($observacoes as $obs)
			$html .= $this->getObservacaoHtml($obs);
		return $html;
	}
	public function haveObservacaoEtapa ($etapa) {
		$observacoes = $this->getObservacoes('default', 'ASC');
		foreach ($observacoes as $obs) {
			if ($obs['observacoes'][$etapa] != '') return true;
		}
		return false;
	}

	/**
	 * Métodos de envio dos e-mails
	 */

	public function connectSMTP () {
		require_once LIB . '/PHPMailer/PHPMailerAutoload.php';

		$mail = new PHPMailer;
		$mail->isSMTP();
		// $mail->Host = 'smtp.mandic.net.br';
		$mail->Host = 'smtp-cluster.idc2.mandic.com.br';
		$mail->Username = 'metlifesys@nunesegrossi.com.br';
		$mail->Password = 'Gosocial@2018';

		$mail->SMTPAuth = true;
	    // $mail->SMTPSecure = 'ssl';
	    // $mail->Port = 465;
	    $mail->SMTPSecure = 'tls';
	    $mail->Port = 587;
	    $mail->SMTPOptions = array(
	        'ssl' => array(
	            'verify_peer' => false,
	            'verify_peer_name' => false,
	            'allow_self_signed' => true
	        )
	    );

	    $mail->setFrom('metlifesys@nunesegrossi.com.br', 'METLIFE - Nunes & Grossi');

		$this->mail = $mail;
		return $this->mail;
	}

	public function sendMailEmAnalise () {
		if ( $this->mail_cliente_analise == 1 ) return false;
		
		$cliente = $this->sendMailClienteEmAnalise();
		$adminstradora = $this->sendMailAdministradoraAnalise();
		$corretora = $this->sendMailCorretoraAnalise();

		if ( $cliente ) {
			$this->mail_cliente_analise = 1;
			$this->save();
		}
	}

	public function sendMailImplantada () {
		if ( $this->mail_corretora_implantada == 1 ) return false;

		$corretora = $this->sendMailCorretoraImplantada();

		if ( $corretora ) {
			$this->mail_corretora_implantada = 1;
			$this->save();
		}
	}

	public function sendMailGeneric ($template, $to = false, $subject = 'Proposta METLIFE - Nunes & Grossi') {
		$this->connectSMTP();
		// Processa o template
		$tpl = new Documento( TEMPLATES . "/{$template}" );
		$tpl->fields = $this;
		$tpl->process();
		$html = $tpl->getHtml();

		if ( !$to || !$to[0] || $to[0] == '' ) return false;
		if (MODE == 'dev' || MODE == 'sandbox')
			$this->mail->addAddress('joao+metlifesandbox@gosocial.cc');
		else
			$this->mail->addAddress($to[0], $to[1]);
			
		$this->mail->Subject = $subject;
		$this->mail->isHTML(true);
		$this->mail->Body = utf8_decode( $html );

		if ( $this->mail->send() ) return true;
		else return false;
	}

	public function sendMailClienteEmAnalise () {
		return $this->sendMailGeneric('mail-cliente-analise.phtml', array($this->email, $this->nome));
	}

	public function sendMailAdministradoraAnalise () {
		return $this->sendMailGeneric('mail-administradora-analise.phtml', array('conferencia@nunesegrossi.com.br', 'METLIFE - Nunes & Grossi'), "Proposta METLIFE #".$this->protocolo." - Nunes & Grossi");
	}

	public function sendMailCorretoraAnalise () {
		return $this->sendMailGeneric('mail-corretora-analise.phtml', array($this->corretora->email, $this->corretora->nome), "Proposta METLIFE #".$this->protocolo." - Nunes & Grossi");
	}

	public function sendMailCorretoraImplantada () {
		return $this->sendMailGeneric('mail-corretora-implantada.phtml', array($this->corretora->email, $this->corretora->nome), "Proposta METLIFE #".$this->protocolo." Implantada - Nunes & Grossi");
	}

	public function sendMailCadastro () {
		return $this->sendMailGeneric('mail-cadastro-analise.phtml', array('cadastro@nunesegrossi.com.br', 'CADASTRO METLIFE - Nunes & Grossi'), "Proposta METLIFE #".$this->protocolo." - Nunes & Grossi");
	}

	public function sendMailVendedorRetorno () {
		return $this->sendMailGeneric(
					'mail-vendedor-retorno.phtml',
					array($this->usuario->email, $this->usuario->nome),
					"Revisar Proposta METLIFE #".$this->protocolo." - Nunes & Grossi"
				);
	}

	public function sendMailTest () {

		$this->mail->setFrom('metlifesys@nunesegrossi.com.br', 'METLIFE - Nunes & Grossi');
		$this->mail->addAddress('joao@gosocial.cc', 'João');
		$this->mail->Subject = "Proposta METLIFE - Nunes & Grossi - ".date('Y-m-d H:i');
		$this->mail->isHTML(true);
		$html = $this->getDocumento();
		$this->mail->Body = utf8_decode( $html->getHtml() );

		return $this->mail->send();
	}


	/**
	 * Métodos de SMS
	 */
	public function getCelularInt () {
		return '+55' . preg_replace('/[^0-9]/', '', $this->celular);
	}
	public function sendSmsAceite () {
		$sms = new GSSms;
		$sms->startZenvia();
		$send = $sms->sendSMSGeneric( $this->getCelularInt(), 'Prezado '.$this->nome.', o n. de protocolo da sua proposta é '.$this->protocolo, $this->getID() );
		$this->sms_cliente_aceite = 1;
		$this->save();
		return $send;
	}


	/**
	 * Métodos que registram as interações com esta proposta no histórico
	 */

	public function setHistory ($action, $descricao = '') {
		$auth = new Auth;
		$user = $auth->getUsuario();
		$who = ($user) ? $user->nome : 'Usuário';
		$whoID = ($user) ? $user->getID() : 0;

		$history = new PropostaHistorico;
		$history->acao = $action;
		$history->descricao = $descricao;
		$history->usuarioid = $whoID;
		$history->usuarionome = $who;
		$history->datehist = date('Y-m-d H:i:s');
		$history->remoteip = $_SERVER['REMOTE_ADDR'];
		$history->remoteinfo = $_SERVER ['HTTP_USER_AGENT'];
		$history->propostaid = $this->getID();

		return $history->save();
	}

	public function getHistory ($actions = false) {
		$args = array(
			'orderby' => 'datehist',
			'order' => 'DESC',
			'relation' => 'AND',
			array(
				'field' => 'propostaid',
				'value' => $this->getID()
			)
		);

		if ($actions) {
			$args_action = array(
				'relation' => 'OR'
			);
			if (!is_array($actions)) $actions = array($actions);
			foreach ($actions as $action) {
				$args_action[] = array(
					'field' => 'acao',
					'value' => $action
				);
			}
			$args[] = $args_action;
		}

		$history = new PropostaHistorico;
		$history->loadList($args);

		return $history;
	}

	public function getHistoryHtml ($actions = false, $title = '<i class="fa fa-history" aria-hidden="true"></i> Atividade') {
		$html = array();

		$historico = $this->getHistory($actions);
		if ($historico->getList()) {
			$html[] = '<table class="table table-striped table-historico">';
			$html[] = '<thead><tr><th colspan="3">'.$title.'</th></tr></thead>';
			$html[] = '<tbody>';
			foreach ($historico->getList() as $item) {
				$html[] = '<tr>';
				$html[] = '<td class="hist-date">' . date('d/m/Y H:i', strtotime($item->datehist)) . '</td>';
				$html[] = '<td class="hist-action">'. $item->acao .' ('. $item->usuarionome .')</td>';
				if ( $item->descricao_html != '' )
					$html[] = '<td class="hist-desc"><i class="fa fa-info-circle" aria-hidden="true" title="' . $item->descricao_html . '" data-toggle="tooltip" data-placement="bottom"></i></td>';
				else
					$html[] = '<td class="hist-desc"></td>';
				$html[] = '</tr>';
			}
			$html[] = '</tbody>';
			$html[] = '</table>';
		}
		else {
			$html[] = '<div class="alert alert-info">Não há registro de interações para esta proposta.</div>';
		}

		return implode('', $html);
	}

}

function get_vigencia () {
	global $proposta;
	if (!$proposta->vigencia) return '';
	$vigencia = date_create_from_format('d/m/Y', $proposta->vigenciaf);
	return date_format($vigencia, 'Y-m-d');
	// return date('Y-m-d', strtotime($proposta->vigenciaf));
}

?>