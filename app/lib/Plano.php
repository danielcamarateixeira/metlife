<?php

class Plano extends Generic 
{
	public $entidade, $valor;

	protected function config () {
		$this->table = get_config('db.prefix') . 'planos';
		$this->fields = [
			'planoid' => 'int(9) NOT NULL AUTO_INCREMENT',
			'nome' => 'varchar(64) NOT NULL',
			'codigo' => 'int(9) NOT NULL'
		];
		$this->pk = 'planoid';

		$this->valor = new PlanoValor;
		$this->entidade = new Entidade;
	}

	public function getValor () {
		$this->valor->loadSingle(array(
			'relation' => 'AND',
			array(
				'field' => 'entidadeid',
				'value' => $this->entidade->getID()
			),
			array(
				'field' => 'planoid',
				'value' => $this->getID()
			)
		));

		return $this->valor;
	}

	public function setEntidade ($entidadeid) {
		$this->entidade->loadByID($entidadeid);
	}
}

?>