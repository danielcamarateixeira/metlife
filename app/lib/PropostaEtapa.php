<?php 

/*
 * Controla as etapas da proposta e o HTML.
 * V1: Exibe botões antes e depois do formulário.
 * V2: Exibe as etapas numa listagem no topo do formulário. (NOV/19)
 */

class PropostaEtapa
{

    public $etapas, $etapas_ordem;
	protected 	$etapas_raw, $etapas_json, $current, $linkable,
				$DOM;

    public function __construct ($file = false) {

    	if (!$file) $file = TEMPLATES . '/etapas.json';
        $this->linkable = true;
    	$this->DOM = new DOMDocument('1.0');
    	$this->loadEtapas( $file );

    }

    public function loadEtapas ($file) {
    	if ( file_exists($file) ) {
    		$this->etapas_raw = file_get_contents( $file );
    		$this->etapas_json = json_decode( $this->etapas_raw, true );
    		$this->etapas = array();
    		foreach ( $this->etapas_json as $item ) {
    			$this->etapas[$item['slug']] = $item;
    			$this->etapas_ordem[$item['ordem']] = $item;
    		}
    	}
    }

    public function setCurrent ($slug) {
    	$this->current = $this->etapas[$slug];
    }

    public function getCurrent () {
        return $this->current;
    }
    public function getCurrentSlug () {
        return $this->current['slug'];
    }

    public function getEtapaHtml ($slug) {
        global $proposta;

        $li_class = 'etapa-item etapa-item-'.$slug;
        $a_href = get_url( 'proposta/' . $slug . '?id='.$_GET['id'] );
        $name = $this->getNome($slug);

        if ( $slug == $this->getCurrentSlug() )
            $li_class .= ' etapa-item-current';
        if ($proposta->getLastObservacaoEtapaHtml($slug)) {
            $obs_title = 'Existem observações referentes à esta etapa, clique para saber mais';
            $obs = sprintf('<i class="fa fa-warning etapa-observacao-icon" aria-hidden="true" title="%s"></i>', $obs_title);
            $name .= ' ' . $obs;
        }

        $a_element = sprintf('<a href="%s">%s</a>', $a_href, $name);
        $text_element = $name;

        $inner = ( $this->linkable ) ? $a_element : $text_element;
        $element = sprintf('<li class="%s">%s</li>', $li_class, $inner);
        return $element;

        /*
    	// return sprintf('<div class="navbar-etapa"><div class="container">%s <i class="fa fa-caret-right" aria-hidden="true"></i></div></div>', $this->getNome($slug));
    	$DOM = new DOMDocument('1.0');
    	$etapa = $DOM->createElement('div');
    	$etapa->setAttribute('class', 'navbar-etapa');

    	$inner_container = $DOM->createElement('div');
    	$inner_container->setAttribute('class', 'container');

        $inner_link = $DOM->createElement('a');
        $inner_link->setAttribute('href', get_config('url').'proposta/'.$slug.'?id='.$_GET['id']);

        // Verifica se há alguma notificação (observação) para esta etapa
        $observacao = false;
        if ($proposta->getLastObservacaoEtapaHtml($slug)) {
            $observacao = $DOM->createElement('i');
            $observacao->setAttribute('class', 'fa fa-warning etapa-observacao-icon');
            $observacao->setAttribute('aria-hidden', 'true');
            $observacao->setAttribute('title', 'Existem observações referentes à esta etapa, clique para saber mais');
        }

    	$etapa_name = $DOM->createTextNode($this->getNome($slug) . ' ');
    	$etapa_caret = $DOM->createElement('i');
    	$etapa_caret->setAttribute('class', 'fa fa-caret-right');
    	$etapa_caret->setAttribute('aria-hidden', 'true');

        if ( $this->linkable ) {
            if ($observacao) $inner_link->appendChild( $observacao );
            $inner_link->appendChild( $etapa_name );
            $inner_link->appendChild( $etapa_caret );

            $inner_container->appendChild( $inner_link );
        }
        else {
            if ($observacao) $inner_link->appendChild( $observacao );
    	   $inner_container->appendChild( $etapa_name );
    	   $inner_container->appendChild( $etapa_caret );
        }

    	$etapa->appendChild( $inner_container );
    	$DOM->appendChild($etapa);

    	return $DOM->saveHTML();
        */
    }

    public function getBeforeCurrentHtml () {
        $html = '<div class="navbar-etapa"><div class="container"><ul class="etapa-list">';
        foreach ( $this->etapas_ordem as $etapa ) {
            $html .= $this->getEtapaHtml( $etapa['slug'] );
        }
        $html .= '</ul></div></div>';

        return $html;

        /*
    	$i = 1;
    	$html = array();
    	while ( $i <= $this->current['ordem'] ) {
    		if ( isset( $this->etapas_ordem[$i] ) )
    			$html[] = $this->getEtapaHtml( $this->etapas_ordem[$i]['slug'] );
    		$i++;
    	}

    	return implode('', $html);
        */
    }

    public function getAfterCurrentHtml () {
        /*
    	$i = $this->current['ordem']+1;
    	$html = array();
    	while ( $i <= count($this->etapas) ) {
    		if ( isset($this->etapas_ordem[$i]) )
    			$html[] = $this->getEtapaHtml( $this->etapas_ordem[$i]['slug'] );
    		$i++;
    	}

    	return implode('', $html);
        */
    }

    public function getNome ($slug) {
    	return $this->etapas[$slug]['nome'];
    }

}

?>