<?php

include '_auth.php';

$accordion = array(
	0 => array(
		'id' => 'coletar-assinaturas',
		'title' => 'Como coletar assinaturas?',
		'text' => 'Existem 3 tipos de coleta de assinatura. Digital, onde o corretor deve enviar o arquivo digital da assinatura. Manuscrita, onde o cliente deve assinar em papel a mão e o corretor deverá entregar a Administradora de Benefícios.'
	),
	1 => array(
		'id' => 'vender-plano',
		'title' => 'Como vender um plano Metlife?',
		'text' => 'Para vender um plano, sugere-se abordagem com educação e boas vestes. Apresenta-se os benefícios do plano de saúde, explicando seus detalhes e orientando-o a preencher junto ao corretor todos os dados corretamente. '
	),
	2 => array(
		'id' => 'atender-bem',
		'title' => 'Como atender bem?',
		'text' => 'Para atender bem o corretor deve transmitir credibilidade, confiabilidade e ter boa dicção para expressar os benefícios dos planos oferecidos.'
	),
	3 => array(
		'id' => 'boa-impressao',
		'title' => 'Como causar uma boa impressão?',
		'text' => 'Para causar uma boa impressão, deve-se atentar-se a postura, higiene pessoal e vestes adequadas à ocasião.'
	),
	4 => array(
		'id' => 'o-que-vestir',
		'title' => 'O que vestir para transmitir credibilidade?',
		'text' => 'Sugere-se trajes sociais, acompanhando camisa e material de identificação de corretor.'
	),
	5 => array(
		'id' => 'sistema',
		'title' => 'Como usar o sistema Metlife Nunes e Grossi?',
		'text' => 'Para usar o sistema, realize seu login que pode ser consultado junto à Admnistradora de benefícios. Acesse e clique em Nova proposta para preencher os dados junto ao cliente, colete as assunaturas e envie a via do cliente. Para ver suas últimas propostas, clique em HISTÓRICO. Para aprender como melhorar seu atendimento, visite boas práticas.  Em HISTÓRICO também é possível anexar assinaturas que não foram coletadas no ato de prospecção.'
	)
);

get_header(); ?>

<div class="faq">

	<header class="proposta-header">
		<div class="container">
			<div class="row row-flex align-center">
				<div class="col-xs-10">
					<h1>FAQ &amp; Boas práticas</h1>
				</div>
				<div class="col-xs-2 text-center">
					<a class="menu-bar btn btn-outline btn-default" data-toggle="collapse" href="#main-nav">
			            <i class="fa fa-bars" aria-hidden="true"></i>
			        </a>
				</div>
			</div>
		</div>
	</header>

	<nav class="navbar navbar-proposta">
		<div class="collapse main-nav" id="main-nav">
		    <ul class="nav navbar-nav">
		        <li><a href="<?php echo get_config('url'); ?>proposta/nova" class="btn btn-success">NOVA PROPOSTA</a></li>
				<li><a href="<?php echo get_config('url'); ?>proposta/historico" class="btn btn-primary btn-light">HISTÓRICO</a></li>
				<li><a href="<?php echo get_config('url'); ?>faq" class="btn btn-primary btn-light">FAQ & BOAS PRÁTICAS</a></li>
				<li><a href="<?php echo get_config('url'); ?>sair" class="btn btn-primary btn-light">SAIR</a></li>
		    </ul>   
		</div>
	</nav>

	<div class="container" style="margin-top:2em;">

		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

			<?php foreach ($accordion as $item) : ?>

		  		<div class="panel panel-default">
		    		<div class="panel-heading" role="tab" id="heading-<?php echo $item['id']; ?>">
		      			<h4 class="panel-title">
		        			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#panel-<?php echo $item['id']; ?>" aria-expanded="true" aria-controls="panel-<?php echo $item['id']; ?>">
		          				<?php echo $item['title']; ?>
		        			</a>
		      			</h4>
		    		</div>
		    		<div id="panel-<?php echo $item['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $item['id']; ?>">
		      			<div class="panel-body">
		        			<p><?php echo $item['text']; ?></p>
		      			</div>
		    		</div>
		  		</div>

		  	<?php endforeach; ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>