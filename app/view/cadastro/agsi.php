<?php

$proposta = new Proposta;
$proposta->loadByID($_GET['id']);
if (!$proposta->getID()) die('Proposta inexistente');


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=AGSI METLIFE - Proposta '.$proposta->protocolo.'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));

function get_agsi_estado_civil ($proposta, $dep = false) {
	$estado_civil = ($dep) ? $proposta->{'dependente'.$dep.'-estado-civil'} : $proposta->{'estado-civil'};
	if ($estado_civil == 'DESQUITADO')
		return 'E';

	return substr($estado_civil, 0, 1);
}
function get_agsi_dep_grau_parentesco ($proposta, $dep) {
	if ( $proposta->{'dependente'.$dep.'-parentesco'} == 'CÔNJUGE' ) 
		return 'E';
	elseif ( $proposta->{'dependente'.$dep.'-parentesco'} == 'FILHO(A)/ENTEADO(A)' )
		return 'F';
	else
		return 'A';
}
function get_agsi_tipo_dep ($proposta, $dep) {
	if ( $proposta->{'dependente'.$dep.'-sexo'} == 'MASCULINO' )
		return 1;
	elseif ( $proposta->{'dependente'.$dep.'-sexo'} == 'FEMININO' )
		return 2;
	else
		return 3;
}
$agsi_ger_grau_par = array(
	'filhos' => 3,
	'agregado' => 80
);
function get_agsi_dep_cod_grau_parentesco ($proposta, $dep) {
	global $agsi_ger_grau_par;
	if ( $proposta->{'dependente'.$dep.'-parentesco'} == 'CÔNJUGE' ) 
		return '02';
	elseif ( $proposta->{'dependente'.$dep.'-parentesco'} == 'FILHO(A)/ENTEADO(A)' )
		return $agsi_ger_grau_par['filhos']++;
	else
		return $agsi_ger_grau_par['agregado']++;
}
/**
 * Retira os acentos e remove os caracteres , . - /
 */
function get_agsi_filter_field ($field) {
	$field = remove_accents($field);
	// $field = str_replace(array(',', '.', '-', '/'), '', $field);
	return $field;
}

// DECLARA CADA LINHA DO CSV
// TITULAR E DEPENDENTES
$rows = array();
$rows[] = array(
	$proposta->entidade->codigo,					// COD_EMPRESA
	'',												// COD_USR
	$proposta->nome,								// NOME_USR
	substr($proposta->sexo, 0, 1),					// Sexo do usuário (M ou F)
	'T',											// Grau de parentesco (T-Tiular; F-Filhos; A-Agregados; E-Côjuge)
	'U',											// TIPO_USR
	$proposta->nascimento,							// DATA DE NASCIMENTO
	$proposta->vigenciaf,							// Data de inclusão do usuário
	'9',											// DIA_VECTO
	$proposta->endereco,							// ENDEREÇO RESIDENCIAL
	$proposta->complemento,							// COMPLEMENTO
	$proposta->numero,								// NÚMERO
	$proposta->cidade,								// CIDADE
	$proposta->bairro,								// BAIRRO
	$proposta->uf,									// UF (2 caracteres)
	preg_replace('/\D/', '', $proposta->cep),		// CEP Apenas com números
	preg_replace('/\D/', '', $proposta->telefone),	// Telefone Apenas com números
	preg_replace('/\D/', '', $proposta->cpf),		// CPF Apenas com números
	get_agsi_estado_civil($proposta),				// Estado Civil
	$proposta->{'titular-valor'},					// Valor da mensalidade do usuário
	'',												// DATA_TERM_CONTR
	'3411',											// COD_BANCO_USR
	'0268',											// COD_AGENC_USR
	'750767',										// COD_CONTA_USR
	$proposta->plano->codigo,						// COD_PLANO_USR
	'',												// COD_INTERNO_USR_EMP
	'',												// NO_CONTR_ADESAO
	0,												// COD_TIPO_DEP 0-titular; 1-dep masculino; 2-dep feminino; 3-universitario; 4-deficiente fisico
	$proposta->{'nome-mae'},						// Nome da mãe do usuário
	preg_replace('/\D/', '', $proposta->rg),		// RG com 14 caracteres somente números
	'',												// DATA_VAL_CART
	'',												// DATA_EMI_CART
	'',												// NO_PIS_USR
	$proposta->email,								// E-mail do usuário
	'01',											// COD_GER_GRAU_PAR 01-titular; 02-conjuge; 03~79-filhos; 80~...-agregados
	$proposta->vigenciaf,							// DATA_INIC_CARE
	$proposta->usuario_nome,						// Nome do vendedor
	'',												// COD_ANTIGO
	'',												// COD_PROFISSAO
	'1',											// STATUS_REGISTRO
	$proposta->dnv,									// Número da declaração de nascido vivo - DNV (Obrigatório nascidos a partir de 2010)
	'',												// CNS
	'',												// DATA_ADM
	'VO'.$proposta->protocolo,						// Número do contrato do beneficiário
	$proposta->{'orgao-expedidor'},					// Órgão emissor do RG
	'',												// RG_PAIS
	$proposta->corretora->nome
);

if ( $proposta->{'quantidade-dependentes'} > 0 ) for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) {
	$rows[] = array(
		$proposta->entidade->codigo,						// COD_EMPRESA
		'',													// COD_USR
		$proposta->{'dependente'.$i.'-nome'},				// NOME_USR
		substr($proposta->{'dependente'.$i.'-sexo'}, 0, 1),	// Sexo do usuário (M ou F)
		get_agsi_dep_grau_parentesco($proposta, $i),		// Grau de parentesco (T-Tiular; F-Filhos; A-Agregados; E-Côjuge)
		'U',												// TIPO_USR
		$proposta->{'dependente'.$i.'-nascimento'},			// DATA DE NASCIMENTO
		$proposta->vigenciaf,								// Data de inclusão do usuário
		'9',												// DIA_VECTO
		$proposta->endereco,								// ENDEREÇO RESIDENCIAL
		$proposta->complemento,								// COMPLEMENTO
		$proposta->numero,									// NÚMERO
		$proposta->cidade,									// CIDADE
		$proposta->bairro,									// BAIRRO
		$proposta->uf,										// UF (2 caracteres)
		preg_replace('/\D/', '', $proposta->cep),			// CEP Apenas com números
		preg_replace('/\D/', '', $proposta->telefone),		// Telefone Apenas com números
		preg_replace('/\D/', '', $proposta->cpf),			// CPF Apenas com números
		get_agsi_estado_civil($proposta, $i),				// Estado Civil
		$proposta->{'dependente-'.$i.'-valor'},				// Valor da mensalidade do usuário
		'',													// DATA_TERM_CONTR
		'3411',												// COD_BANCO_USR
		'0268',												// COD_AGENC_USR
		'750767',											// COD_CONTA_USR
		$proposta->plano->codigo,							// COD_PLANO_USR
		'',													// COD_INTERNO_USR_EMP
		'',													// NO_CONTR_ADESAO
		get_agsi_tipo_dep($proposta, $i),					// COD_TIPO_DEP 0-titular; 1-dep masculino; 2-dep feminino; 3-universitario; 4-deficiente fisico
		$proposta->{'dependente'.$i.'-nome-mae'},			// Nome da mãe do usuário
		preg_replace('/\D/', '', $proposta->{'dependente'.$i.'-rg'}), // RG com 14 caracteres somente números
		'',													// DATA_VAL_CART
		'',													// DATA_EMI_CART
		'',													// NO_PIS_USR
		$proposta->email,									// E-mail do usuário
		get_agsi_dep_cod_grau_parentesco($proposta, $i),	// COD_GER_GRAU_PAR 01-titular; 02-conjuge; 03~79-filhos; 80~...-agregados
		$proposta->vigenciaf,								// DATA_INIC_CARE
		$proposta->usuario_nome,							// Nome do vendedor
		'',													// COD_ANTIGO
		'',													// COD_PROFISSAO
		'1',												// STATUS_REGISTRO
		$proposta->{'dependente'.$i.'-dnv'},				// Número da declaração de nascido vivo - DNV (Obrigatório nascidos a partir de 2010)
		'',													// CNS
		'',													// DATA_ADM
		'VO'.$proposta->protocolo,							// Número do contrato do beneficiário
		$proposta->{'dependente'.$i.'-orgao-expedidor'},	// Órgão emissor do RG
		'',													// RG_PAIS
		$proposta->corretora->nome
	);
}

// $input: array sem acentos
$input = array();
foreach ($rows as $row) {
	$linha = array();
	foreach ($row as $item)
		$linha[] = get_agsi_filter_field($item);
	$input[] = $linha;
}


$headers = array(
	'COD_EMPRESA',
	'COD_USR',
	'NOME_USR',
	'SEXO',
	'GRAU_PARENTE',
	'TIPO_USR',
	'DATA_NAS',
	'DATA_INCLUSAO',
	'DIA_VECTO',
	'ENDERECO USUARIO',
	'ENDER_COMPL',
	'NUMERO',
	'CIDADE_USR',
	'BAIRRO_USR',
	'UF_USR',
	'CEP',
	'TELEFONE',
	'CPF/DNV',
	'EST_CIVIL',
	'VALOR_MENSAL',
	'DATA_TERM_CONTR',
	'COD_BANCO_USR',
	'COD_AGENC_USR',
	'COD_CONTA_USR',
	'COD_PLAN_USR',
	'COD_INTERNO_USR_EMP',
	'NO_CONTR_ADESAO',
	'COD_TIPO_DEP',
	'NOME_MAE_USR',
	'NO_RG_USR',
	'DATA_VAL_CART',
	'DATA_EMI_CART',
	'NO_PIS_USR',
	'EMAIL_USR',
	'COD_GER_GRAU_PAR',
	'DATA_INIC_CARE',
	'COD_VENDEDOR',
	'COD_ANTIGO',
	'COD_PROFISSAO',
	'STATUS_REGISTRO',
	'DNV',
	'CNS',
	'DATA_ADM',
	'NUMERO_CONTRATO',
	'RG_ORGAO',
	'RG_PAIS',
	'ASSESSORIA'
);

// GRAVA O ARQUIVO CSV
fputcsv($output, $headers, ';');
foreach($input as $row) fputcsv($output, $row, ';');


?>