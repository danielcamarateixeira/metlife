<?php

	include VIEW . '/cadastro/_auth.php';

	$proposta = new Proposta;
	$proposta->loadByID($_GET['id']);

	if ( !$proposta->getID() ) {
		header("Location: ".get_config('url').'documentos');
		exit;
	}

	$base_url = get_config('url') . 'cadastro/documentos';

	get_header();

?>

<main class="main dashboard">

	<div class="container widget">

		<div class="pull-right">
			<br><br>
			<a href="<?php echo $base_url; ?>/download-zip?id=<?php echo $proposta->getID(); ?>" class="btn btn-danger btn-lg">Download do .zip <i class="fa fa-file-archive-o" aria-hidden="true"></i></a>
		</div>

		<h1>Documentos</h1>
		<h4>Proposta #<?php echo $proposta->protocolo; ?> - <?php echo $proposta->nome; ?></h4>

		<hr>

		<?php if ( $all_docs = $proposta->getTodosDocumentos() ) : ?>

			<div class="row row-flex documentos-grid">
				
				<?php foreach($all_docs as $slug => $fileurl) : $fileinfo = pathinfo($fileurl); ?>
					<div class="col-xs-6 col-md-4 col-lg-3 item-container">
						<div class="panel panel-default">
							<div class="panel-heading"><?php echo $proposta->getAutoDocName($slug); ?></div>
							<div class="panel-body">
								<?php $file_original_url = $fileurl; $fileurl = ( mb_strtolower($fileinfo['extension']) == 'pdf' ) ? $fileurl : get_url('cadastro/documentos/ver?id='.base64_encode($fileurl)); ?>
								<a href="<?php echo $fileurl; ?>" title="Ver documento" target="_blank">
									<?php if ( mb_strtolower($fileinfo['extension']) == 'pdf' ) : ?>
										<div class="alert alert-info"><i class="fa fa-file-pdf-o"></i> Visualizar o PDF</div>
									<?php else : ?>
										<img src="<?php echo $file_original_url; ?>" class="img-responsive">
									<?php endif; ?>
								</a>
							</div>
							<div class="panel-footer">
								<a href="<?php echo $base_url; ?>/download?id=<?php echo base64_encode($fileurl); ?>" class="btn btn-block btn-danger"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

			</div>

		<?php else : ?>
			<div class="alert alert-danger">
				Não há documentos nessa proposta.
			</div>
		<?php endif; ?>


	</div>

</main>

<?php get_footer(); ?>