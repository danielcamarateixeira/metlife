<?php

	$id = $_GET['id'];
	$proposta = new Proposta;
	$proposta->loadByID($id);

	if ( !$proposta->getID() ) die('Erro: Proposta não encontrada');

	$all_docs = $proposta->getTodosDocumentos();

	if ( count($all_docs) == 0 ) die('Erro: Nenhum documento disponível para download');

	$files = array();

	foreach ($all_docs as $name => $doc) {
		$files[$name] = get_upload_abs_from_rel( $doc );
	}

	$zippath = UPLOADS . '/documentos/zip/';
	$zipfilename = 'DOCUMENTOS_PROPOSTA_'.$proposta->protocolo.'.zip';
	$zipname = $zippath . $zipfilename;
	$zip = new ZipArchive;
	$zip->open($zipname, ZipArchive::CREATE);
	foreach ($files as $name => $file) {
		$zip->addFile($file, $proposta->getAutoDocName($name) .'.'. pathinfo($file, PATHINFO_EXTENSION));
	}
	$zip->close();

	header('Content-Type: application/zip');
	header('Content-disposition: attachment; filename='.$zipfilename);
	header('Content-Length: ' . filesize($zipname));
	readfile($zipname);

?>