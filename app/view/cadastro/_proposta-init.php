<?php

	/**
	 * Arquivo que realiza verificações se o usuário tem permissão para revisar a proposta
	 */

	$proposta = new Proposta;
	$proposta->loadByID( $_GET['id'] );

	// Redireciona caso não encontre a proposta
	if ( !$proposta->getID() ) {
		header('Location: '.get_url('404'));
		exit;
	}

	

?>