<?php

	$list = new Proposta;
	$list->loadList(array(
		'orderby' => 'datecreate',
		'order' => 'DESC',
		array(
			'field' => 'status',
			'value' => 'indeferida'
		)
	), 'p');

?>

<section class="propostas-recentes widget">

	<table class="table table-striped table-condensed table-responsive">
		<thead>
			<tr>
				<th class="text-right">Protocolo</th>
				<th class="text-right">N&ordm;</th>
				<th>Vigência</th>
				<th>Nome</th>
				<th class="text-center">N&ordm; de vidas</th>
				<th class="text-center">Entidade</th>
				<th>&nbsp;</th>
				<th class="text-center">Proposta</th>
			</tr>
		</thead>

		<tbody>
			<?php if ($list->getList()) : foreach($list->getList() as $proposta) :
				$ultima_observacao_interna = $proposta->getLastObservacao(true, 'cadastro'); ?>
				<tr>
					<td class="text-right"><?php echo $proposta->protocolo; ?></td>
					<td class="text-right"><?php echo $proposta->getID(); ?></td>
					<td><?php echo $proposta->vigenciaf; ?></td>
					<td><?php echo $proposta->nome; ?></td>
					<td class="text-center"><?php echo $proposta->{'quantidade-dependentes'}+1; ?></td>
					<td class="text-center"><?php echo $proposta->entidade->nome; ?></td>
					<td class="text-right historico-notificacoes">
						<?php if ($ultima_observacao_interna) : ?>
							<a href="<?php echo get_url('cadastro/revisar?id='.$proposta->getID()); ?>">
								<span class="tem-observacoes"><i class="fa fa-warning" data-toggle="tooltip" data-placement="left" title="<?php echo $ultima_observacao_interna['observacoes']; ?>"></i></span></a>
						<?php endif; ?>
						<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="<?php echo sprintf('Vendedor: %s &bull; Corretora: %s', $proposta->usuario->nome, $proposta->corretora->nome); ?>"></i>
					</td>
					<td class="text-center">
						<a href="<?php echo get_url('cadastro/revisar'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-link btn-sm btn-tb-action btn-success"><i class="fa fa-check-square-o"></i> Revisar</a>
						<a href="<?php echo get_url('cadastro/visualizar'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-link btn-sm btn-tb-action btn-primary" title="Visualizar proposta"><i class="fa fa-file-text-o"></i></a>
					</td>
				</tr>
			<?php endforeach; else : ?>
				<tr>
					<td colspan="7">
						<div class="alert alert-info">Nenhuma proposta encontrada</div>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>

	<div class="text-center">
		<?php echo $list->getPaginationHtml(); ?>
	</div>
	
</section>