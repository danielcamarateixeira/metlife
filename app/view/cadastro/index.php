<?php

	include VIEW . '/cadastro/_auth.php';

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php include VIEW . '/cadastro/section-fluxo.php'; ?>

		<?php include VIEW . '/cadastro/section-relatorios.php'; ?>

		<?php include VIEW . '/cadastro/section-recentes.php'; ?>
		
	</div>

</main>


<?php get_footer(); ?>