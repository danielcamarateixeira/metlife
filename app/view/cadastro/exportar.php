<?php

$proposta = new Proposta;
$proposta->loadByID($_GET['id']);
if (!$proposta->getID()) die('Proposta inexistente');


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=METLIFE - Proposta '.$proposta->protocolo.'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));

// Salva o titular
fputcsv($output, array(
	'INCLUSÃO',								// OPERAÇÃO
	$proposta->protocolo,					// CÓDIGO DO CONTRATO
	$proposta->corretora->cnpj,				// CNPJ DA FILIAL
	$proposta->entidade->nome,				// DESCRIÇÃO DA FILIAL
	'',										// N DE MATRÍCULA NA EMPRESA
	'',										// DATA DE ADMISSÃO
	'',										// CARGO NA EMPRESA
	'', 									// CENTRO DE CUSTO
	'', 									// DESCRIÇÃO CENTRO DE CUSTO
	$proposta->plano->nome,					// PLANO
	$proposta->nome,						// NOME COMPLETO DO BENEFICIÁRIO
	'TITULAR',								// TIPO ASSOCIADO
	'TITULAR', 								// RELAÇÃO DE DEPENDÊNCIA
	$proposta->{'nome-mae'},				// NOME COMPLETO DA MÃE
	$proposta->nascimento,					// DATA DE NASCIMENTO
	$proposta->{'pis-pasep'},				// PIS
	$proposta->cpf,							// CPF
	'CPF VÁLIDO',							// CPF VÁLIDO
	$proposta->cns,							// N DO CARTÃO NACIONAL DE SAUDE - CNS
	$proposta->rg,							// RG
	$proposta->{'orgao-expedidor'},			// ÓRGÃO EMISSOR
	'1',									// PAÍS EMISSOR
	'',										// DATA DE EMISSÃO
	$proposta->{'estado-civil'},			// ESTADO CIVIL
	$proposta->sexo,						// SEXO
	'',										// TIPO LOGRADOURO
	$proposta->endereco,					// ENDEREÇO RESIDENCIAL
	$proposta->numero,						// NÚMERO
	$proposta->complemento,					// COMPLEMENTO
	$proposta->bairro,
	$proposta->uf,
	$proposta->cep,
	get_ddd($proposta->telefone),			// DDD RESIDENCIAL
	get_telefone($proposta->telefone),		// TELEFONE RESIDENCIAL
	'',										// DDD COMERCIAL
	'',										// TELEFONE COMERCIAL
	get_ddd($proposta->celular),			// DDD CELULAR
	get_telefone($proposta->celular),		// TELEFONE CELULAR
	$proposta->email,
	$proposta->banco,
	$proposta->agencia,
	$proposta->conta,
	$proposta->dnv 							// N DE DECLARAÇÃO DE NASCIDO VIVO

));

if ( $proposta->{'quantidade-dependentes'} > 0 ) for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) {

	$cpf_valido = ($proposta->{'dependente'.$i.'-cpf'}) ? 'CPF VÁLIDO' : '';

	// Salva os dependentes
	fputcsv($output, array(
		'INCLUSÃO',								// OPERAÇÃO
		$proposta->protocolo,					// CÓDIGO DO CONTRATO
		$proposta->corretora->cnpj,				// CNPJ DA FILIAL
		$proposta->entidade->nome,				// DESCRIÇÃO DA FILIAL
		'',										// N DE MATRÍCULA NA EMPRESA
		'',										// DATA DE ADMISSÃO
		'',										// CARGO NA EMPRESA
		'', 									// CENTRO DE CUSTO
		'', 									// DESCRIÇÃO CENTRO DE CUSTO
		$proposta->plano->nome,					// PLANO
		$proposta->{'dependente'.$i.'-nome'},	// NOME COMPLETO DO BENEFICIÁRIO
		'DEPENDENTE',							// TIPO ASSOCIADO
		$proposta->{'dependente'.$i.'-parentesco'},	// RELAÇÃO DE DEPENDÊNCIA
		$proposta->{'dependente'.$i.'-nome-mae'},	// NOME COMPLETO DA MÃE
		$proposta->{'dependente'.$i.'-nascimento'},	// DATA DE NASCIMENTO
		'',										// PIS
		$proposta->{'dependente'.$i.'-cpf'},							// CPF
		$cpf_valido,							// CPF VÁLIDO
		$proposta->{'dependente'.$i.'-cns'},	// N DO CARTÃO NACIONAL DE SAUDE - CNS
		$proposta->{'dependente'.$i.'-rg'},		// RG
		'',										// ÓRGÃO EMISSOR
		'1',									// PAÍS EMISSOR
		'',										// DATA DE EMISSÃO
		$proposta->{'dependente'.$i.'-estado-civil'},	// ESTADO CIVIL
		$proposta->{'dependente'.$i.'-sexo'},	// SEXO
		'',										// TIPO LOGRADOURO
		$proposta->endereco,					// ENDEREÇO RESIDENCIAL
		$proposta->numero,						// NÚMERO
		$proposta->complemento,					// COMPLEMENTO
		$proposta->bairro,
		$proposta->uf,
		$proposta->cep,
		get_ddd($proposta->telefone),			// DDD RESIDENCIAL
		get_telefone($proposta->telefone),		// TELEFONE RESIDENCIAL
		'',										// DDD COMERCIAL
		'',										// TELEFONE COMERCIAL
		get_ddd($proposta->celular),			// DDD CELULAR
		get_telefone($proposta->celular),		// TELEFONE CELULAR
		$proposta->email,
		$proposta->banco,
		$proposta->agencia,
		$proposta->conta,
		$proposta->{'dependente'.$i.'-dnv'} 							// N DE DECLARAÇÃO DE NASCIDO VIVO

	));

}

?>