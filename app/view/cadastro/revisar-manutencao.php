<?php

	include VIEW . '/_auth.php';
	include VIEW . '/cadastro/_proposta-init.php';

	// Verifica se este usuário tem permissão para revisar a proposta
	if ( $currentuser->cargo != $proposta->status && !in_array($currentuser->cargo, array('desenvolvedor', 'administradora')) )
		go_to(get_url('cadastro'));

	$etapas = new PropostaEtapa;

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<div class="alert alert-warning">
			Em manutenção
		</div>
		
	</div>

</main>


<?php get_footer(); ?>