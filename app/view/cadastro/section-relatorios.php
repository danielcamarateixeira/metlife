<?php

	$andamento = new Proposta;
	$andamento->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'vendedor'
		),
		array(
			'field' => 'observacoes',
			'value' => ''
		)
	));

	$pendencia = new Proposta;
	$pendencia->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'vendedor'
		),
		array(
			'field' => 'observacoes',
			'value' => '',
			'compare' => '!='
		)
	));

	$backoffice = new Proposta;
	$backoffice->loadList(array(
		array(
			'field' => 'status',
			'value' => 'backoffice'
		)
	));

	$cadastro = new Proposta;
	$cadastro->loadList(array(
		array(
			'field' => 'status',
			'value' => 'cadastro'
		)
	));

	$analise = new Proposta;
	$analise->loadList(array(
		array(
			'field' => 'status',
			'value' => 'analise'
		)
	));

	$implantado = new Proposta;
	$implantado->loadList(array(
		array(
			'field' => 'status',
			'value' => 'implantada'
		)
	));

	$indeferido = new Proposta;
	$indeferido->loadList(array(
		array(
			'field' => 'status',
			'value' => 'indeferida'
		)
	));

?>

<div class="relatorios-backoffice row flex-row dashboard">
	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas em andamento <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas com pendências do cliente"></i></h4>
			<p class="data"><?php echo count($andamento->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas com pendências <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas que foram devolvidas pelo Backoffice ou Cadastro"></i></h4>
			<p class="data"><?php echo count($pendencia->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas no backoffice</h4>
			<p class="data"><?php echo count($backoffice->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas no cadastro</h4>
			<p class="data"><?php echo count($cadastro->getList()); ?></p>
		</div>
	</div>
</div>

<div class="relatorios-backoffice row flex-row dashboard">
	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<a href="<?php echo get_url('cadastro/analise'); ?>">
				<h4 class="title">Propostas em análise <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas que estão sob análise da METLIFE"></i></h4>
				<p class="data"><?php echo count($analise->getList()); ?></p>
			</a>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<a href="<?php echo get_url('cadastro/implantadas'); ?>">
				<h4 class="title">Propostas implantadas <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas que foram aprovadas pela METLIFE"></i></h4>
				<p class="data"><?php echo count($implantado->getList()); ?></p>
			</a>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<a href="<?php echo get_url('cadastro/indeferidas'); ?>">
				<h4 class="title">Propostas indeferidas <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas que não foram aprovadas pela METLIFE"></i></h4>
				<p class="data"><?php echo count($indeferido->getList()); ?></p>
			</a>
		</div>
	</div>

</div>