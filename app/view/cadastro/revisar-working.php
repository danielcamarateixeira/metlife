<?php

	include VIEW . '/_auth.php';
	include VIEW . '/backoffice/_proposta-init.php';

	// Verifica se este usuário tem permissão para revisar a proposta
	if ( $currentuser->cargo != $proposta->status && !in_array($currentuser->cargo, array('desenvolvedor', 'administradora')) )
		go_to(get_url('backoffice'));

	$etapas = new PropostaEtapa;

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php foreach($etapas->etapas_ordem as $i => $etapa) : ?>

			<div class="etapa-form row">

				<div class="col-xs-12 col-sm-8">

					<div class="widget">

						<table class="table table-stripped table-condensed table-responsive">

							<?php

							if ( file_exists(TEMPLATES . '/metlife-'.$etapa['slug'].'.json') ) :
								$form = new Form( TEMPLATES . '/metlife-'.$etapa['slug'].'.json', $proposta->getProperties() );

								switch ($etapa['slug']) {
									case 'dependentes':
										$fields = false;
										$dep_fields = $form->getFields();
										$qtd_deps = $proposta->{'quantidade-dependentes'};
										foreach($dep_fields as $field) {
											if ($field['prefix'] == 'dependente'.($qtd_deps+1).'-') break;
											$fields[] = $field;
										}
									break;
									case 'plano':
										$fields = array(
											array(
												'name' => 'plano_nome',
												'label' => 'Plano'
											),
											array(
												'name' => 'titular-idade',
												'label' => 'Idade do titular'
											),
											array(
												'name' => 'titular-valor',
												'label' => 'Valor do titular'
											)
										);
										for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) {
											$fields[] = array(
												'name' => 'dependente-'.$i.'-idade',
												'label' => 'Idade do dependente '.$i,
											);
											$fields[] = array(
												'name' => 'dependente-'.$i.'-valor',
												'label' => 'Valor do dependente '.$i,
											);
										}
										$fields[] = array(
											'name' => 'valor-total-proposta',
											'label' => 'Valor total da proposta'
										);
										$fields[] = array(
											'name' => 'vigencia-inicio',
											'label' => 'Valor total da proposta'
										);
										$fields[] = array(
											'name' => 'valor-proposta-extenso',
											'label' => 'Valor total da proposta por extenso'
										);
									break;
									default :
										$fields = $form->getFields();
										// debug($fields);
									break;
								}

								// debug($fields);

								if ($fields) foreach ( $fields as $i => $field ) :

									if (isset($field['label'])) $label = $field['label'];
									elseif (isset($field['title'])) $label = $field['title'];
									else $label = false;

									// debug($field);
							?>
								<tr>
									<td><?php echo $label; ?></td>
									<td><?php echo $proposta->$field['name']; ?></td>
								</tr>

							<?php endforeach; endif; ?>

						</table>

					</div>

				</div>

				<div class="col-xs-12 col-sm-4">
					<div class="widget">
						<label>Observações - Etapa <?php echo $etapa['nome'] ;?></label>
						<textarea rows="12" class="form-control"></textarea>
					</div>
				</div>
				
			</div>

			<hr>

		<?php endforeach; ?>
		
	</div>

</main>


<?php get_footer(); ?>