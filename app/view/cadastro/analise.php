<?php

	include VIEW . '/cadastro/_auth.php';

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php include VIEW . '/cadastro/section-fluxo.php'; ?>

		<?php include VIEW . '/cadastro/section-relatorios.php'; ?>

		<div class="alert alert-info alert-sm"><a href="<?php echo get_url('cadastro/analise/relatorio'); ?>"><strong>Novo!</strong> Relatório de Propostas em Análise, clique aqui para acessar.</a></div>

		<?php include VIEW . '/cadastro/section-analise.php'; ?>
		
	</div>

</main>


<?php get_footer(); ?>