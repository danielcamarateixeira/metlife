<?php

	include VIEW . '/cadastro/_auth.php';

	$list = new Proposta;
	$list->per_page = 100;
	$list->loadList(array(
		array(
			'field' => 'status',
			'value' => 'analise'
		)
	));
	$list->orderByField('nome');

	$base_url = get_config('url') . 'painel/proposta';

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php include VIEW . '/cadastro/section-fluxo.php'; ?>

		<?php include VIEW . '/cadastro/section-relatorios.php'; ?>

		<div class="widget col-xs-12">

			<h1>Propostas em análise <a href="<?php echo get_url('cadastro/analise/exportar'); ?>" class="pull-right btn btn-primary">Exportar CSV</a></h1>

			<hr>

			<?php if ($list->getList()) : ?>
				<table class="table table-responsive table-condensed">
					
					<thead>
						<tr>
							<th>Protocolo</th>
							<th>Vendedor</th>
							<th>Vigência</th>
							<th>Nome</th>
							<th class="text-center">Tipo</th>
							<th>Corretora</th>
							<th>Entidade</th>
						</tr>
					</thead>

					<tbody>
						<?php if ($list->getList()) : foreach($list->getList() as $proposta) : ?>
							<tr>
								<td><?php echo $proposta->protocolo; ?></td>
								<td><?php echo $proposta->usuario->nome; ?></td>
								<td><?php echo $proposta->vigenciaf; ?></td>
								<td><?php echo $proposta->nome; ?></td>
								<td class="text-center">T</td>
								<td><?php echo $proposta->corretora->nome; ?></td>
								<td><?php echo $proposta->entidade->nome; ?></td>
							</tr>
							<?php if ($proposta->{'quantidade-dependentes'} > 0) for($i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++) : ?>
								<tr class="warning">
									<td><?php echo $proposta->protocolo; ?></td>
									<td><?php echo $proposta->usuario->nome; ?></td>
									<td><?php echo date('d/m/y', strtotime($proposta->{'vigencia-inicio'})); ?></td>
									<td><?php echo $proposta->{'dependente'.$i.'-nome'}; ?></td>
									<td class="text-center">D</td>
									<td><?php echo $proposta->corretora->nome; ?></td>
									<td><?php echo $proposta->entidade->nome; ?></td>
								</tr>
							<?php endfor; ?>
						<?php endforeach; else : ?>
							<tr>
								<td colspan="6">
									<div class="alert alert-info">Nenhum pedido encontrado</div>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>

				</table>
			<?php else : ?>
				<div class="alert alert-warning">
					Não foram encontradas propostas
				</div>
			<?php endif; ?>

		</div>

	</div>

</main>

<?php get_footer(); ?>