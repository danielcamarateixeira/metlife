<?php

	include VIEW . '/cadastro/_auth.php';
	include VIEW . '/cadastro/_proposta-init.php';

	// Verifica se este usuário tem permissão para revisar a proposta
	if ( $currentuser->cargo != $proposta->status && !in_array($currentuser->cargo, array('desenvolvedor', 'administradora', 'cadastro')) )
		go_to(get_url('cadastro'));

	$etapas = new PropostaEtapa;

	$doc = $proposta->getDocumento();
	$doc_filiacao = $proposta->getDocumentoFiliacao();

	if ( isset($_GET['salvar']) ) {
		// 1. Atualiza o campo de observações
		$proposta->setObservacoes( $_POST['observacao'], $_POST['status'] );
		$proposta->setObservacoesAdm( $_POST['observacao_adm'], $_POST['status'] );
		// 2. Cria uma nova interação no histórico da proposta
		$proposta->setHistory("Revisado pelo setor Cadastro e enviado para ".$_POST['status'], json_encode($_POST['observacao']));
		// 3. Atualiza o status da proposta
		$proposta->status = $_POST['status'];
		// 4. Salva a proposta
		$proposta->save();
		// 5. Envia os e-mails
		if($_POST['status'] == 'implantada') $proposta->sendMailImplantada();
		// 6. Volta para a lista
		go_to(get_url('cadastro'));
	}

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<h1>Revisar proposta #<?php echo $proposta->protocolo; ?></h1>

		<div class="row">

			<div class="col-xs-12 col-sm-8">
				<div id="proposta-documento" class="paper"><?php echo $doc->getHtml(); ?></div>
				<?php if ( $doc_filiacao ) : ?>
					<hr>
					<div id="ficha-filiacao" class="paper"><?php echo $doc_filiacao->getHtml(); ?></div>
				<?php endif; ?>
			</div>

			<div class="col-xs-12 col-sm-4">

				<div class="widget widget-default widget-sticky">

					<form action="?id=<?php echo $_GET['id']; ?>&salvar" method="post">

						<div class="alert alert-danger">
							<strong>Atenção!</strong> Os campos abaixo são visíveis tanto para os vendedores quanto para a corretora dessa proposta.
						</div>
					
						<div class="panel-group" id="obs-accordion" role="tablist" aria-multiselectable="true">

							<?php foreach($etapas->etapas_ordem as $i => $etapa) : ?>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading-<?php echo $etapa['slug']; ?>">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#obs-accordion" href="#panel-<?php echo $etapa['slug']; ?>" aria-expanded="<?php echo ($i==1) ? 'true' : 'false'; ?>" aria-controls="panel-<?php echo $etapa['slug']; ?>">
												<?php echo $etapa['nome']; ?>
											</a>
										</h4>
									</div>
									<div id="panel-<?php echo $etapa['slug']; ?>" class="panel-collapse collapse <?php echo ($i==1) ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $etapa['slug']; ?>">
	      								<div class="panel-body">
											<textarea rows="5" class="form-control" name="observacao[<?php echo $etapa['slug']; ?>]" placeholder="Observações sobre a etapa <?php echo $etapa['nome']; ?>..."></textarea>
											<a href="<?php echo get_url('proposta/'.$etapa['slug'].'?id='.$proposta->getID()); ?>&revisao=cadastro" class="btn btn-sm btn-block btn-primary" target="_blank"><i class="fa fa-pencil"></i> Editar campos desta etapa</a>

											<?php if ($proposta->getLastObservacaoEtapaHtml($etapa['slug'])) : ?>
												<a href="#popup-historico-etapa-<?php echo $etapa['slug']; ?>" data-toggle="modal" class="btn btn-xsm btn-link btn-block btn-historico-etapa"><i class="fa fa-clock-o"></i> Ver histórico de observações dessa etapa</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

						</div>

						<div class="observacoes-internas-container alert alert-warning alert-dashed text-center">
							<label for="observacoes-internas">Observações internas <i class="fa fa-lock"></i></label>
							<textarea name="observacao_adm" id="inputObservacaoAdm" class="form-control input-observacao" placeholder="Observações internas gerais sobre essa proposta..." rows="6"></textarea>
							<span class="info info-sm">As informações preenchidas nesse campo <strong>não</strong> são acessíveis por vendedores ou corretora.</span>
							<?php if ($proposta->getLastObservacao(true)) : ?>
								<a href="#popup-historico-adm" data-toggle="modal" class="btn btn-xsm btn-link btn-block btn-historico-etapa"><i class="fa fa-clock-o"></i> Ver histórico de observações <br>internas dessa proposta</a>
							<?php endif; ?>
						</div>

						<a href="<?php echo get_url('cadastro/agsi'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-block btn-success" target="_blank">Exportar AGSI</a>
						<br>

						<?php

							if ($proposta->status == 'analise') :
								$proposta_actions = array(
									'cadastro' => array(
										'class' => 'alert alert-danger alert-radio',
										'texto' => 'Enviar de volta para o <strong>Cadastro</strong> <i class="fa fa-warning"></i>',
										'checked' => false
									),
									'indeferida' => array(
										'class' => 'alert alert-danger alert-radio',
										'texto' => 'Enviar para <strong>Indeferida</strong> <i class="fa fa-close"></i>',
										'checked' => false
									),
									'implantada' => array(
										'class' => 'alert alert-success alert-radio',
										'texto' => 'Enviar para <strong>Implantada</strong> <i class="fa fa-check"></i>',
										'checked' => true
									)
								);
							else:
								$proposta_actions = array(
									'backoffice' => array(
										'class' => 'alert alert-danger alert-radio',
										'texto' => 'Enviar de volta para o <strong>Backoffice</strong> <i class="fa fa-warning"></i>',
										'checked' => false
									),
									'analise' => array(
										'class' => 'alert alert-success alert-radio',
										'texto' => 'Enviar para <strong>Aguardando Análise</strong> <i class="fa fa-clock-o"></i>',
										'checked' => true
									)
								);
							endif;

							foreach ($proposta_actions as $proposta_action_status => $proposta_action_info) :

						?>

							<div class="<?php echo $proposta_action_info['class']; ?>">
								<div class="radio">
									<label>
										<input required type="radio" name="status" id="inputStatus" value="<?php echo $proposta_action_status; ?>" <?php if ($proposta_action_info['checked']) echo 'checked="checked"'; ?>><?php echo $proposta_action_info['texto']; ?>
									</label>
								</div>
							</div>

						<?php endforeach; ?>

						<hr>

						<div class="text-right">
							<button class="btn btn-primary btn-revisar-submit" data-page="cadastro" type="submit">Salvar</button>
						</div>

					</form>

				</div>
				
			</div>
			
		</div>

	</div>

</main>

<?php

	/**
	 * Popup com histórico de observações da proposta, caso existam
	 */
	foreach($etapas->etapas_ordem as $i => $etapa) :
		if ($proposta->getLastObservacaoEtapaHtml($etapa['slug'])) :

?>
			<div class="modal fade" id="popup-historico-etapa-<?php echo $etapa['slug']; ?>">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="fa fa-clock-o"></i> Proposta #<?php echo $proposta->protocolo; ?> - Observações para etapa <?php echo $etapa['nome']; ?></h4>
						</div>
						<div class="modal-body">
							<?php echo $proposta->getObservacoesHtml(null, 'DESC'); ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Ok, obrigado</button>
						</div>
					</div>
				</div>
			</div>
<?php

		endif;
	endforeach;

?>

<?php if ($proposta->getLastObservacao(true)) : ?>
	<div class="modal fade" id="popup-historico-adm">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><i class="fa fa-lock"></i> Proposta #<?php echo $proposta->protocolo; ?> - Observações internas</h4>
				</div>
				<div class="modal-body">
					<?php echo $proposta->getObservacoesHtml(null, 'DESC', true); ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Ok, obrigado</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>