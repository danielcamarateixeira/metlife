<?php

	$list = new Proposta;
	$list->loadList(array(
		'orderby' => 'datecreate',
		'order' => 'DESC',
		array(
			'field' => 'status',
			'value' => 'implantada'
		)
	), 'p');

?>

<section class="propostas-recentes widget">

	<table class="table table-stripped table-condensed table-responsive">
		<thead>
			<tr>
				<th>Protocolo</th>
				<th>N&ordm;</th>
				<th>Vigência</th>
				<th>Nome</th>
				<th class="text-center">N&ordm; de vidas</th>
				<th class="text-center">Entidade</th>
				<th class="text-center">Proposta</th>
			</tr>
		</thead>

		<tbody>
			<?php if ($list->getList()) : foreach($list->getList() as $proposta) : ?>
				<tr>
					<td><?php echo $proposta->protocolo; ?></td>
					<td><?php echo $proposta->getID(); ?></td>
					<td><?php echo $proposta->vigenciaf; ?></td>
					<td><?php echo $proposta->nome; ?></td>
					<td class="text-center"><?php echo $proposta->{'quantidade-dependentes'}+1; ?></td>
					<td class="text-center"><?php echo $proposta->entidade->nome; ?></td>
					<td class="text-center">
						<a href="<?php echo get_url('cadastro/revisar'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-link btn-sm">Revisar</a>
						<a href="<?php echo get_url('cadastro/visualizar'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-link btn-sm">Visualizar</a>
					</td>
				</tr>
			<?php endforeach; else : ?>
				<tr>
					<td colspan="7">
						<div class="alert alert-info">Nenhuma proposta encontrada</div>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>

	<div class="text-center">
		<?php echo $list->getPaginationHtml(); ?>
	</div>
	
</section>