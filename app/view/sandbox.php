<?php

$admin = new Usuario;
$admin->truncate();
$admin->loadSingle(array(
	array(
		'field' => 'usuario',
		'value' => 'admin'
	)
));
if ( !$admin->getID() ) {
	$admin->nome = "Administrador";
	$admin->usuario = 'admin';
	$admin->senha = md5('admin');
	$admin->permissao = 'desenvolvedor';
	$admin->entidadeid = '1';
	$admin->save();
}

$entidade1 = new Entidade;
$entidade1->loadSingle(array(
	array(
		'field' => 'nome',
		'value' => 'Entidade 1'
	)
));
if ( !$entidade1->getID() ) {
	$entidade1->nome = "Entidade 1";
	$entidade1->save();
}

$entidade2 = new Entidade;
$entidade2->loadSingle(array(
	array(
		'field' => 'nome',
		'value' => 'Entidade 2'
	)
));
if ( !$entidade2->getID() ) {
	$entidade2->nome = "Entidade 2";
	$entidade2->save();
}


$planofirst = new Plano;
$planofirst->truncate();
$planofirst->loadSingle(array(
	array(
		'field' => 'nome',
		'value' => 'Plano First (ANS N&ordm; 467.614/12-9)'
	)
));
if ( !$planofirst->getID() ) {
	$planofirst->nome = 'Plano First (ANS N&ordm; 467.614/12-9)';
	$planofirst->save();
}

$p1e1 = new PlanoValor;
$p1e1->truncate();
$p1e1->loadSingle(array(
	'relation' => 'AND',
	array(
		'field' => 'entidadeid',
		'value' => '1'
	),
	array(
		'field' => 'planoid',
		'value' => '1'
	)
));
if ( !$p1e1->getID() ) {
	$p1e1->planoid = 1;
	$p1e1->entidadeid = 1;
	$p1e1->valor = 30.50;
	$p1e1->save();
}


?>
