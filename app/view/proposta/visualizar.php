<?php

	include VIEW . '/_auth.php';

	$proposta = new Proposta;
	$proposta->loadByHash( $_GET['id'] );

	// Redireciona caso não encontre a proposta
	if ( !$proposta->getID() ) {
		header('Location: '.get_config('url').'proposta/inexistente');
		exit;
	}

	$doc = $proposta->getDocumento();

	$force_view = (isset($_GET['exibir_doc']) && $_GET['exibir_doc'] == 'sim') ? true : false;

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>


<div class="etapa-body">
	<div class="container">

		<div class="alert alert-info">
			Status da proposta: <strong><?php echo mb_strtoupper($proposta->status); ?></strong>
		</div>
		
		<?php if ( !in_array($proposta->status, array('vendedor', 'backoffice', 'cadastro', 'analise')) || $currentuser->temPermissao(array('administradora','desenvolvedor')) || $force_view ) : ?>

			<div class="text-center" style="margin-bottom: 30px;">
				<a href="#" class="btn btn-block btn-primary btn-lg print-this" data-target="#proposta-documento">Imprimir proposta</a>
			</div>

			<div id="proposta-documento"><?php echo $doc->getHtml(); ?></div>


		<?php else : ?>
			
			<div class="alert alert-info">
				<strong>Aguarde!</strong> A proposta está em análise pelo setor responsável e se for necessário qualquer correção você será notificado.
			</div>

		<?php endif; ?>

		<hr>

		<div class="text-center">
			<a href="<?php echo get_url('proposta/historico'); ?>" class="btn btn-primary">Histórico</a>
		</div>
		
	</div>
</div>

<?php get_footer(); ?>