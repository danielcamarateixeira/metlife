<?php

	include VIEW . '/_auth.php';

	// Gera uma nova proposta e redireciona o usuário
	$proposta = new Proposta;
	$proposta->usuarioid = $currentuser->getID();
	$proposta->etapa = 'corretora';
	$proposta->status = 'corretor';
	$proposta->datecreate = date('Y-m-d H:i:s');
	$proposta->dateupdate = date('Y-m-d H:i:s');
	$proposta->remoteip = $_SERVER['REMOTE_ADDR'];
	$proposta->remoteinfo = $_SERVER ['HTTP_USER_AGENT'];
	$proposta->save();
	$proposta->geraProtocolo();
	$proposta->geraHash();
	$proposta->setHistory('Criou a proposta');
	$proposta->save();

	// if ( $currentuser->cargo == 'desenvolvedor' ) {
	// 	debug($proposta->getProperties());
	// 	exit;
	// }

	header('Location: ' . get_config('url') . 'proposta/corretora?id=' . $proposta->getHash());
	exit;

?>