<header class="proposta-header">

	<div class="container">

		<div class="row row-flex align-center">

			<div class="col-xs-10">

				<div class="proposta-info">
					<p class="vendedor"><span>Vendedor:</span> <strong><?php echo $currentuser->nome; ?></strong></p>
					<?php if (isset($proposta) && $proposta->corretora) : ?>
						<p class="entidade"><span>Corretora:</span> <strong><?php echo $proposta->corretora->nome; ?></strong></p>
					<?php endif; ?>
				</div>

			</div>

			<div class="col-xs-2 text-center">
				<a class="menu-bar btn btn-outline btn-default" data-toggle="collapse" href="#main-nav">
		            <i class="fa fa-bars" aria-hidden="true"></i>
		        </a>
			</div>

		</div>

	</div>

</header>

<nav class="navbar navbar-proposta">

	<div class="collapse main-nav" id="main-nav">
	    <ul class="nav navbar-nav">
	        <li><a href="<?php echo get_config('url'); ?>proposta/nova" class="btn btn-success">NOVA PROPOSTA</a></li>
			<li><a href="<?php echo get_config('url'); ?>proposta/historico" class="btn btn-primary btn-light">HISTÓRICO</a></li>
			<li><a href="<?php echo get_config('url'); ?>faq" class="btn btn-primary btn-light">FAQ & BOAS PRÁTICAS</a></li>
			<li><a href="<?php echo get_url('perfil'); ?>" class="btn btn-primary btn-light btn-block">PERFIL</a></li>
			<li><a href="<?php echo get_config('url'); ?>sair" class="btn btn-primary btn-light">SAIR</a></li>
	    </ul>   
	</div>
	
</nav>