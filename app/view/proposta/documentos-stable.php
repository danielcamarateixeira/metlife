<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) ) {

		// Manuseia todos os uploads
		if ( count($_FILES) > 0 ) foreach ( $_FILES as $name => $_SFILE ) {
			$file = new Upload( $_SFILE );
			if ( $file->uploaded ) {
				$file->file_new_name_body = md5(uniqid(rand(), true));
				$file->file_overwrite = false;
				$file->Process( UPLOADS . '/documentos/' . date('Y-m') );
				if ( $file->processed )
					$proposta->$name = UPLOADS_REL . '/documentos/' . date('Y-m') . '/' . $file->file_dst_name;
			}
		}
		$proposta->{'tipo-assinatura'} = $_POST['tipo-assinatura'];
		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Altera a etapa da proposta
		$proposta->etapa = 'finalizacao';
		// Cria a interação no histórico
		$proposta->setHistory('Enviou documentos');
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/finalizacao?id='.$proposta->getHash()); exit;

	}

	if ( isset($_GET['pular']) ) {

		// Altera a etapa da proposta
		$proposta->etapa = 'finalizado';
		// Salva as atualizações
		$proposta->save();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/finalizacao?id='.$proposta->getHash()); exit;

	}

	if ( isset($_GET['deletar']) ) {
		$file_slug = base64_decode( $_GET['deletar'] );
		$proposta->$file_slug = '';
		$proposta->serializeFields();
		$proposta->save();
	}

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('documentos');

	// Documentos
	$doc_titular = array(
		'identidade' => 'Documento de identidade (RG/CNH/CPF)',
		'residencia' => 'Comprovante de residência'
	);
	$doc_dep = array(
		'identidade' => 'Documento de identidade (RG/CNH/CPF)',
		// 'residencia' => 'Comprovante de residência',
		'vinculo' => 'Comprovante de vínculo (Certidão de casamento/nascimento/etc.)'
	);

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>

			<?php if ( isset($_GET['deletar']) ) : ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					Documento excluído com sucesso!
				</div>
			<?php endif; ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post" enctype="multipart/form-data">

				<div class="form-group-container row">

					<h4>Documentos do proponente titular: <?php echo $proposta->nome; ?></h4>

					<div class="form-field-group">
					
						<?php foreach ( $doc_titular as $slug => $label ) : ?>

							<div class="form-group col-sm-6">

								<label><?php echo $label; ?></label>
								<?php if ( !$proposta->{'doc-titular-'.$slug} || ($proposta->{'doc-titular-'.$slug} && $currentuser->temPermissao(array('desenvolvedor','administradora','cadastro'))) ) : ?>
									<div class="alert alert-warning alert-sm">
										Arquivos permitidos apenas <strong>PDF</strong> e <strong>JPG</strong> com até <strong>5MB</strong>
									</div>
									<input type="file" name="doc-titular-<?php echo $slug; ?>" class="form-control" accept=".jpg,.jpeg,.pdf,image/jpeg,application/pdf">
								<?php endif; ?>

								<?php if ( $proposta->{'doc-titular-'.$slug} ) : ?>
									<div class="alert alert-success">
										<a href="<?php echo get_url( 'proposta/documentos?id='.$proposta->getHash().'&deletar='.base64_encode('doc-titular-'.$slug) ); ?>" class="close confirm" data-alert="Essa ação não poderá ser desfeita." data-dismiss="alert" aria-hidden="true" title="Excluir documento"><i class="fa fa-trash" aria-hidden="true"></i></a>
										<a href="<?php echo $proposta->{'doc-titular-'.$slug}; ?>" target="_blank"><?php echo get_file_icon($proposta->{'doc-titular-'.$slug}); ?> Visualizar documento</a>
									</div>
								<?php endif; ?>

							</div>

						<?php endforeach; ?>

					</div>

				</div>

				<?php if ( $proposta->{'quantidade-dependentes'} > 0) for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) : ?>

					<div class="form-group-container dependente-<?php echo $i; ?> row">

						<h4>Documentos do dependente: <?php echo $proposta->{"dependente{$i}-nome"}; ?></h4>

						<div class="form-field-group">

							<?php foreach ( $doc_dep as $slug => $label ) : ?>

								<div class="form-group col-sm-6">

									<label><?php echo $label; ?></label>
									<?php if ( !$proposta->{'doc-dependente'.$i.'-'.$slug} || ($proposta->{'doc-dependente'.$i.'-'.$slug} && $currentuser->temPermissao(array('desenvolvedor','administradora','cadastro'))) ) : ?>
										<div class="alert alert-warning alert-sm">
											Arquivos permitidos apenas <strong>PDF</strong> e <strong>JPG</strong> com até <strong>5MB</strong>
										</div>
										<input type="file" name="doc-dependente<?php echo $i; ?>-<?php echo $slug; ?>" class="form-control" accept=".jpg,.jpeg,.pdf,image/jpeg,application/pdf">
									<?php endif; ?>

									<?php if ( $proposta->{'doc-dependente'.$i.'-'.$slug} ) : ?>
										<div class="alert alert-success">
											<a href="<?php echo get_url( 'proposta/documentos?id='.$proposta->getHash().'&deletar='.base64_encode('doc-dependente'.$i.'-'.$slug) ); ?>" class="close confirm" data-alert="Essa ação não poderá ser desfeita." data-dismiss="alert" aria-hidden="true" title="Excluir documento"><i class="fa fa-trash" aria-hidden="true"></i></a>
											<a href="<?php echo $proposta->{'doc-dependente'.$i.'-'.$slug}; ?>" target="_blank"><?php echo get_file_icon($proposta->{'doc-dependente'.$i.'-'.$slug}); ?> Visualizar documento</a>
										</div>
									<?php endif; ?>

								</div>

							<?php endforeach; ?>

						</div>

					</div>

				<?php endfor; ?>

				<?php

					// Documentos necessários pela entidade
					if ( $proposta->filiado != 'SIM' && file_exists( TEMPLATES . '/metlife-filiacao-'.slugify($proposta->entidade->nome).'-documentos.json' ) ) {
						$form = new Form( TEMPLATES . '/metlife-filiacao-'.slugify($proposta->entidade->nome).'-documentos.json', $proposta->getProperties() );
						echo $form->getHtml();
					}

				?>

				<hr>

				<div class="form-group tipo-assinatura-container">
					<select name="tipo-assinatura" id="tipo-assinatura" class="form-control">
						<option>Manuscrita</option>
						<option>Digital</option>
					</select>
				</div>

				<div class="alert alert-assinatura alert-danger digital">
					Colete a assinatura via aplicativo ou fotografia legível com fundo branco.
				</div>
				<div class="alert alert-assinatura alert-warning manuscrita">
					<strong>Atenção!</strong> Será preciso entregar a assinatura manuscrita à Administradora de Benefícios.
				</div>

				<div class="form-group assinatura-digital">
					<label class="sr-only">Assinatura Digital</label>
					<?php if ( !$proposta->{'doc-assinatura-digital'} || ($proposta->{'doc-assinatura-digital'} && $currentuser->temPermissao(array('desenvolvedor','administradora','cadastro'))) ) : ?>
						<div class="alert alert-warning alert-sm">
							Arquivos permitidos apenas <strong>JPG</strong> com até <strong>5MB</strong>
						</div>
						<input type="file" name="doc-assinatura-digital" id="input-assinatura-digital" class="form-control" accept=".jpg,.jpeg,image/jpeg">
					<?php endif; ?>
				</div>

				<?php if ( $proposta->{'doc-assinatura-digital'} ) : ?>
					<div class="alert alert-success">
						<a href="<?php echo get_url( 'proposta/documentos?id='.$proposta->getHash().'&deletar='.base64_encode('doc-assinatura-digital') ); ?>" class="close confirm" data-alert="Essa ação não poderá ser desfeita." data-dismiss="alert" aria-hidden="true" title="Excluir documento"><i class="fa fa-trash" aria-hidden="true"></i></a>
						<a href="<?php echo $proposta->{'doc-assinatura-digital'}; ?>" target="_blank"><?php echo get_file_icon($proposta->{'doc-assinatura-digital'}); ?> Visualizar assinatura</a>
					</div>
				<?php endif; ?>

				<div class="assinaturas row row-flex align-top">
					<div class="col-sm-6">
						<a href="#" class="btn btn-default btn-lg btn-assinatura" id="btn-assinatura-digital" data-assinatura="Digital">
							<div class="col-xs-3">
								<img src="<?php the_image('assinatura-digital.png'); ?>" alt="Enviar assinatura digital" class="img-responsive">
							</div>
							<div class="col-xs-9 text">
								<strong>Enviar assinatura</strong>
								<h3>DIGITAL</h3>
							</div>
						</a>
					</div>
					<div class="col-sm-6">
						<a href="#" class="btn btn-default btn-lg btn-assinatura" id="btn-assinatura-manuscrita" data-assinatura="Manuscrita">
							<div class="col-xs-3">
								<img src="<?php the_image('assinatura-manuscrita.png'); ?>" alt="Informar assinatura manuscrita" class="img-responsive">
							</div>
							<div class="col-xs-9 text">
								<strong>Informar assinatura</strong>
								<h3>MANUSCRITA</h3>
							</div>
						</a>
						<a href="#" class="btn btn-default btn-lg btn-assinatura print-this" data-target="#ficha-assinatura">
							<div class="col-xs-3">
								<img src="<?php the_image('assinatura-card.png'); ?>" alt="Imprimir ficha de assinatura" class="img-responsive">
							</div>
							<div class="col-xs-9 text">
								<strong>Imprimir ficha de assinatura</strong>
							</div>
						</a>
					</div>
				</div>

				<div class="form-action row">
					<div class="col-xs-12 col-sm-6">
						<a href="<?php echo get_config('url'); ?>proposta/documentos?id=<?php echo $proposta->getHash(); ?>&pular" class="btn btn-lg btn-block btn-danger">Coletar documentos depois</a>
					</div>
					<div class="col-xs-12 col-sm-6">
						<button type="submit" class="btn btn-lg btn-block btn-success btn-prosseguir btn-upload">Enviar documentos e prosseguir</button>
					</div>
				</div>

			</form>
			
		</div>
	</div>

	<div class="hidden container">
		<div id="ficha-assinatura">
			<?php

				$doc = new Documento( TEMPLATES . '/ficha-assinatura.phtml' );
				$doc->fields = $proposta;
				$doc->process();
				echo $doc->getHtml();

			?>
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<div id="proposta-data" class="hidden"><?php echo $proposta->campos; ?></div>
<?php //echo $proposta->getPropertiesJs(); ?>


<?php get_footer(); ?>