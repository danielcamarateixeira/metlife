<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) && count($_POST)>0 ) {

		// Coloca todos os valores dos campos do formulário na classe
		$proposta->setPostData( $_POST );
		// Corrige o problema com o nome da corretora 28/12/2017
		if ($proposta->corretora_nome != '') {
			$corretora = new Corretora;
			$corretora->loadByID($_POST['corretoraid']);
			$proposta->corretora_nome = $corretora->nome;
		}
		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Define o ID da entidade selecionada
		$proposta->corretoraid = $_POST['corretoraid'];
		// Altera a etapa da proposta
		$proposta->etapa = 'entidade';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/entidade?id='.$proposta->getHash()); exit;

	}

	// $form = new Form( TEMPLATES . '/metlife-corretora.json', $proposta->getProperties() );

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('corretora');

	$corretora_list = new Corretora;
	$corretora_list->loadList();

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post">

				<div class="row">
					
					<div class="col-12 col-sm-4">
						<div class="form-group input-corretora">
							<input type="text" id="inputCorretoraCod" name="inputCorretoraCod" class="form-control" required value="<?php if ($proposta->corretora_codigo) echo $proposta->corretora_codigo; ?>" placeholder="Digite o código da corretora">
						</div>
					</div>
					<div class="col-12 col-sm-8">
						<div class="form-group input-corretora-nome">
							<input type="text" id="inputCorretora" name="inputCorretora" class="form-control" required value="<?php if ($proposta->corretora_codigo) echo $proposta->corretora_codigo; ?>">
						</div>
					</div>
					<input type="hidden" id="corretoraId" name="corretoraid" value="<?php if ($proposta->corretoraid) echo $proposta->corretoraid; ?>" required>

					<script type="text/javascript">
						jQuery(document).ready(function($) {
							$('#inputCorretoraCod').keyup(function(event) {
								verifyCorr();
							});
							$('#inputCorretora').keypress(function(event) {
								event.preventDefault();
							});

							function verifyCorr () {
								var cod = $('#inputCorretoraCod').val();
								console.log('verifyCorr');
								$.get('/sandbox/metlife/ajax/corretora-cod', {cod: cod}, function(data, textStatus, xhr) {
									console.log(data);
									if ( data.error ) {
										$('.input-corretora').removeClass('has-success').addClass('has-error');
										$('#inputCorretora').val( '' );
										$('.input-corretora .help-block').remove();
										$('.input-corretora').append('<span class="help-block">'+data.error+'</span>');
									}
									else {
										$('#corretoraId').val( data.corretoraid );
										$('#inputCorretora').val( data.nome );
										$('.input-corretora .help-block').remove();
										$('.input-corretora').removeClass('has-error').addClass('has-success');
									}
								}, 'json');
							}
						});
					</script>

				</div>

				<div class="form-group select-corretora">

					<?php /* Autocomplete version (v2)

					<input type="text" id="inputCorretora" class="form-control" required value="<?php if ($proposta->corretora_nome) echo $proposta->corretora_nome; ?>" placeholder="Digite o nome da corretora...">
					<input type="hidden" id="corretoraId" name="corretoraid" value="<?php if ($proposta->corretoraid) echo $proposta->corretoraid; ?>" required>

					<script type="text/javascript">
						jQuery(document).ready(function($) {
							var corr = [
								<?php foreach ($corretora_list->getList() as $corretora) : ?>
									{
										value: "<?php echo $corretora->nome; ?>",
										id: "<?php echo $corretora->getID(); ?>"
									},
								<?php endforeach; ?>
							];
							$('#inputCorretora').autocomplete({
								minLength: 3,
								source: corr,
								select: function (event, ui) {
									$('#corretoraId').val(ui.item.id);
									$('#inputCorretora').val(ui.item.value);
									return false;
								},
								change: function (event, ui) {
									if (!ui.item) {
										$('#inputCorretora').val("");
										$('#corretoraId').val("");
									}
								}
							});
						});
					</script>

					*/ ?>

					<?php /* Legacy Dropdown List 

					<select id="corretora" name="corretoraid" class="form-control select-placeholder" required>

						<option readonly disabled value="" <?php if (!$proposta->corretoraid) echo 'selected'; ?>>Selecione uma corretora...</option>

						<?php foreach ($corretora_list->getList() as $corretora) : ?>

							<option value="<?php echo $corretora->getID(); ?>" <?php if($proposta->corretoraid && $proposta->corretoraid == $corretora->getID()) echo 'selected'; ?>><?php echo $corretora->nome; ?></option>

						<?php endforeach; ?>

					</select>

					*/ ?>

					<hr>

					<?php // echo $form->getHtml(); ?>

				</div>

				<div class="form-action row">
					<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>