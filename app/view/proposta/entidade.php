<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) && count($_POST)>0 ) {

		// Coloca todos os valores dos campos do formulário na classe
		$proposta->setPostData( $_POST );
		// Corrige o problema com o nome da entidade 21/12/2017
		if ($proposta->entidade_nome != '') {
			$entidade = new Entidade;
			$entidade->loadByID($_POST['entidadeid']);
			$proposta->entidade_nome = $entidade->nome;
		}
		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Define o ID da entidade selecionada
		$proposta->entidadeid = $_POST['entidadeid'];
		// Altera a etapa da proposta
		$proposta->etapa = 'titular';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/titular?id='.$proposta->getHash()); exit;

	}

	$form = new Form( TEMPLATES . '/metlife-entidade.json', $proposta->getProperties() );

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('entidade');

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post">

				<div class="form-group radio-entidade">

					<?php foreach ($proposta->corretora->entidades as $entidade) : ?>

						<label class="radio-inline">
							<input type="radio" name="entidadeid" id="inputEntidadeid" value="<?php echo $entidade->getID(); ?>" required <?php if($proposta->entidadeid && $proposta->entidadeid == $entidade->getID()) echo 'checked'; ?>>
							<?php echo $entidade->nome; ?>
						</label>

					<?php endforeach; ?>

					<hr>

					<?php echo $form->getHtml(); ?>

				</div>

				<div class="form-action row">
					<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>