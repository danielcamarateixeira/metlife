<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) && count($_POST)>0 ) {

		if ( !isset($_GET['pular']) ) {
			// Coloca todos os valores dos campos do formulário na classe
			$proposta->setPostData( $_POST );
			// Transforma os campos que não são colunas do BD em JSON
			$proposta->serializeFields();
		}
		// Altera a etapa da proposta
		$proposta->etapa = 'adesao';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/adesao?id='.$proposta->getHash()); exit;

	}

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('filiacao');

	$form_action  = '?id=' . $proposta->getHash();
	$form_action .= ($proposta->filiado == 'SIM') ? '&salvar&pular' : '&salvar';
	$form_action .= (isset($_GET['revisao'])) ? '&revisao=' . $_GET['revisao'] : '';
	/* ?id=<?php echo $proposta->getID(); ?>&<?php if($proposta->filiado == 'SIM') ?><?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?> */

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>

			
			<form action="<?php echo $form_action; ?>" method="post">

				<?php /*
				<?php if ( $proposta->filiado == 'SIM' ) : ?>

					<div class="alert alert-warning">
						Já que esse usuário já possui filiação (marcado na etapa <strong>Entidade</strong>) não será necessário o preenchimento de campos nessa etapa.
					</div>
					<input type="hidden" name="id" value="<?php echo $proposta->getHash(); ?>">

				<?php else: ?>
				*/ ?>
					<div class="form-group radio-entidade">

						<h4>Ficha de filiação da entidade <strong><?php echo $proposta->entidade->nome; ?></strong></h4>

						<?php

							if ( file_exists( TEMPLATES . '/metlife-filiacao-'.slugify($proposta->entidade->nome).'.json' ) ) {
								$form = new Form( TEMPLATES . '/metlife-filiacao-'.slugify($proposta->entidade->nome).'.json', $proposta->getProperties() );
								echo $form->getHtml();
							}
							else {
								echo sprintf('<div class="alert alert-danger">A entidade %s não necessita de campos adicionais. Por favor, clique em prosseguir.</div>', $proposta->entidade->nome);
							}

						?>

					</div>

				<?php // endif; ?>

				<div class="form-action row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
					</div>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>