<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) ) {

		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Altera a etapa da proposta
		$proposta->etapa = 'finalizacao';
		// Altera o status da proposta
		$proposta->status = 'backoffice';
		// Cria uma nova interação no histórico
		$proposta->setHistory('Finalizou o cadastro da proposta');
		// Salva as atualizações
		$proposta->save();
		// Envia o e-mail para o cliente e para a administradora
		if ( !$proposta->mail_cliente_analise )
			$proposta->sendMailEmAnalise();
		// Envia o SMS para o cliente
		// if ( !$proposta->sms_cliente_aceite || $proposta->sms_cliente_aceite == 0 )
			$proposta->sendSmsAceite();
		// Redireciona o usuário para a próxima etapa
		go_to( get_url('proposta/concluido').'?id='.$proposta->getHash() );

	}

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('finalizacao');

	// Gera os documentos
	$doc = $proposta->getDocumento();
	$doc_filiacao = $proposta->getDocumentoFiliacao();

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar" method="post">

				<div class="alert alert-danger">
					<strong>Atenção!</strong> Ao clicar em finalizar será enviado um e-mail ao cliente e a proposta entrará em análise, portanto não será possível editar essa proposta.
				</div>

				<div class="alert alert-info">
					<i class="fa fa-commenting" aria-hidden="true"></i> Ao clicar em finalizar também será enviado um SMS para o número de celular <?php echo $proposta->celular; ?>.
				</div>

				<?php if ( $proposta->getDocumentosPendentes() ) : ?>

					<div class="alert alert-danger">
						<strong>Documentos pendentes!</strong> Os seguintes documentos não foram enviados e são necessários para a finalização da proposta:
						<ul><?php foreach($proposta->getDocumentosPendentes() as $docname) echo '<li>'.$proposta->getAutoDocName( $docname ).'</li>'; ?></ul>
					</div>

					<div class="form-action row">
						<div class="col-xs-12">
							<button type="button" disabled class="btn btn-lg btn-block btn-danger">Finalizar</button>
						</div>
					</div>

				<?php else : ?>

					<div class="form-action row">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-lg btn-block btn-success">Finalizar</button>
						</div>
					</div>

				<?php endif; ?>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>