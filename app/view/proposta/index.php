<?php

	$url = get_config('url') . 'proposta/historico';

	if ( !isset($_GET['id']) )
		go_to($url);
	else {
		$proposta = new Proposta;
		$proposta->loadByHash($_GET['id']);
		if ( !$proposta->getID() ) go_to($url);

		if ( $proposta->status == 'corretor' && $proposta->etapa )
			include VIEW . '/proposta/' . $proposta->etapa . '.php';
		else
			go_to($url);
	}


?>