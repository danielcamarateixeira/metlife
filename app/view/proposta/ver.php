<?php

	include VIEW . '/_auth.php';

	$proposta = new Proposta;
	$proposta->loadByID( $_GET['id'] );

	// Redireciona caso não encontre a proposta
	if ( !$proposta->getID() ) {
		header('Location: '.get_config('url').'proposta/inexistente');
		exit;
	}

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>


<div class="etapa-body">
	<div class="container">
		
		<div class="alert alert-warning">
			<strong>Em manutenção</strong>
		</div>
		
	</div>
</div>

<?php get_footer(); ?>