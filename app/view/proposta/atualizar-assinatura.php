<?php

include VIEW . '/_auth.php';

$proposta = new Proposta;
$proposta->loadByID( $_GET['id'] );
$proposta->setEtapa(4);

// Redireciona caso não encontre a proposta
if ( !$proposta->getID() ) {
	header('Location: '.get_config('url').'proposta/inexistente');
	exit;
}

$assinatura_campos = array(
	'assinatura_carencia' => 'DA CARÊNCIA',
	'assinatura_adesao' => 'DOS TERMOS DE ADESÃO'
);
$options = array(
	'DEPOIS',
	'DIGITAL',
	'MANUSCRITA'
);

// Aqui trata do formulário que foi enviado
if ( isset($_GET['salvar']) && isset($_POST['id']) ) {

	// debug($_POST);
	// exit;

	unset($_POST['id']);
	// Coloca todos os valores dos campos do formulário na classe
	foreach ($_POST as $field => $value) {
		$field_value = is_array($value) ? implode(';', $value) : trim($value);

		if ($field_value != '')
			$proposta->$field = mb_strtoupper($field_value, 'UTF-8');
	}

	foreach ( $assinatura_campos as $name => $title ) {
		if ( isset($_FILES[ $name . '_digital' ]) ) {
			$assinatura = new Upload( $_FILES[ $name . '_digital' ] );
			if ( $assinatura->uploaded ) {
				// $assinatura->file_new_name_body = 'assinatura_adesao_' . $proposta->getID();
				$assinatura->file_new_name_body = md5(uniqid(rand(), true));
				$assinatura->file_overwrite = true;
				$assinatura->Process( UPLOADS . '/assinaturas/' . date('Y-m') );
				if ( $assinatura->processed )
					$proposta->{$name . '_digital'} = UPLOADS_REL . '/assinaturas/' . date('Y-m') . '/' . $assinatura->file_dst_name;
			}
		}
	}
	$proposta->save();

	header('Location: '.get_config('url').'proposta/atualizar-assinatura?id='.$proposta->getID());
	exit;

}

get_header(); ?>


<?php include VIEW . '/proposta/_header.php'; ?>


<div class="etapa-body">

	<div class="container">

		<form action="?id=<?php echo $proposta->getID(); ?>&salvar" method="post" enctype="multipart/form-data">

			<input type="hidden" name="id" value="<?php echo $proposta->getID(); ?>">

			<?php foreach ( $assinatura_campos as $name => $title ) : ?>
				<?php if ( $proposta->$name == 'DIGITAL' && $proposta->{$name.'_digital'} != '' ) : ?>

					<label for="sl_<?php echo $name; ?>">ASSINATURA <?php echo $title; ?></label>
					<div class="alert alert-success">
						Assinatura enviada!
					</div>

				<?php else : ?>

					<div class="form-group">
						<label for="sl_<?php echo $name; ?>">ASSINATURA <?php echo $title; ?></label>
						<div class="input-group">
							<div class="input-group-addon">ENVIAR ASSINATURA</div>
							<select name="<?php echo $name; ?>" id="sl_<?php echo $name; ?>" class="form-control assinatura_sl_update input-<?php echo $name; ?>" data-target="#file_<?php echo $name; ?>">
								<?php foreach ( $options as $option ) : ?>
									<option <?php echo ($proposta->$name == $option) ? 'selected':''; ?>><?php echo $option; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group <?php echo $name; ?>-digital-container" style="display: none;">
						<label for="file_<?php echo $name; ?>">Assinatura digital</label>
						<input type="file" name="<?php echo $name; ?>_digital" id="file_<?php echo $name; ?>" class="form-control input-<?php echo $name; ?>">
					</div>
				<?php endif; ?>

				<hr>

			<?php endforeach; ?>

			<div class="form-action row">
				<button type="submit" class="btn btn-lg btn-block btn-success">Salvar</button>
			</div>

		</form>
		
	</div>

</div>


<?php get_footer(); ?>