<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';


	// Salva a proposta
	if ( isset($_GET['salvar']) && count($_POST)>0 ) {

		// Coloca todos os valores dos campos do formulário na classe
		$proposta->setPostData( $_POST );
		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Altera a etapa da proposta
		$proposta->etapa = 'plano';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/plano?id='.$proposta->getHash()); exit;

	}

	$form = new Form( TEMPLATES . '/metlife-dependentes.json', $proposta->getProperties() );

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('dependentes');

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post">

				<div class="form-group">
					<label>Quantidade de dependentes</label>
					<select name="quantidade-dependentes" id="quantidade-dependentes" class="form-control">
						<option value="0">Nenhum</option>
						<?php for($i=1; $i<=10; $i++) : ?><option value="<?php echo $i; ?>" <?php if ($proposta->{'quantidade-dependentes'} && $proposta->{'quantidade-dependentes'} == $i) echo 'selected' ?>><?php echo $i; ?></option><?php endfor; ?>
					</select>
				</div>

				<hr>

				<?php echo $form->getHtml(); ?>

				<div class="form-action row">
					<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>