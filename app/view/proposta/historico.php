<?php

include VIEW . '/_auth.php';

$params = array(
	'orderby' => 'propostaid',
	'order' => 'DESC',
	'relation' => 'AND',
	array(
		'field' => 'usuarioid',
		'value' => $currentuser->getID()
	)
);

if ( !isset($_GET['tudo']) )
	$params[] = array(
		'field' => 'corretoraid',
		'value' => '0',
		'compare' => '!='
	);

$list = new Proposta;
$list->loadList($params, 'p');

get_header(); ?>


<?php include VIEW . '/proposta/_header-user.php'; ?>

<div class="proposta-historico">

	<div class="container">

		<?php if ( $currentuser->temPermissao('vendedor') ) : ?>
			<div class="historico-actions">
				<a href="<?php echo get_config('url'); ?>proposta/nova" class="btn btn-success">Nova proposta</a>
			</div>
		<?php endif; ?>

		<table class="table table-striped table-responsive table-propostas">
			<thead>
				<tr>
					<th class="text-right">Protocolo</th>
					<th class="text-center">Vigência</th>
					<th>Nome</th>
					<th class="text-center">N&ordm; de vidas</th>
					<th class="text-center">Entidade</th>
					<th>&nbsp;</th>
					<th class="text-center">Proposta</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($list->getList()) : foreach($list->getList() as $proposta) : ?>
					<?php $docs_pendentes = $proposta->getDocumentosPendentes(); ?>
					<tr>
						<td class="text-right"><?php echo $proposta->protocolo; ?></td>
						<td class="text-center"><?php echo $proposta->getVigencia() ? $proposta->getVigencia() : '-'; ?></td>
						<td><?php echo $proposta->nome; ?></td>
						<td class="text-center"><?php echo $proposta->{'quantidade-dependentes'}+1; ?></td>
						<td class="text-center"><?php echo $proposta->entidade->nome; ?></td>
						<td class="text-right historico-notificacoes">
							<?php

								$permissao_notif = (($currentuser->temPermissao('vendedor') && $proposta->status == 'corretor') || $currentuser->temPermissao(array('desenvolvedor','administradora')));

								$documentos_linkado = ( $permissao_notif && ( $currentuser->temPermissao(array('desenvolvedor','administradora')) || $docs_pendentes ) );

							?>

							<?php if ($permissao_notif && $proposta->getLastObservacaoHtml()) : ?>
								<a href="<?php echo $proposta->getLastObservacaoEtapaLink(); ?>">
									<span class="tem-observacoes" title="Proposta com pendências em: <?php echo $proposta->getLastObservacaoEtapaListHtml(', '); ?>"><i class="fa fa-warning"></i></span></a>
							<?php endif; ?>

							<?php if ( $documentos_linkado ) : ?>
								<a href="<?php echo get_url('proposta/documentos?id='.$proposta->getHash()); ?>" class="documentos-pendentes-link"><?php endif; ?>
									<span class="documentos-pendentes documentos-pendentes-<?php echo ($docs_pendentes) ? 'sim' : 'nao'; ?>" title="<?php echo ($docs_pendentes) ? 'Proposta com '.count($docs_pendentes).' documentos pendentes, clique para enviá-los' : 'Proposta sem documentos pendentes'; ?>"><i class="fa fa-file-pdf-o"></i><?php echo ($docs_pendentes) ? ' ('.count($docs_pendentes).')' : ''; ?></span><?php if ( $documentos_linkado ) : ?></a><?php endif; ?>&nbsp; 
						</td>
						<td class="text-center">
							<?php if ($proposta->status == 'corretor') : //if ( ( $proposta->getDocumentosPendentes() || $proposta->getLastObservacaoHtml() ) && $proposta->status == 'corretor' ) : ?>
								<a href="<?php echo get_config('url'); ?>proposta/documentos?id=<?php echo $proposta->getHash(); ?>" class="btn btn-primary">Completar</a>
							<?php else : ?>
								<a href="<?php echo get_config('url'); ?>proposta/visualizar?id=<?php echo $proposta->getHash(); ?>" class="btn btn-success">Visualizar</a>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; else : ?>
					<tr>
						<td colspan="6">
							<div class="alert alert-danger">Nenhum pedido encontrado</div>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>

		<div class="text-center legenda">
			<span class="documento-pendente"><span class="icon"><i class="fa fa-file-pdf-o"></i> (1+)</span> Documentos pendentes</span> &bull; 
			<span class="sem-documento-pendente"><span class="icon"><i class="fa fa-file-pdf-o"></i></span> Nenhum documento pendente</span> &bull; 
			<span class="tem-observacoes"><span class="icon"><i class="fa fa-warning"></i></span> Possui observações por etapa</span>
		</div>

		<div style="height: 15px;"></div>
		<div class="alert alert-xsm alert-warning">
			Algumas propostas foram ocultadas por estarem em branco ou com algum problema, para visualizá-las <a href="<?php echo get_url('proposta/historico?tudo'); ?>">clique aqui</a>.
		</div>
		
		<div class="text-center">
			<?php echo $list->getPaginationHtml(); ?>
		</div>
		
	</div>

</div>

<?php get_footer(); ?>