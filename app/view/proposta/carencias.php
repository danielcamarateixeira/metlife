<?php

include VIEW . '/_auth.php';

$proposta = new Proposta;
$proposta->loadByID( $_GET['id'] );

// Redireciona caso não encontre a proposta
if ( !$proposta->getID() ) {
	header('Location: '.get_config('url').'proposta/inexistente');
	exit;
}

// Carrega o formulário do passo 1
$form = new Form(TEMPLATES . '/metlife-3.json');

// Aqui trata do formulário que foi enviado
if ( isset($_GET['salvar']) && isset($_POST['id']) ) {

	unset($_POST['id']);
	// Coloca todos os valores dos campos do formulário na classe
	foreach ($_POST as $field => $value) {
		$field_value = is_array($value) ? implode(';', $value) : trim($value);
		
		if ($field == 'cpf' || $field == 'rg' || $field == 'cep')
			$field_value = preg_replace("/[^0-9]/", "", $field_value);

		if ($field_value != '')
			$proposta->$field = mb_strtoupper($field_value, 'UTF-8');
	}

	$proposta->serializeCampos($form);
	$proposta->save();

	header('Location: '.get_config('url').'proposta/carencias?id='.$proposta->getID());
	exit;

}


get_header(); ?>

<?php include VIEW . '/_navbar.php'; ?>

<div class="container">

	<?php include VIEW . '/proposta/_header.php'; ?>
	
	<form action="?id=<?php echo $proposta->getID(); ?>&salvar" method="post">
		
		<input type="hidden" name="id" value="<?php echo $proposta->getID(); ?>">

		<?php echo $form->getHtml(); ?>

		<div class="form-action row">
			<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
		</div>

	</form>
	

</div>

<?php get_footer(); ?>