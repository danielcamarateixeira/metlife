<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) && count($_POST)>0 ) {

		// Coloca todos os valores dos campos do formulário na classe
		$proposta->setPostData( $_POST );
		// Transforma os campos que não são colunas do BD em JSON
		$proposta->serializeFields();
		// Define o plano
		$proposta->planoid = $_POST['planoid'];
		// Altera a etapa da proposta
		$proposta->etapa = 'adesao';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Define qual é a próxima etapa
		if ( $proposta->filiado && $proposta->filiado == 'NÃO' )
			$next_step_url = get_config('url').'proposta/filiacao?id='.$proposta->getHash();
		else
			$next_step_url = get_config('url').'proposta/adesao?id='.$proposta->getHash();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.$next_step_url); exit;

	}

	// $form = new Form( TEMPLATES . '/metlife-plano.json', $proposta->getProperties() );

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('plano');

	$planos = new Plano;
	$planos->loadList();

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post">

				<div class="form-group text-center">
					<?php foreach ( $planos->getList() as $plano ) : $plano->setEntidade( $proposta->entidadeid ); ?>
						<div class="radio btn btn-primary">
							<label>
								<input type="radio" name="planoid" id="inputPlano" class="plano" value="<?php echo $plano->getID(); ?>" data-valor="<?php echo $plano->getValor()->valor; ?>" data-dependentes="<?php echo $proposta->{'quantidade-dependentes'}; ?>">
								<?php echo $plano->nome; ?>
							</label>
						</div>
					<?php endforeach; ?>
				</div>

				<table class="table table-stripped">
					<thead>
						<tr>
							<th></th>
							<th>Idade</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><label>Titular</label></td>
							<td><input type="text" name="titular-idade" value="<?php echo $proposta->getIdade( $proposta->nascimento ); ?>" readonly class="form-control"></td>
							<td>
								<div class="input-group">
	  								<span class="input-group-addon">R$</span>
									<input type="text" name="titular-valor" id="titular-valor" value="Escolha um plano..." readonly class="form-control">
								</div>
							</td>
						</tr>

						<?php if ( $proposta->{'quantidade-dependentes'} > 0 ) for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) : ?>
							<tr>
								<td><label>Dependente <?php echo $i; ?></label></td>
								<td><input type="text" name="dependente-<?php echo $i; ?>-idade" id="dependente-<?php echo $i; ?>-idade" value="<?php echo $proposta->getIdade( $proposta->{"dependente{$i}-nascimento"} ); ?>" readonly class="form-control"></td>
								<td>
									<div class="input-group">
	  									<span class="input-group-addon">R$</span>
	  									<input type="text" name="dependente-<?php echo $i; ?>-valor" id="dependente-<?php echo $i; ?>-valor" value="Escolha um plano..." readonly class="form-control">
	  								</div>
	  							</td>
							</tr>
						<?php endfor; ?>

					</tbody>
					<tfoot>
						<tr>
							<td>Total</td>
							<td></td>
							<td>
								<div class="input-group">
	  								<span class="input-group-addon">R$</span>
									<input type="text" name="valor-total" id="valor-total" value="Escolha um plano..." readonly class="form-control">
								</div>
							</td>
						</tr>
					</tfoot>
				</table>

				<?php 

					$form_vencimento = new Form( TEMPLATES . '/metlife-plano-vencimento.json', $proposta->getProperties() );
					echo $form_vencimento->getHtml();

				?>
				
				<div class="carencias alert alert-danger">
					<h4>CARÊNCIAS</h4>
					<p>24 (vinte e quatro) horas para procedimentos de Urgência e Emergência.</p>
					<p>30 (trinta) dias para procedimentos das demais especialidades.</p>
				</div>

				<div class="taxa-angariacao alert alert-warning">
					<h4>TAXA DE ANGARIAÇÃO</h4>
					<p>A Taxa de Angariação é devida ao Angariador desta Proposta no ato de sua assinatura, por conta da Intermediação do Benefício, e equivale a 100% (cem por cento) do valor total do(s) benefício(s) contratado(s). Em caso de não aceitação dessa Proposta, essa taxa será integralmente devolvida pelo Angariador ao Proponente Titular.</p>
					<p>ATENÇÃO: O pagamento da Taxa de Angariação É OBRIGATÓRIA e NÃO se confunde, isenta, exclui ou substitui o pagamento da primeira e das demais cobranças mensais do benefício, as quais são arrecadadas pela Administradora de Benefícios e darão direito às coberturas decorrentes desta Proposta.</p>
				</div>

				<?php 

					$form_valores = new Form( TEMPLATES . '/metlife-plano-valores.json', $proposta->getProperties() );
					echo $form_valores->getHtml();

				?>

				<hr>

				<div class="alert alert-danger">
					<strong>DECLARAÇÃO DE CIÊNCIA DE REAJUSTE</strong><br>
					Declaro que, por ocasião da contratação, recebi todas as informações pertinentes às diferenças entre o plano ora contratado (Plano Coletivo por Adesão) e os Planos Individuais/Familiares, sobretudo no tocante à forma e à periodicidade do reajuste, cuja data base será verificada a partir da contratação entre minha entidade e a administradora de benefícios (e não no momento da assinatura desta proposta). Declaro, ainda, que me foi explicada a abrangência da clausula 18 deste contrato, cujo texto está em total conformidade com a Resolução Normativa n° 195 da ANS (Agência Nacional de Saúde), que em seu Art. 19, §1, dispõe que: "Para fins do disposto no caput, considera-se reajuste qualquer variação positiva na contraprestação pecuniária, inclusive aquela decorrente de revisão ou reequilíbrio econômico-atuarial do contrato". Por fim, declaro que me foi informado que os índices de reajuste de meu contrato não seguirão os parâmetros aplicáveis aos Planos Individuais/Familiares, podendo variar em percentuais maiores tendo em vista uma eventual alta sinistralidade que venha a ser apurada a cada período de 12 meses.
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-xs-6 text-center">
							<div class="radio">
								<label>
									<input type="radio" name="autorizo-envio" id="input" value="Autorizo o envio de informações sobre o benefício contratado." checked="checked">
									Autorizo o envio de informações* sobre o benefício contratado.
								</label>
							</div>
						</div>
						<div class="col-xs-6 text-center">
							<div class="radio">
								<label>
									<input type="radio" name="autorizo-envio" id="input" value="Não autorizo o envio de informações sobre o benefício contratado.">
									Não autorizo o envio de informações* sobre o benefício contratado.
								</label>
							</div>
						</div>
					</div>
					<div class="text-center"><small>*informações relevantes, tais como reajustes, ofertas de novos serviços e benefícios.</small></div>
				</div>
				

				<div class="form-action row">
					<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>