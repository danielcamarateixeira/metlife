<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	// Salva a proposta
	if ( isset($_GET['salvar']) ) {

		// Altera a etapa da proposta
		$proposta->etapa = 'documentos';
		// Registra a edição por parte de backoffice/cadastro
		if ( isset($_GET['revisao']) ) $proposta->setHistory('Proposta editada pelo '.mb_convert_case($_GET['revisao'], MB_CASE_TITLE), 'Etapa: '.mb_strtoupper($proposta->etapa));
		// Salva as atualizações
		$proposta->save();
		// Fecha a janela caso seja uma revisão
		if ( isset($_GET['revisao']) ) close_window();
		// Redireciona o usuário para a próxima etapa
		header('Location: '.get_config('url').'proposta/documentos?id='.$proposta->getHash()); exit;

	}

	$etapas = new PropostaEtapa;
	$etapas->setCurrent('adesao');

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<?php echo $etapas->getBeforeCurrentHtml(); ?>

	<div class="etapa-body">
		<div class="container">

			<?php echo $proposta->getLastObservacaoEtapaHtml($etapas->getCurrentSlug(), 'corretor', 'backoffice'); ?>
			
			<form action="?id=<?php echo $proposta->getHash(); ?>&salvar<?php if (isset($_GET['revisao'])) echo "&revisao=$_GET[revisao]"; ?>" method="post">

				<h3>Termo de Adesão ao Plano de Assistência a saúde</h3>
				<h4>Plano Odontológico</h4>
				<p class="text-justify">
					Contrato Coletivo Por Adesão<br>
					Pelo Presente declaro expressamente para todos os fins de  
					direito, ter recebido o manual de Orientação para contratação  
					do Plano de assistência Odontológico e o guia de leitura  
					contratual — GLC e estar ciente que de acordo com as seguintes  
					normas para utilização do plano odontológico, as quais me  
					obrigam, por mim e por meu (s) Dependente (s) eAgregado (s):
					1. Este instrumento é meu contrato de Adesão (a "Proposta")  
					ao contrato de plano de Assistência a Saúde - Plano  
					Odontológico Coletivo por Adesão (o "Benefício"), celebrado  
					entre a Nunes & Grossi Administradora de Benefícios e  
					Serviços Ltda. Sediada em Santos na Av. Ana Costa n° 374 - 
					2° andar - inscrita no CNPJ sob n° 11925725/0001-07 e na  
					ANS sob o n° 41.764-5 (a "Administradora de Benefícios") e  
					Metlife Planos Odontológicos Ltda sediada em São Paulo, na  
					Av. Eng. Luis Carlos Berrini, n° 1253 - 14° andar - Cidade  
					Monções - CEP 04571-010, inscrita sob o n° CNPJ  
					03273825/0001-78 e na ANS sob ° 40.648-1 (a "Operadora") é  
					destinado a população que mantenha vínculo com a minha  
					Entidade que é a pessoa jurídica legitimada, indicada na  
					página 1 (um) desta proposta.
					2. A aceitação de minha Proposta depende da comprovação de  
					meu vínculo com a Entidade, sendo que os documentos  
					comprobatórios desse vínculo estão sendo entregues por mim  
					ao angariador, no ato da assinatura desta, para que a  
					idoneidade deles seja conferida em até 15 (quinze) dias pela  
					Administradora de Benefícios, podendo esta proposta, ser  
					recusada em razão da falta de minha elegibilidade. Em caso de  
					recusa desta Proposta, a Taxa de Angariação será devolvida a  
					mim pelo angariador que a recebeu.
					3. Somente serão aceitos como dependentes, meu cônjuge ou  
					meu (minha) companheira (a); o (a) meu(minha) filho(a) solteiro  
					o(a) meu(minha) enteado(a) solteiro(a) sob minha guarda ou  
					tutela judicial todos até 40 Anos. Os Associados Dependente(s)  
					devem obrigatoriamente, estar inscritos no mesmo tipo de  
					Plano Odontológico optado pelo Titular.
					4. Sou o único responsável pelos documentos e informações  
					fornecidos por mim e por meu (s) dependente (s) sobre toda e  
					qualquer circunstância que possa influir na aceitação desta  
					proposta, na manutenção ou no valor mensal do benefício,  
					sabendo que omissões ou dados errôneos acarretarão a perda  
					de todos os meus direitos, bem como os do (s) meu (s)  
					dependente (s), decorrentes do benefício;
					5. Após a aceitação desta Proposta, o benefício terá inicio na  
					data indicada no campo. " Data de Vigência", e tanto eu quanto  
					meu (s) dependente (s) indicado (s) passaremos a ser  
					denominados "beneficiários";
					6. Assim que eu assumir a condição de beneficiário titular fica  
					outorgado à Administradora de Benefícios amplos poderes para  
					me representar, assim como o(s) meu(s) beneficiário(s)  
					dependente(s), perante a Operadora e outros órgãos, em  
					especial a ANS, no cumprimento e/ou nas alterações deste  
					benefício, bem como nos reajustes dos seus valores mensais;
					7. O contrato coletivo firmado entre a Administradora clel  
					Benefícios e a Operadora, contrato que passarei a integrar,  
					vigorará por prazo indeterminado, desde que não ocorra  
					denúncia, por escrito, de qualquer das partes, seja pela  
					Administradora de Benefícios ou pela Operadora. Em caso de  
					rescisão desse contrato coletivo, a Administradora de  
					Benefícios me fará a Comunicação desse fato, com  
					antecedência mínima de 30(trinta) dias.
					8. Contrato coletivo por adesão somente poderá ser  
					rescindido imotivadamente após a vigência do período de 12  
					meses. Caso o Beneficiário solicite exclusão, ou fique  
					inadimplente dentro do período mínimo de vigência deste  
					CONTRATO, será cobrado o valor da contribuição mensal  
					vigente à época do fato, multiplicada pelo número de meses  
					faltantes para completar 12 (doze) meses da contratação do  
					benefício, respeitando o multiplicador mínimo de 6 (seis). A  
					exclusão do beneficiário será efetivada mediante o envio da  
					solicitação por escrito, redigida pelo titular inscrito no Plano.
					8.1 Após o período de 12 (doze) meses da adesão do  
					beneficiário ao contrato coletivo por adesão , este passará a  
					vigorar por prazo indeterminado, podendo ser rescindido pela  
					Administradora de Benefício ou pelo Beneficiário mediante  
					prévia notificação, com antecedência mínima de 60 (sessenta)  
					dias, quando serão devidas as respectivas mensalidades.
					9. Poderei, assim como meu(s) beneficiário(s) dependente(s),  
					utilizar o beneficio por meio dos prestadores credenciados da  
					Operadora respeitadas as condições contratuais de cada  
					plano. E estou ciente de que, exclusivamente em casos de  
					urgência ou emergência comprovada, ocorrida em localidades  
					onde não existam prestadores credenciados da Operadora as  
					regras estarão definidas no Manual de Orientação  
					Beneficiário/Usuário.
					10. Os prazos de carências são os períodos nos quais nem eu e  
					nem meu(s) beneficiário(s) dependente(s) teremos direito a  
					determinada coberturas, mesmo que em dia como o  
					pagamento do beneficio. Haverá prazos de carências para  
					utilização do beneficio conforme tabela indicativa da qual tive  
					ciência e que também consta do Manual de Orientação  
					Beneficiário/Usuário. Para efeitos de isenção de carências,  
					devem-se observar as normas regulamentares da ANS e a  
					legislação em vigore, se houver redução de carências, deve-se  
					observar o Aditivo de Redução de Carências que pode  
					acompanhar esta Proposta.
					11. A data de Vencimento do pagamento do valor mensal do  
					benefício, bem como sua forma, serão aquelas indicadas na  
					proposta sendo que a falta de pagamento na data do seu  
					vencimento acarretará multa compensatória de 2 % (dois por  
					cento) sobre o referido valor mensal do Benefício e acréscimo  
					de juros de 1% (um por cento) ao mês sobre o valor total do  
					benefício. No período de inadimplência, poderá ocorrer a  
					suspensão automática do benefício no dia seguinte ao  
					inadimplemento, cuja utilização somente será restabelecida  
					em até 5 (cinco) dias úteis a partir da quitação do(s) valor(es)  
					pendente(s) acrescido(s) dos encargos supracitados,  
					observada a possibilidade de cancelamento, conforme no  
					item 14, "(ii)", desta Proposta.
					12. Independentemente de meu ingresso como beneficiário,  
					o valor mensal do benefício poderá sofrer reajustes legais e  
					contratuais, de forma cumulativa (parcial ou total) ou isolada,  
					nas seguintes situações: (I) reajuste financeiro: (II) índice de  
					sinistralidade, (III) em outras hipóteses, desde que  
					conformidade com as normas e legislação em vigor
					13. Devo solicitar e informar expressamente a administradora  
					de Benefícios toda e qualquer alteração cadastral, tal como a  
					eventual perda de elegibilidade e o cancelamento da adesão  
					ao benefício, respeitando os prazos e as condições gerais e  
					particulares do Benefício.
					14. O beneficiário poderá ser cancelado: (I) por solicitação  
					expressa minha, como beneficiário titular, à administradora de  
					benefícios, observados os prazos e as condições  
					estabelecidos pela Administradora de Benefícios; (II)  
					automaticamente, pela falta de pagamento de 1 (um) valor  
					mensal do benefício por prazo superior a 30 (trinta) dias,  
					contados a partir do início da data de vigência do mês  
					inadimplido, como minha consequente exclusão e a de meu  
					(s) beneficiário(s) dependente(s), sem prejuízo de cobrança  
					do(s) valor(es) pendente(s), sem prejuízo de cobrança do(s)  
					valor(es) pendente(s) e observando também o disposto às  
					clausulas 8 a 8.1; (III) pela perda da minha elegibilidade na  
					Entidade; e (IV) por motivo de falecimento do Beneficiário. O  
					Beneficiário poderá ainda ter o atendimento suspenso  
					automaticamente após a data de vencimento nos casos de  
					falta de pagamento: O Associado também será excluído: (V)  
					em caso de recisão do contrato de Plano de Assistência  
					Odontológica celebrado entre a CONTRATANTE e a MetLife  
					(VI) quando o titular perder o vinculo com a CONTRATANTE, e  
					no caso de dependente e agregado, quando perder o vinculo  
					de dependência com o titular ou; (VII) quando devidamente  
					comprovada a fraude em documento ou informação  
					pertinentes à utilização dos serviços contratados.
					15. Tenho ciência que a Administradora de Benefícios poderá  
					realizar a cobrança judicial ou extrajudicial, caso haja  
					pendência financeira nos termos deste contrato, através de e- 
					mails, cartas, torpedo SMS e/ou qualquer outro meio legal de  
					comunicação.
					16. No caso de cancelamento do benefício, devolverei  
					imediatamente a administradora de Benefícios os cartões de  
					identificação do(s) beneficiário(s). Caso a devolução não seja  
					efetuada, assumirei ainda todas as responsabilidades civis,  
					criminais e financeiras por toda e qualquer utilização indevida  
					do benefícios incluindo a utilização por terceiros, com ou sem  
					meu consentimento.
					17. Poderei postular nova adesão ao beneficiário, mediante: (I)  
					aceitação pela Administradora de Benefícios: (II) quitação de  
					eventuais débitos anteriores: e (III) cumprimento de novos  
					prazos de carência, independentemente do período em que  
					permaneci no contrato coletivo.
					18. Devo encaminhar prontamente a Entidade, Administradora  
					de Benefícios e/ou a Operadora, quando qualquer uma delas  
					solicitar, documentos complementares e comprobatórios  
					referentes a todas as informações declaradas
					19. Poderei desistir desta Proposta, sem nenhum ônus, desde  
					que tal decisão seja comunicada por escrito à Administradora  
					de Benefícios no prazo máximo de 7 (sete) dias, contados a  
					partir da data de minha assinatura neste instrumento,  
					autorizando a cobrança da Taxa de Angariação e do valor  
					mensal do benefício, caso esse prazo não seja observado.
					20. O foro para dirimir quaisquer questões oriundas da  
					presente proposta é o de domicilio da Administradora de  
					Benefícios. Após ter lido os termos e estar totalmente de  
					acordo com as condições prévias para a aceitação desta  
					proposta, é de livre e espontânea vontade que manifesto a  
					intenção de fazer minha adesão , e a do(s) meu(s)  
					dependente(s) indicado(s) na proposta, ao benefício. Estou  
					ciente de que os cartões de identificação do(s) beneficiário(s),  
					serão enviados a mim tão logo eu e meu(s) dependente(s)  
					tenhamos sido aceitos e nossos registros estejam  
					regularizados na Operadora.
				</p>

				<div class="alert alert-warning alert-lg text-center">
					<strong>Ao clicar em prosseguir o proponente titular desta proposta atesta que leu e concordou com o Termo de Adesão acima.</strong>
				</div>

				<hr>

				<div class="form-action row">
					<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
				</div>

			</form>
			
		</div>
	</div>

	<?php echo $etapas->getAfterCurrentHtml(); ?>

</div>

<?php get_footer(); ?>