<?php

	/**
	 * Arquivo que realiza verificações se o usuário tem permissão para visualizar a proposta
	 */

	$proposta = new Proposta;
	$proposta->loadByHash( $_GET['id'] );

	// Redireciona caso não encontre a proposta
	if ( !$proposta->getID() ) {
		header('Location: '.get_config('url').'proposta/inexistente');
		exit;
	}

	// O usuário que for corretor e a etapa for diferente de 'corretor' será redirecionado
	if ( $currentuser->temPermissao('vendedor') && $proposta->status != 'corretor' )
		go_to(get_url('proposta/visualizar?id='.$_GET['id']));

?>