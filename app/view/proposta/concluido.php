<?php

	include VIEW . '/_auth.php';
	include VIEW . '/proposta/_init.php';

	$doc = $proposta->getDocumento();

	get_header();

?>

<?php include VIEW . '/proposta/_header.php'; ?>

<div class="proposta">

	<div class="etapa-body">
		<div class="container">
			
			<div class="text-center">
				<h1><i class="fa fa-check-circle" style="color: green"></i> Concluído</h1>
			</div>
			<div class="alert alert-success text-center">
				A proposta agora encontra-se em análise, acompanhe pelo <a href="<?php echo get_url('proposta/historico'); ?>">histórico</a>.
			</div>
			<hr>

			<a href="<?php echo get_url('proposta/visualizar?id='.$proposta->getHash().'&exibir_doc=sim'); ?>" class="btn btn-block btn-success">Visualizar proposta</a>
			<a href="#" class="btn btn-block btn-primary print-this" data-target="#proposta-documento">Imprimir proposta</a>
			<a href="<?php echo get_url(''); ?>" class="btn btn-block btn-primary btn-lg">Voltar ao início</a>
			<a href="<?php echo get_url('proposta/historico'); ?>" class="btn btn-block btn-success btn-lg">Histórico</a>

			<div style="display: none;">
				<div id="proposta-documento"><?php echo $doc->getHtml(); ?></div>
			</div>
			
		</div>
	</div>

</div>

<?php get_footer(); ?>
