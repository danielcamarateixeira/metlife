<?php

$auth = new Auth;

if (!$auth->isLogged()) {
	$url_now = $_SERVER['REQUEST_URI'];
	$url = get_url('login') . '?redirect=' . urlencode($url_now);
	go_to($url);
}

$currentuser = $auth->getUsuario();

$painel_current_url = (isset($URL[1])) ? $URL[1] : false;
if ( !$currentuser->temPermissao(array('backoffice', 'administradora', 'desenvolvedor')) && $painel_current_url != 'permissao' )
	go_to(get_url('backoffice/permissao'));