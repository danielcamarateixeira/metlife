<?php

	include VIEW . '/backoffice/_auth.php';

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php include VIEW . '/backoffice/section-fluxo.php'; ?>

		<?php include VIEW . '/backoffice/section-relatorios.php'; ?>

		<?php include VIEW . '/backoffice/section-recentes.php'; ?>
		
	</div>

</main>


<?php get_footer(); ?>