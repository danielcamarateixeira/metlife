<?php

	$andamento = new Proposta;
	$andamento->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'vendedor'
		),
		array(
			'field' => 'observacoes',
			'value' => ''
		)
	));

	$pendencia = new Proposta;
	$pendencia->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'vendedor'
		),
		array(
			'field' => 'observacoes',
			'value' => '',
			'compare' => '!='
		)
	));

	$backoffice = new Proposta;
	$backoffice->loadList(array(
		array(
			'field' => 'status',
			'value' => 'backoffice'
		)
	));

	$cadastro = new Proposta;
	$cadastro->loadList(array(
		array(
			'field' => 'status',
			'value' => 'cadastro'
		)
	));

?>

<div class="relatorios-backoffice row flex-row dashboard">
	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas em andamento <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas com pendências do cliente"></i></h4>
			<p class="data"><?php echo count($andamento->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas com pendências <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Propostas que foram devolvidas pelo Backoffice ou Cadastro"></i></h4>
			<p class="data"><?php echo count($pendencia->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas no backoffice</h4>
			<p class="data"><?php echo count($backoffice->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-3">
		<div class="widget item text-center">
			<h4 class="title">Propostas no cadastro</h4>
			<p class="data"><?php echo count($cadastro->getList()); ?></p>
		</div>
	</div>
</div>