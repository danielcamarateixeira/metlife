<?php

	include VIEW . '/backoffice/_auth.php';
	include VIEW . '/backoffice/_proposta-init.php';

	if ( !$proposta->getID() ) {
		header("Location: ".get_config('url').'documentos');
		exit;
	}

	$base_url = get_config('url') . 'painel/proposta';

	$doc = $proposta->getDocumento();
	$doc_filiacao = $proposta->getDocumentoFiliacao();

	get_header();

?>

<main class="main dashboard">

	<div class="container widget">

		<h1>Proposta</h1>
		<h4>Proposta #<?php echo $proposta->protocolo; ?> - <?php echo $proposta->nome; ?></h4>

		<hr>

		<div class="row">

			<div class="col-md-8">
				<div id="proposta-documento"><?php echo $doc->getHtml(); ?></div>
				<?php if ( $doc_filiacao ) : ?>
					<hr>
					<div id="ficha-filiacao"><?php echo $doc_filiacao->getHtml(); ?></div>
				<?php endif; ?>
			</div>

			<div class='col-md-4'>
				<?php echo $proposta->getHistoryHtml(); ?>
				<hr>
				<a href="<?php echo get_url('backoffice/revisar'); ?>?id=<?php echo $proposta->getHash(); ?>" class="btn btn-block btn-primary">Revisar campos</a>
				<a href="<?php echo get_url('backoffice/documentos/proposta'); ?>?id=<?php echo $proposta->getHash(); ?>" class="btn btn-block btn-primary">Revisar documentos</a>
				<hr>
				<a href="#" class="btn btn-block btn-primary print-this" data-target="#proposta-documento">Imprimir proposta</a>
				<?php if ( $doc_filiacao ) : ?>
					<a href="#" class="btn btn-block btn-primary print-this" data-target="#ficha-filiacao">Imprimir ficha de filiação</a>
				<?php endif; ?>
			</div>
			
		</div>		

	</div>

</main>

<?php get_footer(); ?>