<?php

	include VIEW . '/backoffice/_auth.php';

	if ( !$_GET['id'] ) {
		header("Location: ".get_config('url').'backoffice/documentos');
		exit;
	}

	$fileurl = base64_decode($_GET['id']);

	get_header();

?>


<div class="container">

	<div class="thumbnail">
		<img src="<?php echo $fileurl; ?>" class="img-responsive" style="width: 100%; height: auto;">
	</div>
	
</div>

<?php get_footer(); ?>