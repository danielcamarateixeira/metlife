<!DOCTYPE html>
	<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title><?php echo page_title(); ?></title>

			<?php Asset::loadCss('header'); ?>
			<?php Asset::loadJs('header'); ?>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

			<!--[if IE]>
				<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
	</head>
	<body>

	<div id="wrap">

		<header id="top">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="<?php the_image('logo-ret.png'); ?>" alt="<?php get_config('name'); ?>"></a>
					</div>
			
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo get_config('url'); ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar para o formulário</a></li>
							<?php $auth = new Auth; if ($auth->isLogged()) : ?>
								<li><a href="<?php echo get_config('url'); ?>painel/">Painel</a></li>
								<li><a href="<?php echo get_config('url'); ?>painel/pedidos">Pedidos</a></li>
								<li><a href="<?php echo get_config('url'); ?>painel/sair">Sair</a></li>
							<?php else : ?>
								<li><a href="<?php echo get_config('url'); ?>painel/login">Entrar</a></li>
							<?php endif; ?>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</nav>
		</header>