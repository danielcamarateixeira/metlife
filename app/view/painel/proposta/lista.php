<?php

	include VIEW . '/painel/_auth.php';

	$list = new Proposta;
	$list->per_page = 100;
	$list->loadList(array(
		'orderby' => 'protocolo',
		'order' => 'DESC'
	), 'p');

	$base_url = get_config('url') . 'painel/proposta';

	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>


<div class="container">

	<h1>Propostas</h1>

	<hr>

	<?php if ($list->getList()) : ?>
		<table class="table table-stripped table-responsive">
			
			<thead>
				<tr>
					<th>Protocolo</th>
					<th>Usuário</th>
					<th>Vigência</th>
					<th>Nome</th>
					<th class="text-center">N&ordm; de vidas</th>
					<th class="text-center">Entidade</th>
					<th class="text-center">Documentos</th>
					<th class="text-center" colspan="2">Proposta</th>
				</tr>
			</thead>

			<tbody>
				<?php if ($list->getList()) : foreach($list->getList() as $proposta) : ?>
					<tr>
						<td><?php echo $proposta->protocolo; ?></td>
						<td><?php echo $proposta->usuario->nome; ?></td>
						<td><?php echo $proposta->vigenciaf; ?></td>
						<td><?php echo $proposta->nome; ?></td>
						<td class="text-center"><?php echo $proposta->{'quantidade-dependentes'}+1; ?></td>
						<td class="text-center"><?php echo $proposta->entidade->nome; ?></td>
						<td class="text-center"><a href="<?php echo get_config('url'); ?>painel/documentos/proposta?id=<?php echo $proposta->getID(); ?>" class="btn btn-primary" target="_blank">Visualizar</a></td>
						<td class="text-center">
							<a href="<?php echo $base_url; ?>/visualizar?id=<?php echo $proposta->getID(); ?>" class="btn btn-success">Visualizar</a>
						</td>
					</tr>
				<?php endforeach; else : ?>
					<tr>
						<td colspan="6">
							<div class="alert alert-info">Nenhum pedido encontrado</div>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>

		</table>

		<div class="text-center">
			<?php echo $list->getPaginationHtml(); ?>
		</div>
	<?php else : ?>
		<div class="alert alert-warning">
			Não foram encontradas propostas
		</div>
	<?php endif; ?>

</div>


<?php get_footer(); ?>