<?php

	include VIEW . '/painel/_auth.php';

	$proposta = new Proposta;
	$proposta->loadByID($_GET['id']);

	if ( !$proposta->getID() ) {
		header("Location: ".get_config('url').'documentos');
		exit;
	}

	$base_url = get_config('url') . 'painel/proposta';

	$doc = $proposta->getDocumento();
	$doc_filiacao = $proposta->getDocumentoFiliacao();

	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>


<div class="container">

	<h1>Proposta</h1>
	<h4>Proposta #<?php echo $proposta->protocolo; ?> - <?php echo $proposta->nome; ?></h4>

	<hr>

	<div class="row">

		<div class="col-md-8">
			<div id="proposta-documento"><?php echo $doc->getHtml(); ?></div>
			<?php if ( $doc_filiacao ) : ?>
				<hr>
				<div id="ficha-filiacao"><?php echo $doc_filiacao->getHtml(); ?></div>
			<?php endif; ?>
		</div>

		<div class='col-md-4'>
			<?php echo $proposta->getHistoryHtml(); ?>
		</div>
		
	</div>

	

</div>

<?php get_footer(); ?>