<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-painel">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><img src="<?php the_image('logo-ret.png'); ?>" alt="<?php get_config('name'); ?>"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-painel">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo get_config('url'); ?>painel/">Painel</a></li>
				<li><a href="<?php echo get_config('url'); ?>painel/vendedor">Vendedores</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Propostas <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo get_url('painel/proposta'); ?>">Todas</a></li>
						<li><a href="<?php echo get_url('painel/proposta/analise'); ?>">Propostas em análise</a></li>
					</ul>
				</li>
				<li><a href="<?php echo get_config('url'); ?>painel/documentos">Documentos</a></li>
				<li><a href="<?php echo get_config('url'); ?>painel/corretora">Corretoras</a></li>
				<li><a href="<?php echo get_config('url'); ?>painel/usuario">Usuários</a></li>
				<li><a href="<?php echo get_config('url'); ?>painel/sair">Sair</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>