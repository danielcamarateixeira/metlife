<?php

	include VIEW . '/painel/_auth.php';

	$smslist = new GSSms;
	$smslist->loadList();


	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>

<div class="container">
	<h1>SMS</h1>

	<div class="row">

		<?php if ( $smslist->getList() ) : ?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Ref</th>
						<th>Proposta</th>
						<th>Número</th>
						<th>Mensagem</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($smslist->getList() as $sms) : ?>
						<tr>
							<td><?php echo $sms->getID(); ?></td>
							<td><?php echo $sms->smsuniqid; ?></td>
							<td>#<?php echo $sms->propostaid; ?></td>
							<td><?php echo $sms->celular; ?></td>
							<td><?php echo $sms->body; ?></td>
							<td><?php echo $sms->getStatus(); ?> <span title="<?php echo $sms->getDetail(); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i></span></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

	</div>
</div>

<?php get_footer(); ?>