<?php

	include VIEW . '/painel/_auth.php';

	get_header();

?>

<hr>

<div class="container">

	<div class="text-center">
		
	</div>
	
	<div class="alert alert-danger">
		<p><strong><i class="fa fa-lock"></i> Área restrita!</strong></p>
		<p>Você não possui a permissão necessária para acessar esse conteúdo.</p>
		<p>Por favor, faça o <a href="<?php echo get_url('sair'); ?>">logout</a> e entre com uma conta de administrador ou entre em contato com o setor responsável.</p>
	</div>

</div>

<?php get_footer(); ?>