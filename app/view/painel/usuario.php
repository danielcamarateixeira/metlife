<?php

include VIEW . '/painel/_auth.php';

$edit = new Usuario;
if (isset($_GET['id'])) {
	$edit->loadByID( $_GET['id'] );
}

if ( isset($_POST['nome']) ) {
	// Editar registro existente
	if ( isset($_GET['id']) ) {
		$edit->nome = $_POST['nome'];
		$edit->usuario = $_POST['usuario'];
		if ( $_POST['senha'] != '' )
			$edit->senha = md5($_POST['senha']);
		$edit->email = $_POST['email'];
		$edit->cargo = $_POST['cargo'];
		$edit->corretoraid = $_POST['corretoraid'];
		if ( isset($_POST['verified']) ) $edit->verificado_data = date('Y-m-d H:i:s');

		$edit->save();
	}
	// Cadastrar novo registro
	else {
		$edit->nome = $_POST['nome'];
		$edit->usuario = $_POST['usuario'];
		$edit->senha = md5($_POST['senha']);
		$edit->email = $_POST['email'];
		$edit->cargo = $_POST['cargo'];
		$edit->corretoraid = $_POST['corretoraid'];
		$edit->datecreated = date('Y-m-d H:i:s');
		if ( isset($_POST['verified']) ) $edit->verificado_data = date('Y-m-d H:i:s');
		if ( !$edit->recordExists(array(
			'relation' => 'AND',
			array(
				'field' => 'usuario',
				'value' => $_POST['usuario']
			)
		)) ) {
			$edit->save();
		}
		else {
			$error[] = 'Nome de usuário já cadastrado.';
		}
	}
}

$_u = new Usuario;
$_u->per_page = 100;
$_u->loadList(array(
	'orderby' => 'datecreated',
	'order' => 'DESC'
), 'p');

get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>


<div class="container">
	<h1>Usuário</h1>

	<div class="row">
		<div class="col-md-5">
			<form action="" method="POST">
				<legend><?php echo ($edit->getID()) ? 'Editar: '.$edit->nome : 'Cadastrar'; ?></legend>
				
				<?php if (isset($error)) : ?>
					<div class="alert alert-warning">
					<?php foreach($error as $item) : ?>
						<?php echo $item; ?><br>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input name="nome" type="text" class="form-control" id="inputNome" placeholder="Nome" <?php echo ($edit->getID()) ? 'value="'.$edit->nome.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputUsuario">Login</label>
					<input name="usuario" type="text" class="form-control" id="inputUsuario" placeholder="Login" <?php echo ($edit->getID()) ? 'value="'.$edit->usuario.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<input name="senha" type="password" class="form-control" id="inputSenha" placeholder="<?php echo ($edit->getID()) ? 'Alterar senha. (Para manter a mesma manter esse campo em branco)' : 'Senha'; ?>">
				</div>
				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<select name="cargo" id="inputCargo" class="form-control">
						<?php

							$cargos = array('vendedor','corretora','administradora','desenvolvedor','backoffice','cadastro');
							foreach ($cargos as $cargo) :
								$selected = ($cargo == $edit->cargo) ? 'selected':false;
								echo sprintf('<option value="%s" %s>%s</option>', $cargo, $selected, mb_strtoupper($cargo));
							endforeach;

						?>
					</select>
				</div>
				<div class="form-group">
					<label for="inputEmail">E-mail</label>
					<input name="email" type="email" class="form-control" id="inputEmail" placeholder="E-mail" <?php echo ($edit->getID()) ? 'value="'.$edit->email.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input name="text" type="telefone" class="form-control" id="inputTelefone" placeholder="Telefone" <?php echo ($edit->getID()) ? 'value="'.$edit->telefone.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputCpf">CPF/CNPJ</label>
					<input name="text" type="cpf" class="form-control input-cpf" id="inputCpf" placeholder="CPF/CNPJ" <?php echo ($edit->getID()) ? 'value="'.$edit->cpf.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputCorretora">Corretora</label>
					<select name="corretoraid" id="inputCorretora" class="form-control">
						<option value="0" selected>NENHUMA</option>
						<?php

							$lcorretora = new Corretora;
							$lcorretora->loadList();
							foreach ($lcorretora->getList() as $scorretora) :
								$selected = ($scorretora->getID() == $edit->corretoraid) ? 'selected' : false;
								echo sprintf('<option value="%s" %s>%s</option>', $scorretora->getID(), $selected, mb_strtoupper($scorretora->nome));
							endforeach;

						?>
					</select>
				</div>

				<?php if ( !$edit->isVerified() ) : ?>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="verified" value="1" <?php echo ($edit->getID() && $edit->isVerified()) ? 'checked' : ''; ?>>
								<i class="fa fa-check-circle"></i> Verificar perfil do usuário (só pode ser feito uma vez)
							</label>
						</div>
					</div>
				<?php endif; ?>

				<button type="submit" class="btn btn-primary"><?php echo ($edit->getID()) ? 'Salvar' : 'Cadastrar'; ?></button>
				<?php if ($edit->getID()) : ?>
					<a href="<?php echo get_config('url'); ?>painel/usuario" class="btn">Novo usuário</a>
				<?php endif; ?>
			</form>
		</div>
		<div class="col-md-7">
			<?php if ( $_u->getList() ) : ?>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Login</th>
							<th>Cargo</th>
							<th>Verificado</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($_u->getList() as $item) : ?>
							<tr>
								<td><?php echo $item->getID(); ?></td>
								<td><?php echo $item->nome; ?></td>
								<td><?php echo $item->usuario; ?></td>
								<td><?php echo mb_strtoupper($item->cargo); ?></td>
								<td style="text-align: center;"><?php echo ($item->isVerified()) ? '<i class="fa fa-check-circle"></i>' : '<i class="fa fa-clock-o"></i>'; ?></td>
								<td class="text-right">
									<a href="<?php echo get_config('url'); ?>painel/usuario?id=<?php echo $item->getID(); ?>" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $_u->getPaginationHtml(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	
</div>


<?php get_footer(); ?>