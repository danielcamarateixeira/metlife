<?php

global $currentuser;

?>
<div class="topbar-session">
	<div class="container-fluid">
		<div class="col-sm-8">
			<i class="fa fa-lock" aria-hidden="true"></i> Você está logado como <strong><?php echo $currentuser->nome; ?></strong>.
		</div>
		<div class="col-sm-4 text-right">
			<a href="<?php echo get_config('url'); ?>painel/sair" class="btn btn-primary btn-outline">Sair <i class="fa fa-sign-out" aria-hidden="true"></i></a>
		</div>
	</div>
</div>
<div class="topbar-session-spacer"></div>