<?php

	include VIEW . '/painel/_auth.php';

	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>

<div class="container">
	
	<div class="alert alert-info">
		<strong>Novidade!</strong> Acesse o <a href="<?php echo get_config('url'); ?>painel/vendedor">gerenciador de vendedores</a> para administrar os usuários que podem utilizar o sistema.
	</div>

</div>

<?php get_footer(); ?>