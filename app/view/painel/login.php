<?php

if (isset($_GET['auth'])) {
	if (empty($_POST)) {
		header("Location: ".get_config('url').'painel/login');
		exit;
	}

	$auth = new Auth;
	if ( $auth->login($_POST['usuario'], md5($_POST['senha'])) ) {
		header("Location: ".get_config('url').'painel');
		exit;
	}
	else {
		$alert[] = array('<strong>Oops</strong>, o nome de usuário ou senha estão incorretos, tente novamente', 'warning');
	}
}

include VIEW . '/painel/_header.php'; ?>

<div class="container">
	<h1>Login</h1>

	<form action="?auth" method="POST">
		<?php if (isset($alert)) foreach($alert as $item) : ?>
			<div class="alert alert-<?php echo $item[1]; ?>">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $item[0]; ?>
			</div>
		<?php endforeach; ?>
		<div class="form-field-wrap row clearfix">
			<div class="form-group col-md-4">
				<label for="usuario" class="sr-only">Usuário</label>
				<input name="usuario" type="text" class="form-control" id="usuario" placeholder="USUÁRIO">
			</div>
		</div>
		<div class="form-field-wrap row clearfix">
			<div class="form-group col-md-4">
				<label for="senha" class="sr-only">Senha</label>
				<input name="senha" type="password" class="form-control" id="senha" placeholder="SENHA">
			</div>
		</div>

		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>
</div>

<?php include VIEW . '/painel/_footer.php'; ?>