<?php

include VIEW . '/painel/_auth.php';

$_c = new Corretora;
$_c->per_page = 25;
$_c->loadList(null, 'p');


$edit = new Corretora;
if (isset($_GET['id'])) {
	$edit->loadByID( $_GET['id'] );
}

if ( isset($_POST['nome']) ) {
	// Editar registro existente
	if ( isset($_GET['id']) ) {
		$edit->nome = $_POST['nome'];
		$edit->email = $_POST['email'];
		$edit->codigo = $_POST['codigo'];
		$edit->setEntidades($_POST['entidades']);
		$edit->saveEntidades();

		$edit->save();
	}
	// Cadastrar novo registro
	else {
		$edit->nome = $_POST['nome'];
		$edit->email = $_POST['email'];
		$edit->codigo = $_POST['codigo'];
		$edit->setEntidades($_POST['entidades']);
		if (!$edit->recordExists(array(array(
				'field' => 'nome',
				'value' => $_POST['nome']
		)))) {
			$edit->save();
			$edit->saveEntidades();
		}
		else
			$error[] = 'Uma corretora com o mesmo nome já está cadastrada.';
	}

	go_to( get_url('painel/corretora?id='.$edit->getID()) );
}

get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>


<div class="container">
	<h1>Corretora</h1>

	<div class="row">
		<div class="col-md-4">
			<form action="" method="POST">
				<legend><?php echo ($edit->getID()) ? 'Editar: '.$edit->nome : 'Cadastrar'; ?></legend>
				
				<?php if (isset($error)) : ?>
					<div class="alert alert-warning">
					<?php foreach($error as $item) : ?>
						<?php echo $item; ?><br>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input name="nome" type="text" class="form-control" id="inputNome" placeholder="Nome" <?php echo ($edit->getID()) ? 'value="'.$edit->nome.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputUsuario">Entidades</label>
					<?php

						$entidade_list = new Entidade;
						$entidade_list->loadList();

						if ( $entidade_list->getList() ) foreach ( $entidade_list->getList() as $entidade ) : ?>
							<div class="checkbox">
								<label>
									<input name="entidades[]" type="checkbox" value="<?php echo $entidade->getID(); ?>" <?php if($edit->temEntidade($entidade->getID())) echo 'checked'; ?>>
									<?php echo $entidade->nome; ?>
								</label>
							</div>
						<?php endforeach;
					?>
				</div>
				<div class="form-group">
					<label for="inputEmail">E-mail</label>
					<input name="email" type="email" class="form-control" id="inputEmail" placeholder="E-mail" <?php echo ($edit->getID()) ? 'value="'.$edit->email.'"' : ''; ?>>
				</div>
				<div class="form-group">
					<label for="inputCodigo">Código</label>
					<input name="codigo" type="text" class="form-control" id="inputCodigo" placeholder="Código" <?php echo ($edit->getID()) ? 'value="'.$edit->codigo.'"' : ''; ?>>
				</div>
				<button type="submit" class="btn btn-primary"><?php echo ($edit->getID()) ? 'Salvar' : 'Cadastrar'; ?></button>
				<?php if ($edit->getID()) : ?>
					<a href="<?php echo get_config('url'); ?>painel/corretora" class="btn">Nova corretora</a>
				<?php endif; ?>
			</form>
		</div>
		<div class="col-md-8">
			<?php if ( $_c->getList() ) : ?>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Cod</th>
							<th>Corretora</th>
							<!--<th>E-mail</th>-->
							<th>Entidades</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($_c->getList() as $item) : ?>
							<tr>
								<td><?php echo $item->codigo; ?></td>
								<td><?php echo $item->nome; ?></td>
								<!--<td><?php echo $item->email; ?></td>-->
								<td><?php echo $item->get_the_entidades(); ?></td>
								<td class="text-right">
									<a href="<?php echo get_config('url'); ?>painel/corretora?id=<?php echo $item->getID(); ?>" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<div class="text-center">
					<?php echo $_c->getPaginationHtml(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	
</div>


<?php get_footer(); ?>