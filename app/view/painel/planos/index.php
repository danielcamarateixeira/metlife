<?php

	include VIEW . '/painel/_auth.php';

	$list = new Plano;
	$list->loadList();

	$planos = array();
	$planos_valores = array();

	foreach ( $list->getList() as $plano_obj ) {
		$pv = new PlanoValor;
		$pv->loadList(array(array(
			'field' => 'planoid',
			'value' => $plano_obj->getID()
		)));
		$planos[] = $plano_obj;
		foreach ( $pv->getList() as $valor )
			$planos_valores[] = $valor;
	}
	// debug($planos_valores);

	$base_url = get_config('url') . 'painel/planos';

	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>


<div class="container">

	<h1>Planos</h1>

	<hr>

	<table class="table table-striped">
		<thead>
			<tr>
				<th></th>
				<?php foreach($planos as $plano) : ?>
					<th><?php echo $plano->nome; ?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach($planos_valores as $pv) : ?>
				<tr>
					<td><?php echo $pv->entidade->nome; ?></td>
					<td><?php echo $pv->valor; ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>


</div>