<?php

	if ( !$_GET['id'] ) die('Arquivo não informado');

	$fileurl = base64_decode($_GET['id']);
	$filepath = get_upload_abs_from_rel($fileurl);

	if ( file_exists($filepath) ) {
		$filename = basename($filepath);
        $filesize = filesize($filepath);

        // Output headers.
        header("Cache-Control: private");
        header("Content-Type: application/stream");
        header("Content-Length: ".$filesize);
        header("Content-Disposition: attachment; filename=".$filename);

        // Output file.
        readfile ($filepath);                   
        exit;
    }
    else
        die('Arquivo não existe');

?>