<?php

	include VIEW . '/painel/_auth.php';

	$list = new Proposta;
	$list->loadList();

	get_header();

?>

<?php include VIEW . '/painel/_navbar.php'; ?>

<div class="container">

	<h1>Documentos</h1>

	<?php if ( $list->getList() ) : ?>

		<table class="table table-stripped table-responsive">

			<thead>
				<tr>
					<td>#</td>
					<td>Nome</td>
					<td>Documentos encontrados</td>
					<td>&nbsp;</td>
				</tr>
			</thead>

			<tbody>
				<?php foreach ( $list->getList() as $proposta ) : if ( $all_docs = $proposta->getTodosDocumentos() ) : ?>

					<tr>
						<td><?php echo $proposta->getID(); ?></td>
						<td><?php echo $proposta->nome; ?></td>
						<td><?php echo count($all_docs); ?></td>
						<td class="text-right"><a href="<?php echo get_config('url'); ?>painel/documentos/proposta?id=<?php echo $proposta->getID(); ?>" class="btn btn-primary">Ver documentos <i class="fa fa-files-o" aria-hidden="true"></i></a></td>
					</tr>

				<?php endif; endforeach; ?>
			</tbody>

		</table>

	<?php else : ?>

		<div class="alert alert-warning">
			Ainda não há nenhum documento registrado no sistema.
		</div>

	<?php endif; ?>


</div>


<?php get_footer(); ?>