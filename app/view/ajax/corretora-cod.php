<?php



$cod = $_GET['cod'];
$corr = new Corretora(array(
	array(
		'field'	=> 'codigo',
		'value' => $cod
	)
));

$json = array();

if ( !$corr->getID() ) {
	echo json_encode(array(
		'error' => 'Código de corretora não encontrado!'
	));
	exit;
}

echo json_encode(array(
	'corretoraid' => $corr->getID(),
	'nome' => $corr->nome,
	'codigo' => $corr->codigo
));
exit;

?>