<?php

$today = new DateTime('today');
$vigencia = new DateTime('+45 days');
$interval = new DateInterval('P1D');
$daterange = new DatePeriod($today, $interval ,$vigencia);

$dates = array();

foreach($daterange as $date){
    if ($date->format("d") == '01')
    	$dates[] = array(
    		'value' => $date->format("m/Y"),
    		'display' => $date->format("d/m/Y")
    	);
}

echo json_encode($dates);

?>