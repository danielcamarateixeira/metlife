<?php

/**
 * Verifica se o usuário está logado e possui as permissões necessárias para acessar essa página.
 * Se estiver logado: Carrega os dados do usuário logado.
 * Senão: Redireciona para a página de login
 */

$_auth = new Auth;

if ( $_auth->isLogged() ) {
	$currentuser = $_auth->getUsuario();
	if ($currentuser->firstlogin == 0 && $URLSTR != 'perfil/editar')
		go_to( get_url('perfil/editar') );
}	
else {
	$url_now = $_SERVER['REQUEST_URI'];
	$url = get_url('login') . '?redirect=' . urlencode($url_now);
	go_to($url);
}

?>