<!DOCTYPE html>
<html lang="pt-BR">
	<head>
			<meta charset="utf-8">
			<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
			<title><?php echo page_title(); ?></title>

			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="profile" href="http://gmpg.org/xfn/11">
			<meta name="robots" content="noindex,nofollow">

			<link rel="shortcut icon" href="<?php the_image('favicon.png'); ?>">
			<link rel="apple-touch-icon" type="image/png" href="<?php the_image('apple-touch-icon-57.jpg'); ?>"> 
			<link rel="apple-touch-icon" type="image/png" sizes="72x72" href="<?php the_image('apple-touch-icon-72.jpg'); ?>">

			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
			<?php Asset::loadCss('header'); ?>
			<?php Asset::loadJs('header'); ?>

			<!--[if lt IE 9]>
		    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		    <![endif]-->
	</head>
	<body>

	<div id="wrap">