<div class="row debug-info" style="font-family: monospace;">
	<strong>DATABASE</strong><br>
	Database: <?php echo get_config('db.name'); ?><br>
	Prefix: <?php echo get_config('db.prefix'); ?><br>
	<strong>SESSION</strong><br>
	<pre><?php print_r($_SESSION); ?></pre>
</div>

<style type="text/css">
	.debug-info pre {
		color: #fff;
		background: none;
		border: none;
		padding: 0;
	}
</style>