<?php

	$pendencia = new Proposta;
	$pendencia->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'corretor'
		),
		array(
			'field' => 'corretoraid',
			'value' => $currentuser->corretoraid
		)
	));

	$vidas = new Proposta;
	$vidas->loadList(array(
		array(
			'field' => 'corretoraid',
			'value' => $currentuser->corretoraid
		)
	));
	$vidas_total = 0;
	if ( $vidas->getList() ) foreach($vidas->getList() as $item)
		$vidas_total += (($item->{'quantidade-dependentes'})+1);


	$finalizada = new Proposta;
	$finalizada->loadList(array(
		'relation' => 'AND',
		array(
			'field' => 'status',
			'value' => 'finalizado'
		),
		array(
			'field' => 'corretoraid',
			'value' => $currentuser->corretoraid
		)
	));

?>

<div class="relatorios-backoffice row flex-row dashboard">
	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<h4 class="title">Propostas com pendência (em aberto)</h4>
			<p class="data"><?php echo count($pendencia->getList()); ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<h4 class="title">N&ordm; total de vidas</h4>
			<p class="data"><?php echo $vidas_total; ?></p>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4">
		<div class="widget item text-center">
			<h4 class="title">Propostas implantadas</h4>
			<p class="data"><?php echo count($finalizada->getList()); ?></p>
		</div>
	</div>
</div>