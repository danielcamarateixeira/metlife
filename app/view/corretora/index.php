<?php

	include VIEW . '/corretora/_auth.php';

	get_header();

?>

<main class="main dashboard">

	<div class="container">

		<?php include VIEW . '/corretora/section-relatorios.php'; ?>

		<?php include VIEW . '/corretora/section-recentes.php'; ?>
		
	</div>

</main>


<?php get_footer(); ?>