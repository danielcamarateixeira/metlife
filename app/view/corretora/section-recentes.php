<?php

	/*
	$params = ( $currentuser->temPermissao(array('administradora','desenvolvedor')) ) ?
		array(
			'field' => 'protocolo',
			'value' => '',
			'compare' => '!='
		) :
		array(
			'field' => 'corretoraid',
			'value' => $currentuser->corretoraid
		);
	*/
	$params = array(
		'field' => 'corretoraid',
		'value' => $currentuser->corretoraid
	);


	$list = new Proposta;
	$list->loadList(array(
		'orderby' => 'datecreate',
		'order' => 'DESC',
		$params
	), 'p');

?>

<section class="propostas-recentes widget">

	<table class="table table-stripped table-condensed table-responsive">
		<thead>
			<tr>
				<th>Protocolo</th>
				<th class="text-center">N&ordm; de vidas</th>
				<th>Corretor</th>
				<th>Entidade</th>
				<th class="text-center">Valor total</th>
				<th>Vigência</th>
				<th>Status</th>
				<th class="text-center">Proposta</th>
			</tr>
		</thead>

		<tbody>
			<?php if ($list->getList()) : foreach($list->getList() as $proposta) : ?>
				<tr>
					<td><?php echo $proposta->protocolo; ?></td>
					<td class="text-center"><?php echo $proposta->{'quantidade-dependentes'}+1; ?></td>
					<td><?php echo $proposta->usuario->nome; ?></td>
					<td><?php echo $proposta->entidade->nome; ?></td>
					<td class="text-center">R$ <?php echo $proposta->{'valor-proposta'}; ?></td>
					<td><?php echo date('d/m/y', strtotime($proposta->{'vigencia-inicio'})); ?></td>
					<td><?php echo $proposta->getStatus(); ?></td>
					<td class="text-center">
						<a href="<?php echo get_url('corretora/visualizar'); ?>?id=<?php echo $proposta->getHash(); ?>" class="btn btn-link btn-sm">Visualizar</a>
					</td>
				</tr>
			<?php endforeach; else : ?>
				<tr>
					<td colspan="7">
						<div class="alert alert-info">Nenhuma proposta encontrada</div>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>

	<div class="text-center">
		<?php echo $list->getPaginationHtml(); ?>
	</div>
	
</section>