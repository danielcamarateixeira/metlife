<?php

	include VIEW . '/corretora/_auth.php';

	$proposta = new Proposta;
	$proposta->loadByHash($_GET['id']);

	if ( !$proposta->getID() ) {
		header("Location: ".get_config('url').'documentos');
		exit;
	}

	$base_url = get_config('url') . 'painel/proposta';

	$doc = $proposta->getDocumento();
	$doc_filiacao = $proposta->getDocumentoFiliacao();

	get_header();

?>

<main class="main dashboard">

	<div class="container widget">

		<h1>Proposta</h1>
		<h4>Proposta #<?php echo $proposta->protocolo; ?> - <?php echo $proposta->nome; ?></h4>

		<hr>

		<div class="row">

			<div class="col-md-8">
				<div id="proposta-documento"><?php echo $doc->getHtml(); ?></div>
				<?php if ( $doc_filiacao ) : ?>
					<hr>
					<div id="ficha-filiacao"><?php echo $doc_filiacao->getHtml(); ?></div>
				<?php endif; ?>
			</div>

			<div class='col-md-4'>
				<?php echo $proposta->getHistoryHtml(); ?>
				<hr>
				<a href="<?php echo get_url('proposta/documentos'); ?>?id=<?php echo $proposta->getID(); ?>" class="btn btn-block btn-primary">Revisar documentos</a>
			</div>
			
		</div>

		

	</div>

</main>

<?php get_footer(); ?>