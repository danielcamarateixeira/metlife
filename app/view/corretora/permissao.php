<?php

	include VIEW . '/corretora/_auth.php';

	get_header();

?>

<hr>

<div class="container">

	<div class="alert alert-danger">
		<p><strong><i class="fa fa-lock"></i> Área restrita!</strong></p>
		<p>Você não possui a permissão necessária para acessar esse conteúdo.</p>
		<p>Por favor, faça o <a href="<?php echo get_url('sair'); ?>">logout</a> e entre com uma conta com permissão de Corretora ou entre em contato com o setor responsável.</p>
	</div>

</div>

<?php get_footer(); ?>