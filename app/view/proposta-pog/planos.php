<?php

include VIEW . '/_auth.php';

$proposta = new Proposta;
$proposta->loadByID( $_GET['id'] );
$proposta->setEtapa(3);

// Redireciona caso não encontre a proposta
if ( !$proposta->getID() ) {
	header('Location: '.get_config('url').'proposta/inexistente');
	exit;
}

// Carrega o formulário do passo 3
$form = $proposta->getForm( $proposta->getProperties() );

$planos = new Plano;
$planos->loadList();

// Aqui trata do formulário que foi enviado
if ( isset($_GET['salvar']) && isset($_POST['id']) ) {

	unset($_POST['id']);
	// Coloca todos os valores dos campos do formulário na classe
	foreach ($_POST as $field => $value) {
		$field_value = is_array($value) ? implode(';', $value) : trim($value);
		
		if ($field == 'cpf' || $field == 'rg' || $field == 'cep')
			$field_value = preg_replace("/[^0-9]/", "", $field_value);

		if ($field_value != '')
			$proposta->$field = mb_strtoupper($field_value, 'UTF-8');
	}

	$assinatura = new Upload( $_FILES['assinatura_digital'] );
	if ( $assinatura->uploaded ) {
		// $assinatura->file_new_name_body = 'assinatura_adesao_' . $proposta->getID();
		$assinatura->file_new_name_body = md5(uniqid(rand(), true));
		$assinatura->file_overwrite = true;
		$assinatura->Process( UPLOADS . '/assinaturas/' . date('Y-m') );
		if ( $assinatura->processed )
			$proposta->assinatura_adesao_digital = UPLOADS_REL . '/assinaturas/' . date('Y-m') . '/' . $assinatura->file_dst_name;
	}

	$proposta->serializeCampos($form);
	$proposta->save();

	header('Location: '.get_config('url').'proposta/termos?id='.$proposta->getID());
	exit;

}

get_header(); ?>


<?php include VIEW . '/proposta/_header.php'; ?>

<div class="navbar-etapa <?php echo $proposta->getEtapaStatus(1); ?>"><div class="container"><?php echo $proposta->getEtapaTitle(1); ?> <i class="fa fa-caret-right" aria-hidden="true"></i></div></div>
<div class="navbar-etapa <?php echo $proposta->getEtapaStatus(2); ?>"><div class="container"><?php echo $proposta->getEtapaTitle(2); ?> <i class="fa fa-caret-right" aria-hidden="true"></i></div></div>
<div class="navbar-etapa <?php echo $proposta->getEtapaStatus(3); ?>"><div class="container"><?php echo $proposta->getEtapaTitle(3); ?> <i class="fa fa-caret-right" aria-hidden="true"></i></div></div>

<div class="etapa-body">

	<div class="container">

		<form action="?id=<?php echo $proposta->getID(); ?>&salvar" method="post" enctype="multipart/form-data">
			
			<input type="hidden" name="id" value="<?php echo $proposta->getID(); ?>">
			<input type="hidden" id="vigencia" value="<?php echo $proposta->vigenciaf; ?>">

			<?php //echo $form->getHtml(); ?>
			<?php foreach ( $planos->getList() as $plano ) : $plano->setEntidade( $currentuser->entidadeid ); ?>
				<div class="radio btn btn-primary">
					<label>
						<input type="radio" name="plano" id="inputPlano" class="plano" value="<?php echo $plano->getID(); ?>" data-valor="<?php echo $plano->getValor()->valor; ?>" data-dependentes="<?php echo $proposta->{'quantidade-dependentes'}; ?>">
						<?php echo $plano->nome; ?>
					</label>
				</div>
			<?php endforeach; ?>

			<table class="table table-stripped">
				<thead>
					<tr>
						<th></th>
						<th>Idade</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label>Titular</label></td>
						<td><input type="text" name="titular-idade" value="<?php echo $proposta->getIdade( $proposta->nascimento ); ?>" readonly class="form-control"></td>
						<td>
							<div class="input-group">
  								<span class="input-group-addon">R$</span>
								<input type="text" name="titular-valor" id="titular-valor" value="..." readonly class="form-control">
							</div>
						</td>
					</tr>

					<?php if ( $proposta->{'quantidade-dependentes'} > 0 ) for ( $i=1; $i<=$proposta->{'quantidade-dependentes'}; $i++ ) : ?>
						<tr>
							<td><label>Dependente <?php echo $i; ?></label></td>
							<td><input type="text" name="dependente-<?php echo $i; ?>-idade" id="dependente-<?php echo $i; ?>-idade" value="<?php echo $proposta->getIdade( $proposta->{'dep1-nascimento'} ); ?>" readonly class="form-control"></td>
							<td>
								<div class="input-group">
  									<span class="input-group-addon">R$</span>
  									<input type="text" name="dependente-<?php echo $i; ?>-valor" id="dependente-<?php echo $i; ?>-valor" value="..." readonly class="form-control">
  								</div>
  							</td>
						</tr>
					<?php endfor; ?>

				</tbody>
				<tfoot>
					<tr>
						<td>Total</td>
						<td></td>
						<td>
							<div class="input-group">
  								<span class="input-group-addon">R$</span>
								<input type="text" name="valor-total" id="valor-total" value="..." readonly class="form-control">
							</div>
						</td>
					</tr>
				</tfoot>
			</table>

			<div class="carencias alert alert-default">
				<h4>CARÊNCIAS</h4>
				<p>24 (vinte e quatro) horas para procedimentos de Urgência e Emergência.</p>
				<p>30 (trinta) dias para procedimentos das demais especialidades.</p>
			</div>

			<?php echo $form->getHtml(); ?>

			<div class="declaracao-reajuste alert alert-default">
				<h4>DECLARAÇÃO DE CIÊNCIA DE REAJUSTE</h4>
				<p>Declaro que, por ocasião da contratação, recebi todas as informações pertinentes às diferenças entre o plano ora contratado (Plano Coletivo por Adesão) e os Planos Individuais/Familiares, sobretudo no tocante à forma e à periodicidade do reajuste, cuja data base será verificada a partir da contratação entre minha entidade e a administradora de benefícios (e não no momento da assinatura desta proposta). Declaro, ainda, que me foi explicada a abrangência da clausula 18 deste contrato, cujo texto está em total conformidade com a Resolução Normativa n° 195 da ANS (Agência Nacional de Saúde), que em seu Art. 19, §1, dispõe que: "Para fins do disposto no caput, considera-se reajuste qualquer variação positiva na contraprestação pecuniária, inclusive aquela decorrente de revisão ou reequilíbrio econômico-atuarial do contrato". Por fim, declaro que me foi informado que os índices de reajuste de meu contrato não seguirão os parâmetros aplicáveis aos Planos Individuais/Familiares, podendo variar em percentuais maiores tendo em vista uma eventual alta sinistralidade que venha a ser apurada a cada período de 12 meses.</p>
			</div>

			<hr>

			<div>
				<h4>AUTORIZAÇÃO PARA ENVIO DE INFORMAÇÕES</h4>

				<div class="row row-flex align-center">
					<div class="col-sm-6">
						<div class="radio">
							<label>
								<input type="radio" name="autorizo_envio" id="input" value="Sim" checked="checked">
								Autorizo o envio de informações* sobre o benefício contratado.
							</label>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="radio">
							<label>
								<input type="radio" name="autorizo_envio" id="input" value="Não" checked="checked">
								Não autorizo o envio de informações* sobre o benefício contratado.
							</label>
						</div>
					</div>
				</div>
				<small>*informações relevantes, tais como reajustes, ofertas de novos serviços e benefícios.</small>
			</div>

			<hr>

			<div class="alert alert-default">
				Declaro que as informações aqui prestadas e os documentos apresentados são absolutamente verdadeiros e completos, e me responsabilizo civil e criminalmente por eles.
			</div>

			<div class="alert alert-assinatura alert-info digital">
				Colete a assinatura via aplicativo ou fotografia legível com fundo branco.
			</div>
			<div class="alert alert-assinatura alert-warning manuscrita">
				Será preciso entregar a assinatura manuscrita à Administradora de Benefícios.
			</div>
			<div class="alert alert-assinatura alert-danger depois">
				Atenção, você está se comprometendo a anexar a assinatura em até 24 horas após esta solicitação.
			</div>

			<div class="form-group assinatura-select-container"><label for="slAssinatura_adesao">Assinatura</label><select name="assinatura_carencia" id="slAssinatura_carencia" class="form-control assinatura_sl input-assinatura_carencia"><option>DEPOIS</option><option>DIGITAL</option><option selected="">MANUSCRITA</option></select></div>

			<div class="form-field-wrap row clearfix"><div class="form-group assinatura-digital-container" style="display: block;"><label for="inputAssinatura_digital">Assinatura digital</label><input type="file" name="assinatura_digital" id="inputAssinatura_digital" class="form-control  input-assinatura_digital" required="required"></div></div>

			<div class="assinaturas row row-flex align-center">
				<div class="col-sm-6">
					<a href="#" class="btn btn-default btn-lg btn-assinatura" id="btn-assinatura-digital">
						<div class="col-xs-3">
							<img src="<?php the_image('assinatura-digital.png'); ?>" alt="Enviar assinatura digital">
						</div>
						<div class="col-xs-9 text">
							<strong>Enviar assinatura</strong>
							<h3>DIGITAL</h3>
						</div>
					</a>
				</div>
				<div class="col-sm-6">
					<a href="#" class="btn btn-default btn-lg btn-assinatura" id="btn-assinatura-manuscrita">
						<div class="col-xs-3">
							<img src="<?php the_image('assinatura-manuscrita.png'); ?>" alt="Informar assinatura manuscrita">
						</div>
						<div class="col-xs-9 text">
							<strong>Informar assinatura</strong>
							<h3>MANUSCRITA</h3>
						</div>
					</a>
				</div>
				<div class="col-sm-6">
					<a href="#" class="btn btn-default btn-lg btn-assinatura" id="btn-assinatura-depois">
						<div class="col-xs-3">
							<img src="<?php the_image('assinatura-depois.png'); ?>" alt="Enviar assinatura depois">
						</div>
						<div class="col-xs-9 text">
							<strong>Enviar assinatura</strong>
							<h3>DEPOIS</h3>
						</div>
					</a>
				</div>
			</div>

			<div class="form-action row">
				<button type="submit" class="btn btn-lg btn-block btn-success">Prosseguir</button>
			</div>

		</form>
		
	</div>

</div>

<div class="navbar-etapa <?php echo $proposta->getEtapaStatus(4); ?>"><div class="container"><?php echo $proposta->getEtapaTitle(4); ?> <i class="fa fa-caret-right" aria-hidden="true"></i></div></div>

<?php get_footer(); ?>