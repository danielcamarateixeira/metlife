	
	</div><!-- end #wrap -->

	<footer id="bot">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-4">
					<p>Desenvolvido por <a href="http://gosocial.cc/" target="_blank">gosocial.cc.</a></p>
				</div>
				<div class="col-sm-8 text-right">
					<p class="muted"><?php echo date('Y'); ?> &copy; <?php config('name'); ?>. Todos os direitos reservados.</p>
				</div>
			</div>

			<?php if ( MODE == 'sandbox' ) include '_appinfo.php'; ?>
		</div>
	</footer>

	<?php Asset::loadJs('footer'); ?>
	<?php Asset::loadCss('footer'); ?>

	</body>
</html>