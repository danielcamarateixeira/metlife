<?php 
$page_title = '404';

get_header('blank'); ?>

<div class="jumbotron">
	<div class="container">
		<h1>404</h1>
		<p>Nada encontrado.</p>
		<a href="<?php echo URL; ?>" class="btn btn-primary btn-lg">Voltar para a página Inicial</a>
	</div>
</div>

<?php get_footer('blank'); ?>