<?php

if ( isset($_GET['auth']) && isset($_POST['user']) ) {
	$auth = new Auth;
	$lembrar = (isset($_POST['lembrar'])) ? $_POST['lembrar'] : false;
	if ( $auth->login( $_POST['user'], $_POST['pass'], $lembrar ) ) {
		if ( isset($_GET['redirect']) )
			go_to( $_GET['redirect'] );
		else
			go_to( get_url('') );
	}
	else {
		$error = array('danger', 'Login ou senha inválidos. Tente novamente.');
	}
}

get_header('blank'); ?>

		<div class="login-container text-center">
			<div class="login-bubble">
				<img src="<?php the_image('lock.png'); ?>" class="lock">
				<h1>
					<span class="title-sm">Área do</span>
					Corretor
				</h1>
			</div>

			<div class="container">
				<div class="logos row">
					<div class="col-xs-6 text-center">
						<img src="<?php the_image('logo-metlife.png?v=3'); ?>" alt="Metlife" class="img-responsive">
					</div>
					<div class="col-xs-6 text-center">
						<img src="<?php the_image('logo-nunesegr.png?v=3'); ?>" alt="<?php config('name'); ?>" class="img-responsive">
					</div>
				</div>

				<?php if (isset($error)) : ?>
					<div class="alert alert-<?php echo $error[0]; ?>"><?php echo $error[1]; ?></div>
				<?php endif; ?>

				<?php /*
				<div class="row">
					<div class="col-xs-6 text-center">
						<img src="assets/images/metlife.png" class="img-responsive">
					</div>
					<div class="col-xs-6 text-center">
						<img src="assets/images/nunesegrossi.png" class="img-responsive">
					</div>
				</div>
				*/ ?>
				<form action="?auth<?php if(isset($_GET['redirect'])) echo '&redirect='.urlencode($_GET['redirect']); ?>" method="post">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<input type="text" name="user" class="form-control input-lg" placeholder="Nome de usuário">
						</div>
						<div class="col-sm-12 col-md-4">
							<input type="password" name="pass" class="form-control input-lg" placeholder="Senha">
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="lembrar" id="lembrar" value="1">
									Lembrar meu usuário e senha
								</label>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button type="submit" class="btn btn-default btn-lg btn-wide">Entrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>

<?php get_footer('blank'); ?>