<div class="navbar navbar-main">
	<div class="container-fluid">
		<div class="btn-group btn-group-justified">
			<a href="<?php echo get_config('url'); ?>proposta/nova" class="btn btn-success">Nova proposta</a>
			<a href="<?php echo get_config('url'); ?>proposta/ultimas" class="btn btn-primary">Últimas propostas</a>
			<a href="<?php echo get_config('url'); ?>faq" class="btn btn-primary">Boas práticas</a>
		</div>
	</div>
</div>