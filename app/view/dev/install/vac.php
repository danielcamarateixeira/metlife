<?php

	if (isset($_GET['ban'])) {
		$users = preg_split('/\r\n|[\r\n]/', $_POST['users']);
		$newuser = $_POST['newuser'];

		// debug($users);
		// debug($newuser);

		$nu = new Usuario(array(
			array(
				'field' => 'usuario',
				'value' => $newuser
			)
		));

		foreach($users as $user) {
			$u = new Usuario(array(
				array(
					'field' => 'usuario',
					'value' => $user
				)
			));
			$plist = new Proposta;
			$plist->loadList(array(
				array(
					'field' => 'usuarioid',
					'value' => $u->getID()
				)
			));
			if ($plist->getList()) foreach($plist->getList() as $proposta) {
				$proposta->usuarioid = $nu->getID();
				$proposta->save();
				debug(sprintf('> Proposta #%s propriedade alterada do %s (#%s) para %s (#%s)', $proposta->protocolo, $u->nome, $u->getID(), $nu->nome, $nu->getID()));
			}
		}
	}


	get_header('blank');

?>

<form action="?ban" method="post">
	<div class="row">
		<div class="col-md-6">
			<textarea name="users" class="form-control" rows="10" placeholder="Usuários que serão banidos (um nome de usuário por linha)"></textarea>
		</div>
		<div class="col-md-6">
			<input type="text" name="newuser" class="form-control" placeholder="Nome de usuário que as propostas serão transferidas">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-block btn-danger">Banir</button>
		</div>
	</div>
</form>

<?php get_footer('blank'); ?>