<?php

$plist = new Proposta;
$plist->loadList();

foreach ($plist->getList() as $proposta) {

	if ( strlen($proposta->vigencia) > 7 ) {
		$oldvigencia = $proposta->vigencia;
		$vigencia = substr($proposta->vigencia, strlen($proposta->vigencia)-7);

		$proposta->vigencia = $vigencia;
		$proposta->vigenciaf = '01/' . $vigencia;
		$proposta->{'vigencia-inicio'} = $proposta->vigenciaf;

		$proposta->serializeFields();
		$proposta->save();
		debug( 'FIXED! (' . $proposta->getID() . ') ' . $oldvigencia . ' => ' . $vigencia );
	}

}