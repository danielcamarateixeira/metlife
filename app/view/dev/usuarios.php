<?php

	include '_security.php';

	$id = (isset($_GET['id']) && !empty($_GET['id'])) ? (int) $_GET['id'] : false;
	$q = (isset($_GET['q']) && !empty($_GET['q'])) ? $_GET['q'] : false;

	$params = ($id || $q) ? array(
		'relation' => 'AND'
	) : false;

	if ($id)
		$params[] = array(
			'field' => 'usuarioid',
			'value' => $id
		);

	if ($q)
		$params[] = array(
			'relation' => 'OR',
			array(
				'field' => 'nome',
				'value' => '%'.$q.'%',
				'compare' => 'LIKE'
			)
		);

	debug($params);

	$u = new Usuario;
	$u->loadList($params);

?>

<pre>
	
	<?php if ($q) : ?>EXIBINDO RESULTADOS DE "<?php echo $q; ?>"<br>---<br><?php endif; ?>

	<?php

		foreach ($u->getList as $su) :

			echo $su->nome . "<br>";
			echo '---<br>';

		endforeach;

	?>


</pre>