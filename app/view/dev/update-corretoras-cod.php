<?php

if ( ($handle = fopen(dirname(__FILE__).'/corretoras_cod.csv', "r")) !== false ) {
	while ( ($data = fgetcsv($handle, 1000, ',')) !== false ) {
		$cod = $data[0];
		$nome = $data[1];
		$nome2 = $data[2];
		$cnpj = str_pad( preg_replace('/[^0-9]/', '', $data[3]), 14, '0', STR_PAD_LEFT);

		$c = new Corretora(array(
			array(
				'field' => 'cnpj',
				'value' => $cnpj
			)
		));

		if ( !$c->getID() ) {
			$c->cnpj = $cnpj;
			$c->nome = $nome;
		}

		$c->codigo = $cod;
		$c->save();

		debug(sprintf("%s // %s // %s // %s", $cod, $nome, $cnpj, $c->getID()));
	}
}