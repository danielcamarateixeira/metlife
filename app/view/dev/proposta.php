<?php

$proposta = new Proposta;
$id = $_GET['id'];
$proposta->loadByID($id);

if (!$proposta->getID()) die('Proposta não existe!');

get_header('blank');


echo '<table class="table table-stripped" width="100%" style="border-bottom:5px solid #CCC; margin-bottom:2em;">';
echo '<thead><tr><th>Campo</th><th>Valor</th></tr></thead><tbody>';
foreach ( $proposta->getProperties() as $field => $value ) :
	if ( is_string($value) || is_null($value) )
		echo "<tr><td style=\"width:400px\">{$field}</td><td width=\"100%\">{$value}</td></tr>";
	elseif ( is_object($value) ) {
		echo '<tr><td style="width:400px;">' . $field . '</td><td><table>';
		if ( $value->getProperties() ) {
			foreach ( $value->getProperties() as $f2 => $v2 ) {
				if ( is_string($v2) || is_null($v2) )
					echo "<tr><td style=\"width:400px\">{$f2}</td><td width=\"100%\">{$v2}</td></tr>";
				else {
					echo '<tr><td>' . $f2 . '</td><td>';
					debug($v2->getProperties());
					echo '</td></tr>';
				}
			}
		}
		echo '</table>';
	}
	else {
		echo '<tr><td>' . $field . '</td><td>';
		debug($value);
		echo '</td></tr>';
	}
endforeach;
echo '</tbody></table>';


get_footer();