<?php

$_auth = new Auth;

if ( ! $_auth->isLogged() )
	die('Você não está logado.');

$currentuser = $_auth->getUsuario();

if ( !$currentuser->temPermissao('desenvolvedor') )
	die('Sem permissão para acesar esse conteúdo.');