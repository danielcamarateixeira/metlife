<?php

include '_auth.php';
get_header(); ?>

<div class="home">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="authbox authbox-lg authbox-bordered">
					<div class="row">
						<div class="col-xs-4">
							<figure class="avatar">
								<img src="<?php echo $currentuser->getAvatar(); ?>" alt="<?php echo $currentuser->nome; ?>" class="img-responsive">
							</figure>
						</div>
						<div class="col-xs-6">
							<p class="verified <?php echo $currentuser->verifiedStatus(); ?>"><?php if ($currentuser->isVerified()) :
								?><i class="fa fa-check-circle"></i> VERIFICADO<?php else : 
								?><i class="fa fa-clock-o"></i> PENDENTE<?php endif; 
							?></p>
							<p class="nome"><?php echo $currentuser->nome; ?></p>
							<p class="userid"><span>ID</span><?php echo $currentuser->getID(); ?></p>
						</div>
					</div>
				</div>
				<div class="navbar navbar-index">
					<nav>
						<ul>
							<?php if( $currentuser->temPermissao(array('backoffice','administradora','desenvolvedor')) ) : ?>
								<li class="no-margin"><a href="<?php echo get_url('backoffice'); ?>" class="btn btn-success btn-lg btn-block">ACESSAR O BACKOFFICE</a></li>
							<?php endif; ?>
							<?php if( $currentuser->temPermissao(array('cadastro','administradora','desenvolvedor')) ) : ?>
								<li><a href="<?php echo get_url('cadastro'); ?>" class="btn btn-success btn-lg btn-block">ACESSAR O CADASTRO</a></li>
							<?php endif; ?>
							<?php if( $currentuser->temPermissao(array('administradora','desenvolvedor')) ) : ?>
								<li><a href="<?php echo get_url('painel'); ?>" class="btn btn-success btn-lg btn-block">ACESSAR O PAINEL</a></li>
							<?php endif; ?>
							<?php if( $currentuser->temPermissao(array('corretora','administradora','desenvolvedor')) ) : ?>
								<li class="no-margin"><a href="<?php echo get_url('corretora'); ?>" class="btn btn-success btn-lg btn-block">ACESSAR O PAINEL DA CORRETORA</a></li>
							<?php endif; ?>
							<?php if ( $currentuser->temPermissao(array('vendedor','corretor','administradora','desenvolvedor')) ) : ?>
								<li><a href="<?php echo get_url('proposta/nova'); ?>" class="btn btn-success btn-lg btn-block">NOVA PROPOSTA</a></li>
								<li><a href="<?php echo get_url('proposta/historico'); ?>" class="btn btn-primary btn-light btn-block">HISTÓRICO</a></li>
								<li><a href="<?php echo get_url('faq'); ?>" class="btn btn-primary btn-light btn-block">FAQ & BOAS PRÁTICAS</a></li>
							<?php endif; ?>
							<li><a href="<?php echo get_url('perfil'); ?>" class="btn btn-primary btn-light btn-block">PERFIL</a></li>
							<li><a href="<?php echo get_url('sair'); ?>" class="btn btn-primary btn-light btn-block">SAIR</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>