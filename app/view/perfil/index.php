<?php

	include VIEW . '/_auth.php';

	$propostas = new Proposta;
	$propostas->loadList(array(
		array(
			'field' => 'usuarioid',
			'value' => $currentuser->getID()
		)
	));
	$total = count($propostas->getList());

	get_header();

?>

<?php include VIEW . '/perfil/_header.php'; ?>

<div style="height: 30px;"></div>

<div class="container">
	
	<div class="row flex-row">
		
		<div class="col-sm-2">
			<figure class="avatar thumbnail">
				<img src="<?php echo $currentuser->getAvatar(); ?>" class="img-responsive">
			</figure>
			<a href="<?php echo get_url('perfil/editar'); ?>" class="btn btn-block btn-primary"><i class="fa fa-pencil"></i> Editar</a>
			<a href="<?php echo get_url('sair'); ?>" class="btn btn-block btn-danger"><i class="fa fa-sign-out"></i> Sair</a>
		</div>

		<div class="col-sm-10">
			<h3><?php echo $currentuser->nome; ?></h3>
			<p class="title-sm"><i class="fa fa-user-o" aria-hidden="true"></i> <?php echo $currentuser->usuario; ?></p>
			<p class="email"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $currentuser->email; ?></p>
			<p class="telefone"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $currentuser->telefone; ?></p>
			<p class="whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $currentuser->celular; ?></p>
			<p class="cargo"><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $currentuser->getCargo(); ?></p>
		</div>

	</div>

</div>

<?php get_footer(); ?>