<?php

	include VIEW . '/_auth.php';

	// Verifica se é o primeiro acesso e registra isso
	if ( $currentuser->firstlogin == 0 ) {
		$error[] = array('Bem vindo '.$currentuser->nome.', atualize seu cadastro.', 'success');
		$currentuser->firstlogin = date('Y-m-d H:i:s');
		$currentuser->save();
	}

	// Salva o perfil
	if ( isset($_GET['salvar']) && count($_POST)>1 ) {

		$currentuser->nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_SPECIAL_CHARS);
		$currentuser->cpf = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_SPECIAL_CHARS);
		$currentuser->email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
		$currentuser->telefone = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_SPECIAL_CHARS);
		$currentuser->celular = filter_input(INPUT_POST, 'celular', FILTER_SANITIZE_SPECIAL_CHARS);

		if ( $_POST['senha'] != '' && $_POST['senhaconfirm'] != '' ) {
			if ( $_POST['senha'] != $_POST['senhaconfirm'] )
				$error[] = array('Senhas não conferem', 'danger');
			else
				$currentuser->senha = md5($_POST['senha']);
		}

		$file = new Upload( $_FILES['avatar'] );
		if ( $file->uploaded ) {
			$file->file_new_name_body = md5($currentuser->getID());
			$file->file_overwrite = true;
			$file->Process( UPLOADS . '/avatar' );
			if ( $file->processed )
				$currentuser->avatar = UPLOADS_REL . '/avatar/' . $file->file_dst_name;
		}

		$currentuser->save();
		go_to( get_url('perfil/editar') );

	}

	get_header();

?>

<?php include VIEW . '/perfil/_header.php'; ?>

<div style="height: 30px;"></div>

<div class="container">
	
	<div class="row flex-row">
		
		<div class="col-sm-2">
			<figure class="avatar thumbnail">
				<img src="<?php echo $currentuser->getAvatar(); ?>" class="img-responsive">
			</figure>
			<a href="<?php echo get_url('perfil'); ?>" class="btn btn-block btn-primary"><i class="fa fa-user-o"></i> Perfil</a>
			<a href="<?php echo get_url('sair'); ?>" class="btn btn-block btn-danger"><i class="fa fa-sign-out"></i> Sair</a>
		</div>

		<div class="col-sm-10">
			
			<form action="?salvar" method="post" enctype="multipart/form-data">

				<?php if (isset($error)) foreach($error as $erro) : ?>
					<div class="alert alert-<?php echo $erro[1]; ?>"><?php echo $erro[0]; ?></div>
				<?php endforeach; ?>
				
				<div class="form-group">
					<label for="nome">Nome</label>
					<input type="text" name="nome" id="nome" class="form-control" value="<?php echo $currentuser->nome; ?>">
				</div>

				<div class="form-group">
					<label for="cpf">CPF</label>
					<input type="text" name="cpf" id="cpf" class="form-control input-cpf" value="<?php echo $currentuser->cpf; ?>">
				</div>

				<div class="form-group">
					<label for="nome">E-mail</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
						<input type="email" name="email" id="email" class="form-control" value="<?php echo $currentuser->email; ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="telefone">Telefone</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-phone"></i></div>
						<input type="text" name="telefone" id="telefone" class="form-control" data-mask="(##) ####-####" value="<?php echo $currentuser->telefone; ?>">
					</div>
				</div>

				<div class="form-group">
					<label for="celular">Whatsapp</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-whatsapp"></i></div>
						<input type="text" name="celular" id="celular" class="form-control celular" value="<?php echo $currentuser->celular; ?>">
					</div>
				</div>

				<hr>

				<div class="form-group">
					<label for="senha">Nova senha</label>
					<input type="text" name="senha" id="senha" class="form-control" placeholder="Deixe em branco se não deseja alterá-la">
				</div>

				<div class="form-group">
					<label for="senhaconfirm">Confirmar nova senha</label>
					<input type="text" name="senhaconfirm" id="senhaconfirm" class="form-control">
				</div>

				<hr>

				<div class="form-group">
					<label for="avatar">Foto do perfil (avatar)</label>
					<input type="file" name="avatar" id="avatar" class="form-control">
				</div>

				<button type="submit" class="btn btn-primary btn-block">Salvar</button>

			</form>

		</div>

	</div>

</div>

<?php get_footer(); ?>