<?php

// Usuários
$userlist = array(
	0 => array(
		'nome' => 'GoSocial',
		'usuario' => 'gosocialcc',
		'senha' => 'nunes@2018',
		'permissao' => 'desenvolvedor'
	),
	1 => array(
		'nome' => 'Wallace',
		'usuario' => 'wallaceadm',
		'senha' => 'nunes@2018',
		'permissao' => 'administradora'
	),
	2 => array(
		'nome' => 'Lucas',
		'usuario' => 'lucasadm',
		'senha' => 'nunes@2018',
		'permissao' => 'administradora'
	),
	3 => array(
		'nome' => 'Corretor',
		'usuario' => 'corretor',
		'senha' => 'corretor@2018',
		'permissao' => 'corretor'
	)
);

$user = new Usuario;
$user->truncate();
debug('Tabela de usuários resetada');
foreach ( $userlist as $item ) {
	$user = new Usuario;
	$user->loadSingle(array(
		array(
			'field' => 'usuario',
			'value' => $item['usuario']
		)
	));
	if ( !$user->getID() ) {
		$user->nome = $item['nome'];
		$user->usuario = $item['usuario'];
		$user->senha = md5($item['senha']);
		$user->permissao = $item['permissao'];
		$user->save();
		debug('Criado o usuário ' . $item['usuario'] . ' com a senha '. $item['senha']);
	}
}