<?php
/**
 * PHP Bootstrap 3
 * -----------------------
 * View file.
 * Responsável por chamar as páginas que 
 * são exibidas na tela.
 * 
**/

define ( 'E404', VIEW . '/_404.php' );
define ( 'HEADER', VIEW . '/_header.php' );
define ( 'FOOTER', VIEW . '/_footer.php' );
define ( 'TEMPLATES', VIEW . '/_templates' );
define ( 'URL', get_config('url') );

function get_template_part ($template, $sufix = false) {

	foreach($GLOBALS as $name => $value) global $$name;

	$file_name = ($sufix) ? "{$template}-{$sufix}" : "{$template}";
	$file_path = VIEW . "/{$file_name}.php";
	$default_file_path = VIEW . "/{$template}.php";
	if ( file_exists($file_path) )
		include $file_path;
	else
		include $default_file_path;

}

function get_header ($sufix = false) {
	get_template_part('_header', $sufix);
}

function get_footer ($sufix = false) {
	get_template_part('_footer', $sufix);
}

/* Define quais arquivos css deverão ser carregados */
// jQuery
Asset::addJs('jquery.min.js');
// Bootstrap 3
Asset::addCss('bootstrap.min.css');
Asset::addJs('bootstrap.min.js','foot');
// Lib que torna a validação de forms do HTML5 cross-browser
// http://jaymorrow.github.io/validatr/
Asset::addJs('validatr.min.js','foot');
// Other Libs
Asset::addJs('jquery-ui.min.js');
Asset::addJs('wow.min.js','foot');
Asset::addJs('owl.carousel.min.js','foot');
Asset::addJs('parsley.min.js','foot');
// Asset::addJs('i18n/pt-br.js','foot');
Asset::addJs('moment-with-locales.min.js','foot');
Asset::addJs('jquery.formatter.min.js','foot');
Asset::addJs('jquery.printThis.js','foot');
Asset::addJs('jquery.maskMoney.min.js','foot');
Asset::addJs('jquery.mask.min.js','foot');
Asset::addJs('MonthPicker.min.js','foot');
Asset::addJs('app.js?v='.date('YmdHis'), 'foot');

Asset::addCss('jquery-ui.min.css');
Asset::addCss('animate.min.css');
Asset::addCss('font-awesome.min.css');
Asset::addCss('MonthPicker.min.css');
Asset::addCss('style.css?v='.date('YmdHis'));



// Pega a url atual e limpa valores vazios
$URL = get_url_parts(0);
$URL = ($URL) ? array_filter($URL) : $URL;

// debug($URL);

// Nenhuma página requisitada
if ( !$URL )
	include VIEW . "/index.php";
// Alguma página foi requisitada. Ex.: baseurl/example
else {
	// Define a string da URL
	$URLSTR = implode('/', $URL);
	
	// Verifica se existe um arquivo direto
	if ( file_exists( VIEW . '/' . $URLSTR . '.php' ) )
		include VIEW . '/' . $URLSTR . '.php';
	// Verifica se existe uma pasta e tenta carregar o index.php
	elseif ( file_exists( VIEW . '/' . $URLSTR . '/index.php' ) )
		include VIEW . '/' . $URLSTR . '/index.php';
	// Nenhuma página foi encontrada
	else
		include E404;
	
	exit;
}
