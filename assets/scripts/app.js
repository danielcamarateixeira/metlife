jQuery(document).ready(function(){

    // FUNÇÕES GLOBAIS DO SISTEMA

    moment.locale('pt-br');
    console.log(moment.locale());

    //$('form').validatr(); 
    $('tooltip').tooltip();
    $('input[readonly], textarea[readonly], select[readonly]').addClass('readonly');
    $('[data-toggle="tooltip"]').tooltip();

    // Preenche o CEP automaticamente
    $('.cep').gsCEP();

    // O botão que tiver a classe .click vai perguntar o usuário antes de continuar a sua ação
    $('.confirm').on('click', function () {
        var alert = (typeof($(this).data('alert')) != 'undefined') ? "\n" + $(this).data('alert') : '';
        return confirm('Voce tem certeza que quer fazer isso?'+alert);
    });

    // O botão que tiver a class .print-this irá imprimir o conteúdo do seu data-target
    $('.print-this').click(function(e) {
        var target = $(this).data('target');
        $(target).printThis();
        e.preventDefault();
    });

    // O botão que tiver a classe .btn-disabled não vai fazer nada
    $('.btn-disabled').click(function(event) {
        event.preventDefault();
        return false;
    });


    // FUNÇÕES DOS FORMULÁRIOS

    // TITULAR
    // Obrigatoriedade de DNV apenas para nascidos depois de 01/01/2010
    if ( $('#nascimento_pessoal').length ) {
        var tit_dnvDate = new Date('2010-01-01');
        var tit_nascimento = parseDateStr( $('#nascimento_pessoal').val() );
        if (tit_nascimento >= tit_dnvDate)
            $('#dnv_pessoal').attr('required', 'required').parents('.form-group').find('label-title').removeClass('label-required');
        else
            $('#dnv_pessoal').removeAttr('required').parents('.form-group').find('.label-title').addClass('label-required');
    }

    // Campo de vigência do benefício com monthpicker
    /*
    if ( $('#vigencia_beneficio').length > 0 ) {
        // $('#vigencia_beneficio').attr('min', moment().add(1, 'months').format('YYYY-MM'));
        $('#vigencia_beneficio').mask('00/0000');
        $('#vigencia_beneficio').MonthPicker({
            StartYear: '+1m',
            StartMonth: '+1m',
            MinMonth: '+1m',
            MinYear: '+1m',
            ShowIcon: false,
            i18n: {
                year: 'Ano',
                prevYear: "Ano anterior",
                nextYear: "Próximo ano",
                nextLabel: "Anterior",  
                prevLabel: "Próximo",
                jumpYears: "Escolher o ano",
                backTo: "voltar para",
                months: ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"]
            }
        });
    }
    */

    // Campo de vigência com os meses posteriores
    if ( $('#vigencia_beneficio').length > 0 ) {
        $('#vigencia_beneficio').html('<option>Carregando...</option>');
        $.getJSON('/metlife/ajax/vigencia?v=3', function(json, textStatus) {
            $('#vigencia_beneficio').html('');
            $.each(json, function(index, val) {
                $('#vigencia_beneficio').append('<option value="'+val.value+'">'+val.display+'</option>');
            });
        });        
    }

    // Utilizar o mesmo endereço residencial para o endereço de correspondência
    endCorrespHide()
    $('#mesmo-residencial_endereco-residencial_0').prop('checked', true);
    $('.form-group-mesmo-residencial').click(function(event) {
        if($('#mesmo-residencial_endereco-residencial_1').is(':checked')) {
            endCorrespShow();
        }
        else {
            endCorrespHide();
        }
    });

    function endCorrespHide () {
        $('.endereco-correspondencia').hide();
        $('.endereco-correspondencia input').prop('required', false);
    }
    function endCorrespShow () {
        $('.endereco-correspondencia').fadeIn(200);
        $('.endereco-correspondencia input').prop('required', true);
        $('#correspondencia-complemento_endereco-correspondencia').prop('required', false);
    }

    /*
    var endereco_fields = {
        'correspondencia-cep_endereco-correspondencia': 'cep_endereco-residencial',
        'correspondencia-endereco_endereco-correspondencia': 'endereco_endereco-residencial',
        'correspondencia-numero_endereco-correspondencia': 'numero_endereco-residencial',
        'correspondencia-complemento_endereco-correspondencia': 'complemento_endereco-residencial',
        'correspondencia-bairro_endereco-correspondencia': 'bairro_endereco-residencial',
        'correspondencia-cidade_endereco-correspondencia': 'cidade_endereco-residencial',
        'correspondencia-uf_endereco-correspondencia': 'uf_endereco-residencial'
    };
    $('#correspondencia-mesmo-residencial_endereco-correspondencia_0').click(function(event) {
        if ( $(this).prop('checked') == true ) {
            $('#cep_endereco-correspondencia').val( $('#cep_endereco-residencial').val() );
            $.each(endereco_fields, function(i, v) {
                $('#'+i).val( $('#'+v).val() );
            });
        }
        else {
            $.each(endereco_fields, function(i, v) {
                $('#'+i).val('');
            });
        }
    });
    */

    /**
     * Dependentes
     */

    // Utilizando um campo do tipo select:
    var total_dep = 10;
    // Esconde os campos
    depShow();

    $('#quantidade-dependentes').change(function(event) {
        depShow();
    });
    $('.dep-nascimento').blur(function(event) {
        var index = getDepIndex( $(this).attr('name') );
        depIdadeRequiredFields(index);
    });

    function depHideAll () {
        for (i=1;i<=total_dep;i++) {
            $('.dependente'+i+'-').hide();
            $('.dependente'+i+'- .form-group input').removeAttr('required').parents('.form-group').find('label-title').removeClass('label-required');
        }
    }
    function depShow () {
        if ( $('#quantidade-dependentes').length > 0 ) {
            var qtd = parseInt( $('#quantidade-dependentes').val() );
            if ( qtd == 0 || qtd == 'Nenhum' )
                depHideAll();
            else {
                depHideAll();
                for (i=1;i<=qtd;i++) {
                    $('.dependente'+i+'-').fadeIn(200);
                    $('.dependente'+i+'- .form-group input').not('#dependente'+i+'-rg_dependente, #dependente'+i+'-orgao-expedidor_dependente').attr('required', 'required').parents('.form-group').find('.label-title').addClass('label-required');
                    depIdadeRequiredFields(i);
                }
            }
        }
    }
    function getIdade (birthday) { // birthday is a date
        if (birthday == '') return false;
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    function parseDateStr (datestr) {
        if (datestr == '') return false;
        var day   = parseInt(datestr.substring(0,2));
        var month  = parseInt(datestr.substring(3,5));
        var year   = parseInt(datestr.substring(6,10));
        var date = new Date(year, month-1, day);
        return date;
    }
    function getDepIndex (str) {
        return parseInt(str.replace(/[^0-9\.]/g, ''), 10);
    }
    function depIdadeRequiredFields (i) {
        var nascimento = parseDateStr( $('.input-dependente'+i+'-nascimento').val() );
        var idade = getIdade( nascimento );
        console.log('idade = '+idade);

        // Obrigatoriedade de CPF apenas para maiores de 6 anos
        if (idade<6)
            $('.input-dependente'+i+'-cpf').removeAttr('required').parents('.form-group').find('label-title').removeClass('label-required');
        else
            $('.input-dependente'+i+'-cpf').attr('required', 'required').parents('.form-group').find('.label-title').addClass('label-required');

        // Obrigatoriedade de DNV apenas para nascidos depois de 01/01/2010
        var dnvDate = new Date('2010-01-01');
        if (nascimento >= dnvDate)
            $('.input-dependente'+i+'-dnv').attr('required', 'required').parents('.form-group').find('label-title').removeClass('label-required');
        else
            $('.input-dependente'+i+'-dnv').removeAttr('required').parents('.form-group').find('.label-title').addClass('label-required');
            

    }

    /*
    $('.dependente-1, .dependente-2, .dependente-3, .dependente-4').hide();
    $('select[name=quantidade-dependentes]').change(function(event) {
        if ( $(this).val() != 'Nenhum' ) {
            $('.dependente-1 input, .dependente-2 input, .dependente-3 input, .dependente-4 input').removeAttr('required');
            for(i=1;i<=$(this).val();i++) {
                $('.dependente-'+i).fadeIn();
                $('.dependente-'+i+' input').attr('required', 'required');
            }
        }
        else {
            $('.dependente-1, .dependente-2, .dependente-3, .dependente-4').hide();
            $('.dependente-1 input, .dependente-2 input, .dependente-3 input, .dependente-4 input').removeAttr('required');
        }
    });
    */
    
    // Utilizando botões de Adicionar/Remover
    // @TODO
    /*
    $('.form-field-group.dependente').hide();
    $('.form-group-container.dependente > div').each(function(i, e) {
        var prefix = $(e).data('prefix');
        var index = getDepIndexFromPrefix(prefix);
        $(e).find('button.btn-remove-dep').data('dependente', index);
        console.log(index);
    });
    $('body').on('click', '.btn-add-dep', function(event) {
        event.preventDefault();
        var dependente = $(this).data('dependente');
        console.log('Adicionar o dependente: '+dependente);
        // addDependente( dependente );
    });

    function getDepIndexFromPrefix (prefix) {
        return parseInt(prefix.replace(/[^0-9\.]/g, ''), 10);
    }
    
    function addDependente ( dep ) {
        var qtd_dep = $('#quantidade-dependentes').val();
        if ( dependente == 1 ) {
            $('.form-group-container.dependente').show();
        }
        else {

        }
        $('#quantidade-dependentes').val( qtd_dep++ );
    }
    */

    // Planos
    $('.plano').change(function(event) {
        console.log( $(this).val() );
        var valor = parseFloat( $(this).data('valor') );
        var valor_formatado = valor.formatMoney(2, ',', '.');
        var dependentes = parseInt( $(this).data('dependentes') );
        $('#titular-valor').val( valor_formatado );
        if ( dependentes >= 1 ) {
            for ( i=1; i<=dependentes; i++ ) {
                $('#dependente-'+i+'-valor').val( valor_formatado );
            }
        }
        var valor_total = (dependentes) ? valor * (dependentes+1) : valor;
        var total_formatado = valor_total.formatMoney(2, ',', '.');
        $('#valor-total, .input-valor-proposta').val( total_formatado );
        $('.input-valor-proposta-extenso').val( total_formatado.extenso(true) );

    });

    // Taxa de angariação
    /*
    $('.input-taxa-angariacao-valor').keyup(function(event) {
        // var valor = parseFloat( $(this).val() );
        var valor = $(this).val();
        $('.input-taxa-angariacao-extenso').val( valor.extenso(true) );
    });
    */    

    // Assinatura
    $('.tipo-assinatura-container').hide();
    $('.alert-assinatura').hide();
    $('.assinatura-digital').hide();

    function showAssinaturaAlert (tipo) {
        $('.alert-assinatura').hide();
        $('.alert-assinatura.'+tipo).fadeIn(200);
    }
    // selectTipoAssinatura('Manuscrita');
    function selectTipoAssinatura (tipo) {
        var tipo_slug = tipo.toLowerCase();
        // Marca o botão como selecionado
        $('.btn-assinatura').removeClass('active');
        $('#btn-assinatura-'+tipo_slug).addClass('active');
        $('#tipo-assinatura').val(tipo);
        showAssinaturaAlert(tipo_slug);

        if ( tipo_slug == 'digital' ) {
            $('.assinatura-digital').fadeIn();
            $('#input-assinatura-digital').attr('required', 'required');
        }
        else {
            $('.assinatura-digital').hide();
            $('#input-assinatura-digital').removeAttr('required');
        }
    }
    $('.btn-assinatura').click(function(e) {
        var tipo = $(this).data('assinatura');
        selectTipoAssinatura(tipo);
        e.preventDefault();
    });
    /*
    $('.btn-assinatura').click(function(e) {
        e.preventDefault();
        $('.btn-assinatura').removeClass('active');
        $(this).addClass('active');
        var tipo = $(this).find('h3').html();
        $('.assinatura_sl').val(tipo);
        if ( tipo == 'DIGITAL' ) 
            $('.assinatura-digital-container').fadeIn(200).find('input').attr('required', true);
        else
            $('.assinatura-digital-container').hide().find('input').removeAttr('required');

        showAssinaturaAlert( tipo.toLowerCase() );
    });
    // Deixa o botão visível de acordo com o select selecionado
    if ( $('.assinatura_sl').length > 0 ) {
        var sl_assinatura = $('.assinatura_sl').val().toLowerCase();
        $('.btn-assinatura').removeClass('active');
        $('#btn-assinatura-'+sl_assinatura).addClass('active');
        showAssinaturaAlert( sl_assinatura );
    }

    $('.assinatura_sl_update').change(function(event) {
        var value = $(this).val();
        var target = $( $(this).data('target') );
        console.log(value);
        if ( value == 'DIGITAL' ) {
            target.attr('required', true);
            target.parent().fadeIn(200);
        }
        else {
            target.removeAttr('required');
            target.parent().hide();
        }
    });
    */


    // DOCUMENTOS
    if ( $('#proposta-data').length > 0 ) {
        var proposta = $.parseJSON($('#proposta-data').html());
        console.log( proposta['maior-idade'] );
        if ( proposta['maior-idade'] == 'NÃO' )
            showDocumentoVinculoMaiorIdade();
        else
            hideDocumentoVinculoMaiorIdade();
    }
    function hideDocumentoVinculoMaiorIdade () {
        $('.form-group-doc-entidade-comprovante-vinculo-titular').hide();
    }
    function showDocumentoVinculoMaiorIdade () {
        $('.form-group-doc-entidade-comprovante-vinculo-titular').show();
    }


    $('.btn-prosseguir.btn-upload').click(function(event) {
        $('body').append('<div class="overlay"></div><div class="overlay-text"><p><i class="fa fa-cog fa-spin fa-3x fa-fw"></i>Enviando documentos, aguarde...<small>Por favor, não atualize a página</small></p></div>');
    });


    // BACKOFFICE
    $('.btn-revisar-submit').click(function(event) {
        var status = $('#inputStatus:checked').val();
        var page = $(this).data('page');
        // if ((status == 'corretor' && page == 'backoffice') || ((status == 'backoffice' || status == 'corretor') && page == 'cadastro')) {
        if ( status == 'corretor' ) {
            var all_empty = true;
            $('.input-observacao').each(function() {
                if ( $(this).val().length > 0 ) {
                    all_empty = false;
                    return false;
                }
            });
            if ( all_empty ) {
                alert('É preciso preencher ao menos um campo de correção para enviar de volta para '+status+'.');
                event.preventDefault();
                return false;
            }
        }
    });
    

    


    // Cusom Masks
    $('.input-cpf').mask('000.000.000-00', {reverse: true});
    $('.currency').mask("#.##0,00", {reverse: true});
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $('.celular').mask(SPMaskBehavior, spOptions);
    // ---------

    

    if ( $('#inputVencimento-primeira-mensalidade').length > 0 ) {
        var vigencia = '01/' + $('#vigencia').val();
        var vigencia_date_str = moment(vigencia, 'd/m/Y').format('Y-m-d');
        var vigencia_date = new Date(vigencia_date_str);
        var current = vigencia_date;
        current.setMonth(current.getMonth()+2); //If today is Jan, expect it to be Feb now
        var nextMonth = current.getMonth();
        $('#inputInicio-vigencia').val( vigencia);
        $('#inputVencimento-primeira-mensalidade').val('01/'+ ("0"+nextMonth).slice(-2)+'/'+current.getFullYear() );
    }

});


jQuery.fn.extend({
    // Coloca a classe de validação nos campos do formulário
    bs3ValidateClass: function (hasWhat, message, addIcon, type) {
        if (typeof hasWhat === 'undefined') { hasWhat = 'success'; }
        if (typeof message === 'undefined') { message = false; }
        if (typeof type === 'undefined') { type = 'text'; }
        if (typeof addIcon === 'undefined') { addIcon = true; }

        var iconIndex = {
            "success": "glyphicon-ok",
            "warning": "glyphicon-warning-sign",
            "error": "glyphicon-remove"
        }

        $(this).parent().removeClass('has-success has-warning has-error').addClass('has-feedback has-'+hasWhat);
        if (addIcon) {
            $(this).parent().find('span').remove();
            $(this).parent().append('<span class="glyphicon '+iconIndex[hasWhat]+' form-control-feedback" aria-hidden="true"></span><span class="sr-only">('+hasWhat+')</span>');
        }
        if (message) {
            $(this).parent().append('<span class="help-block">'+message+'</span>');
        }
    },
    gsCEP: function () {
        $(this).blur(function(event) {
            var cep = $(this).val().replace(/\D/g, '');
            var campo_endereco = $(this).data('endereco');
            var campo_bairro = $(this).data('bairro');
            var campo_cidade = $(this).data('cidade');
            var campo_estado = $(this).data('estado');
            var campo_numero = $(this).data('numero');
            var campos = [campo_endereco, campo_bairro, campo_cidade, campo_estado];

            if (cep != "") {
                var validacep = /^[0-9]{8}$/;
                if(validacep.test(cep)) {
                    $(this).bs3ValidateClass('success');
                    $(campos.join(',')).val('...').bs3ValidateClass('warning');
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                        if (!("erro" in dados)) {
                            $(campo_endereco).val(dados.logradouro).bs3ValidateClass('success');
                            $(campo_bairro).val(dados.bairro).bs3ValidateClass('success');
                            $(campo_cidade).val(dados.localidade).bs3ValidateClass('success');
                            $(campo_estado).val(dados.uf).bs3ValidateClass('success');
                            $(campo_numero).focus();
                        }
                        else {
                            $(this).bs3ValidateClass('warning', "CEP não encontrado");
                            $(campos.join(',')).val('').bs3ValidateClass('warning');
                            $(this).focus();
                        }
                    });
                }
                else {
                    $(this).bs3ValidateClass('error', "O CEP informado não é válido");
                    $(campos.join(',')).val('').bs3ValidateClass('warning');
                    $(this).focus();
                }
            }
        });
    }
});


Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function currencyFormatted(value, str_cifrao) {
    return str_cifrao + ' ' + value.formatMoney(2, ',', '.');
}


//+ Carlos R. L. Rodrigues
//@ http://jsfromhell.com/string/extenso [rev. #3]
String.prototype.extenso = function(c){
    var ex = [
        ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
        ["dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"],
        ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
        ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
    ];
    var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
    for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
        j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
        if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
        for(a = -1, l = v.length; ++a < l; t = ""){
            if(!(i = v[a] * 1)) continue;
            i % 100 < 20 && (t += ex[0][i % 100]) ||
            i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
            s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
            ((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("?o", "?es") : ex[3][t]) : ""));
        }
        a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
        a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
    }
    return r.join(e);
}