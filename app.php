<?php
/**
 * PHP Bootstrap 2
 * -----------------------
 * Controller file.
 * Define tudo que é preciso para que o app 
 * funcione independente de onde ele for
 * chamado.
 * 
 * Assegura que todas as variáveis irão 
 * funcionar, e que todas as configurações
 * estejam devidamente carregadas.
 * 
**/


/*==============================================
=            Configurações iniciais            =
==============================================*/

/* Inicia a sessão */
session_start();

/* Corrige o horário e idioma da aplicação */
setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
date_default_timezone_set('America/Sao_Paulo');

/* Pega o tempo de execução inicial da página */
$start_time = microtime(true);

/*==========================================
=            Constantes globais            =
==========================================*/

// Modo de execução da aplicação
define( 'MODE', 'sandbox' );

// Caminhos absolutos
define( 'APP_FOLDER', 'app' );
define( 'ROOT', dirname(__FILE__) );
define( 'APP', ROOT . '/' . APP_FOLDER );
define( 'LIB', APP . '/lib' );
define( 'VIEW', APP . '/view' );
define( 'LOG', APP . '/log' );
define( 'CONFIG', APP . '/config' );
define( 'UPLOADS', ROOT . '/uploads' );

// Carrega arquivos fundamentais
require APP . '/functions.php';
Config::getInstance();
if ( get_config('db') ) Database::getInstance();
Logger::getInstance();
new Auth;
new Usuario;
new Proposta;

// Caminhos relativos
define( 'ASSETS', get_config('relative_url') . 'assets' );
define( 'CSS', ASSETS . '/css' );
define( 'JS', ASSETS . '/scripts' );
define( 'IMAGES', ASSETS . '/images' );
define( 'IMAGES_URL', get_config('url') . 'assets/images' );
define( 'UPLOADS_REL', get_config('relative_url') . 'uploads' );

// Outras operações
register_shutdown_function('shutdown');