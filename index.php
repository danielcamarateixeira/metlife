<?php
/**
 * PHP Bootstrap 3
 * by John Goes
 * 2015-05-31 02:44AM DOM
**/

/* Carrega o arquivo principal da aplicação */
require './app.php';

/* Carrega as páginas da aplicação */
require APP . '/view.php';