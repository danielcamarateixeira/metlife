<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login - Nunes &amp; Grossi METLIFE</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/libs.min.css">
		<link rel="stylesheet" href="assets/metlife.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="login text-center">
			<div class="logo">
				<img src="assets/lock.png" class="lock">
				<h1>
					<span class="title-sm">Área do</span>
					Corretor
				</h1>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-xs-6 text-center">
						<img src="assets/metlife.png" class="img-responsive">
					</div>
					<div class="col-xs-6 text-center">
						<img src="assets/nunesegrossi.png" class="img-responsive">
					</div>
				</div>
				<form action="/metlife/proposta" method="post">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<input type="text" name="user" class="form-control input-lg" placeholder="Nome de usuário">
						</div>
						<div class="col-sm-12 col-md-6">
							<input type="password" name="pass" class="form-control input-lg" placeholder="Senha">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button type="submit" class="btn btn-default btn-lg btn-wide">Entrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<!-- jQuery -->
		<script src="assets/libs.min.js"></script>
		<script src="assets/scripts.js"></script>
	</body>
</html>