<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Etapa 3 Proposta - Nunes &amp; Grossi METLIFE</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/libs.min.css">
		<link rel="stylesheet" href="assets/metlife.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/metlife.png" class="img-responsive">
					</div>
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/nunesegrossi.png" class="img-responsive">
					</div>
					<div class="col-xs-12 col-sm-4 text-center">
						<div class="userinfo">
							<div class="verified">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Verificado
							</div>
							<span class="nome">Jorge dos Santos</span>
							<span class="userid"><strong>ID</strong>87304</span>
						</div>
					</div>
				</div>
				<nav class="navbar" role="navigation">
					<div class="container">
						<ul class="nav navbar-nav">
							<li><a href="#" class="btn btn-success btn-lg btn-wide">Nova proposta</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Histórico</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">FAQ &amp; Boas Práticas</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Sair</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>

		<div class="content">
			<div class="container">

				<form class="text-center" action="/metlife/proposta-3">

					<h1 class="title">Proposta N&ordm; 90498</h1>

					<div class="alert alert-info alert-simple alert-left">
						<h2>Autorização para envio e informações</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ex sem, hendrerit eu cursus non, lacinia venenatis nulla. Sed odio sem, commodo sed viverra et, lacinia consectetur nisi. Praesent mi orci, scelerisque a magna quis, molestie fermentum dui. Sed aliquet dapibus vulputate. Aliquam ac libero et orci efficitur porttitor ut sit amet felis. Cras vel vestibulum mi, vel lobortis eros. Mauris sed nisi in massa interdum dapibus vel nec dolor. Sed dolor ligula, ullamcorper a tincidunt ac, dignissim id enim. Nam non ornare nisi, vel faucibus tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse quis scelerisque ex, nec porta mi.</p>
					</div>

					<div class="form-action">
						<button class="btn btn-primary btn-lg btn-block">Autorizar</button>
						<button class="btn btn-danger btn-lg btn-block">Não autorizar</button>
						<button class="btn btn-primary-alt btn-lg btn-block">Imprimir contrato</button>
					</div>

				</form>

			</div>
		</div>

		<!-- jQuery -->
		<script src="assets/libs.min.js"></script>
		<script src="assets/scripts.js"></script>
	</body>
</html>
