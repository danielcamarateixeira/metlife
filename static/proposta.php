<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Proposta - Nunes &amp; Grossi METLIFE</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/libs.min.css">
		<link rel="stylesheet" href="assets/metlife.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/metlife.png" class="img-responsive">
					</div>
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/nunesegrossi.png" class="img-responsive">
					</div>
					<div class="col-xs-12 col-sm-4 text-center">
						<div class="userinfo">
							<div class="verified">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Verificado
							</div>
							<span class="nome">Jorge dos Santos</span>
							<span class="userid"><strong>ID</strong>87304</span>
						</div>
					</div>
				</div>
				<nav class="navbar" role="navigation">
					<div class="container">
						<ul class="nav navbar-nav">
							<li><a href="#" class="btn btn-success btn-lg btn-wide">Nova proposta</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Histórico</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">FAQ &amp; Boas Práticas</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Sair</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>

		<div class="content">
			<div class="container">
				<div class="alert alert-info alert-simple">
					TERMO DE ADESÃO AO PLANO
					DE ASSISTÊNCIA À SAÚDE PLANO
					ODONTOLÓGICO CONTRATO COLETIVO
					POR ADESÃO 
				</div>

				<form class="text-center" action="/metlife/proposta-2" method="post">

					<h1 class="title">Proposta N&ordm; 90498</h1>

					<div class="corretor-info clearfix">
						<div class="item">
							Vendedor
							<span class="nome">Jorge dos Santos</span>
						</div>
						<div class="item">
							Tel Vendedor
							<span class="nome">(13) 99999 9999</span>
						</div>
						<div class="item">
							Entidade
							<span class="nome">Jorge</span>
						</div>
						<div class="item">
							Corretora
							<span class="nome">Jorge</span>
						</div>
					</div>

					<div class="form-group">
						<label>Vigência do benefício:</label>
						<input type="date" name="" class="form-control">
					</div>
					<div class="form-group">
						<label>Nome completo sem abreviações</label>
						<textarea class="form-control"></textarea>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label>Estado Civil:</label>
								<input type="text" name="" class="form-control">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label>Parentesco:</label>
								<input type="text" name="" class="form-control">
							</div>
						</div>
					</div>
					<div class="radio">
						<label>Sexo</label> 
						<label class="radio-inline">
							<input type="radio" name="" id="input" value="" checked="checked">
							Masculino
						</label>
						<label class="radio-inline">
							<input type="radio" name="" id="input" value="" checked="checked">
							Feminino
						</label>
					</div>

					<div class="form-group">
						<label>Nome da mãe completo</label>
						<input type="text" name="" class="form-control">
					</div>
					<div class="form-group">
						<label>Naturalidade</label>
						<input type="text" name="" class="form-control">
					</div>
					<div class="form-group">
						<label>CNS - Cartão Nacional de Saúde</label>
						<input type="text" name="" class="form-control">
					</div>
					<div class="form-group">
						<label>PIS/PASEP</label>
						<input type="text" name="" class="form-control">
					</div>
					<div class="form-group">
						<label>CEP</label>
						<input type="text" name="" class="form-control">
					</div>

					<div class="form-action">
						<button class="btn btn-success btn-lg btn-block">Prosseguir</button>
					</div>

				</form>

			</div>
		</div>

		<!-- jQuery -->
		<script src="assets/libs.min.js"></script>
		<script src="assets/scripts.js"></script>
	</body>
</html>