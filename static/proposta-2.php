<!DOCTYPE html>
<html lang="pt">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Etapa 2 Proposta - Nunes &amp; Grossi METLIFE</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/libs.min.css">
		<link rel="stylesheet" href="assets/metlife.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/metlife.png" class="img-responsive">
					</div>
					<div class="col-xs-6 col-sm-4 text-center">
						<img src="assets/nunesegrossi.png" class="img-responsive">
					</div>
					<div class="col-xs-12 col-sm-4 text-center">
						<div class="userinfo">
							<div class="verified">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Verificado
							</div>
							<span class="nome">Jorge dos Santos</span>
							<span class="userid"><strong>ID</strong>87304</span>
						</div>
					</div>
				</div>
				<nav class="navbar" role="navigation">
					<div class="container">
						<ul class="nav navbar-nav">
							<li><a href="#" class="btn btn-success btn-lg btn-wide">Nova proposta</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Histórico</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">FAQ &amp; Boas Práticas</a></li>
							<li><a href="#" class="btn btn-primary btn-wide">Sair</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>

		<div class="content">
			<div class="container">

				<form class="text-center" action="/metlife/proposta-3" method="post">

					<h1 class="title">Proposta N&ordm; 90498</h1>

					<div class="form-group">
						<label>Dependentes</label>
						<input type="text" name="" class="form-control">
						<input type="text" name="" class="form-control">
						<input type="text" name="" class="form-control">
						<input type="text" name="" class="form-control">
					</div>

					<div class="total">
						TOTAL: R$ 90,50
					</div>

					<div class="row">
						<div class="col-xs-6 col-md-4">
							<div class="form-group">
								<label>Vencimento</label>
								<input type="text" name="" class="form-control">
							</div>
						</div>
						<div class="col-xs-6 col-md-4">
							<div class="form-group">
								<label>Vigência</label>
								<input type="text" name="" class="form-control">
							</div>
						</div>
						<div class="col-xs-6 col-md-4">
							<div class="form-group">
								<label>Demais parcelas</label>
								<input type="text" name="" class="form-control">
							</div>
						</div>
					</div>



					<div class="form-action">
						<button class="btn btn-success btn-lg btn-block">Prosseguir</button>
					</div>

				</form>

			</div>
		</div>

		<!-- jQuery -->
		<script src="assets/libs.min.js"></script>
		<script src="assets/scripts.js"></script>
	</body>
</html>
